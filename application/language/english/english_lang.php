<?php

if ( ! defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

$lang['SITENAME'] = "Men's Trading";

$lang['name']             = "Name";
$lang['email']            = "Email";
$lang['password']         = "Password";
$lang['confirm_password'] = "Confirm Password";
$lang['phone']            = "Phone";
$lang['status']           = "Status";
$lang['description']      = "Description";
$lang['category']         = "Category";
$lang['edit']             = "Edit";
$lang['view']             = "View";
$lang['delete']           = "Delete";
$lang['created']          = "Created";
$lang['created_at']       = "Created";
$lang['updated_at']       = "Updated";
$lang['updated']          = "Updated";
$lang['actions']          = "Actions";
$lang['English']          = 'English';
$lang['Arabic']           = 'Arabic';
$lang['lang_english']     = "English";
$lang['lang_spanish']     = "Spanish";
$lang['lang_german']      = "German";
$lang['lang_arabic']      = "Arabic";
$lang['login']            = "Login";
$lang['logout']           = "Logout";
$lang['export']           = "Export";

$lang['register']                  = "Register";
$lang['Recruitment']               = "Recruitment";
$lang['Choose Your Region']        = "Choose Your Region";
$lang['All Rights Reserved']       = "All Rights Reserved";
$lang['Copyright']                 = "Copyright";
$lang['Invalid Username/Password'] = "Invalid Username/Password";
$lang['forgot password']           = "Forgot password";
$lang['error_login_account']       = 'First login to your account';
$lang['saved_successfully']        = 'Saved successfully';

$lang['class']         = "Class";
$lang['weight']        = "Weight";
$lang['height']        = "Height";
$lang['class']         = "Class";
$lang['add_class']     = "Add Class";
$lang['list_class']    = "List Class";
$lang['entity']        = 'Entity';
$lang['add_entity']    = 'Add Entity';
$lang['list_entity']   = 'List Entity';
$lang['mobile_number'] = "Mobile Number";
$lang['user_name']     = "User Name";
$lang['news']          = 'News';
$lang['add_news']      = 'Add News';
$lang['list_news']     = 'List News';

$lang['title_en']               = "Title in English";
$lang['title_zh']               = "Title in Chinese";
$lang['description_en']         = "Description in English";
$lang['description_zh']         = "Description in Chinese";
$lang['summary_en']             = "Summary in English";
$lang['summary_zh']             = "Summary in Chinese";
$lang['start_date']             = "Start Date";
$lang['expire_date']            = "Expire Date";
$lang['news_images']            = "News Images";
$lang['entity_type']            = "Entity Type";
$lang['event']                  = 'Event';
$lang['add_event']              = 'Add Event';
$lang['list_event']             = 'List Event';
$lang['list_event']             = 'List Event';
$lang['event_name_en']          = "Name in English";
$lang['event_name_zh']          = "Name in Chinese";
$lang['event_from']             = "From";
$lang['event_to']               = "To";
$lang['reminder']               = "Reminder";
$lang['remind_x_days']          = "Remind x days";
$lang['comment_type']           = "Comment Type";
$lang['desciption_en']          = "Description in English";
$lang['desciption_zh']          = "Description in Chinese";
$lang['comment_date']           = "Date";
$lang['good_job_desciption_en'] = "Job Description in English";
$lang['good_job_desciption_zh'] = "Job Description in Chinese";
$lang['good_job_type']          = "Job Type";
$lang['good_job_picture_count'] = "Picture Count";
$lang['announcement']           = "Announcement";
$lang['add_announcement']       = "";
$lang['list_announcement']      = "";
$lang['title_en']               = "Title in English";
$lang['title_zh']               = "Title in Chinese";
$lang['details_en']             = "Details in English";
$lang['details_zh']             = "Details in Chinese";
$lang['announcement_date']      = "Date";
$lang['push_immediate']         = " Push Immediate";
$lang['add_announcement']       = "Add Announcement";
$lang['list_announcement']      = "List Announcement";
$lang['announcement_image']     = "Image";
$lang['group']                  = "Group";
$lang['global_id']              = "Global ID";
$lang['target_group']           = "Group";
$lang['add_group']              = "Add Group";
$lang['list_group']             = "List Group";
$lang['group']                  = "Group";
$lang['sno']                    = " S.No.";
$lang['late']                   = "Late";
$lang['early_leave']            = "Early Leave";
$lang['absent']                 = "Absent";
$lang['leave']                  = "Leave";
$lang['add_leave']              = "Add Leave";
$lang['list_leave']             = "List Leave";
$lang['leave_to']               = "Leave To";
$lang['leave_from']             = "Leave From";
$lang['approved_date']          = "Approved Date";
$lang['requested_date']         = "Requested Date";
$lang['reason']                 = "Reason";
$lang['leave_type']             = "Type";
$lang['approved_by']            = "Approved By";
$lang['leave_status']           = "Status";
$lang['notice']                 = "Notice";
$lang['notice_from']            = "From";
$lang['notice_to']              = "To";
$lang['require_sign']           = "Require Signing";
$lang['user_already_exist']     = "User with this email already exists";

$lang['login_btn_signin']  = "Sign In";
$lang['Accept']            = "Accept";
$lang['Reject']            = "Reject";
$lang['name']              = "Name";
$lang['email']             = "Email";
$lang['contactno']         = "Contact No";
$lang['status']            = "Status";
$lang['save']              = "Save";
$lang['reset']             = "Reset";
$lang['createdate']        = "Create Date";
$lang['language']          = "Language";
$lang['image']             = "Image";
$lang['coverimage']        = "Cover Image";
$lang['galleryimages']     = "Gallery Images";
$lang['enrolluser']        = "Enroll User";
$lang['admin_type']        = "Admin Type";
$lang['user_type']         = "user_type";
$lang['email_not_found']   = "We could not find your email in our records.  Please register to get access.";
$lang['email_send_forget'] = "Your password has been changed and an email has been sent to your email.";
$lang['Save']              = "Save";
$lang['Reset']             = "Reset";
$lang['select']            = "Select";

//*************************Commonly Used*************************

/*****************API ****************/
$lang['success']                 = 'Success';
$lang['error_invalid_password']  = 'Invalid Password';
$lang['error_invalid_email']     = 'Invalid Email';
$lang['error_no_record_found']   = 'Records are not found';
$lang['invalid_token']           = 'Invalid Token';
$lang['failed']                  = 'Failed';
$lang['registered_successfully'] = 'Registered Successfully';
$lang['registration_failed']     = 'Registration Failed';
$lang['']                        = '';


//************** Login Page *****************
$lang['login_lable_title']       = "AgileCrm";
$lang['login_lable_title_admin'] = "Admin";
$lang['login_page_title_admin']  = "Online Exam Portal Admin";
$lang['login_lable_rememberme']  = "Remember Me";

//******* Select  *****************
$lang['login_select_language'] = "--Select Language--";

//******* Button  *****************
$lang['login_btn_signin'] = "Sign In";
//************** End Login Page *****************

//************** Error and Success Message *****************
$lang['error_login_auth']                   = "Incorrect username or password";
$lang['error_register_auth']                = "Unable to register, please try again.";
$lang['Unable to Process Please Try Again'] = "Unable to Process Please Try Again";
$lang['File Remove Successfully']           = "File Remove Successfully";
$lang['error_occoured']                     = "Error Occurred Please Try Again";
$lang['note_submit_successfully']           = "Note Submit Successfully";
$lang['error_login_auth_error']             = "Unable to Login. Please contact the administrator.";


//************ CMS template page *************
$lang['Email Template'] = "Email Template";
$lang['Select Page']    = "Select Page";
$lang['Page Title']     = "Page Title";
$lang['Page Content']   = "Page Content";
$lang['project_title']  = "Project Title";

/* --------------Change password----------- */
$lang['current_password']                        = "Current Password";
$lang['new_password']                            = "New Password";
$lang['confirm_password']                        = "Confirm Password";
$lang['change_password']                         = "Change Password";
$lang['password_operation_message']              = "Password changed successfully.";
$lang['password_operation_error_message']        = "Invalid Current Password";
$lang['forgot_password']                         = "Forgot Password";
$lang['forgot_password_operation_error_message'] = "We could not find your email in our records.  Please apply to get access.";
$lang['forgot_password_operation_message']       = "Your password has been changed and an email has been sent to your email.";


/* --------------Other Maintenance----------- */
$lang['other_maintenance'] = "Other Maintenance";
$lang['page_template']     = "CMS";
$lang['static_email']      = "Static Email";

$lang['add']       = "Add";
$lang['list']      = "List";
$lang['dashboard'] = "Dashboard";

/*--------------Admin module-----------------*/

/* --------------User----------- */
$lang['user']         = "User";
$lang['user_id']      = "User ID";
$lang['add_user']     = "Add User";
$lang['list_user']    = "List User";
$lang['username']     = "User Name";
$lang['user_profile'] = "User Profile";
$lang['user_type']    = "User Type";
$lang['user_email']   = "User Email";

/*---------------------- Course ----------------*/
$lang['course_name']      = "Course Name";
/*---------------------- Question ----------------*/
$lang['question']      = "Question";
$lang['option_1']      = "Option 1";
$lang['option_2']      = "Option 2";
$lang['option_3']      = "Option 3";
$lang['option_4']      = "Option 4";
$lang['right_option']  = "Right Option";
$lang['active_status'] = "Status";

$lang['student_name'] = "Student Name";
$lang['student_roll'] = "Student Roll";
$lang['total_marks'] = "Total Marks";
$lang['marks_obtained'] = "Marks Obtained";
