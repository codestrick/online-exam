<?php

if ( ! defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

$lang['language_listing_headers'] = array(
    'languagename'  => 'Language Name',
    'languageimage' => 'Default',
    'edit'          => 'Edit',
    'delete'        => 'Delete'
);

$lang['portfolio_listing_headers'] = array(
    'portfolio_title'   => 'Portfolio Title',
    'portfolio_content' => 'Portfolio Content',
    'edit'              => 'Edit',
    'delete'            => 'Delete'
);

$lang['news_listing_headers']       = array(
    'news_title' => 'News Title',
    'edit'       => 'Edit',
    'delete'     => 'Delete'
);

$lang['cast_listing_headers']       = array(
    'cast_name' => 'Cast Name',
    'edit'      => 'Edit',
    'delete'    => 'Delete'
);

$lang['slider_listing_headers']     = array(
    'slider_title' => 'Slider Title',
    'edit'         => 'Edit',
    'delete'       => 'Delete'
);

$lang['fqcategory_listing_headers'] = array(
    'fqcategory_name' => 'Fq Category Title',
    'edit'            => 'Edit',
    'delete'          => 'Delete'
);

$lang['sponsors_listing_headers']   = array(
    'sponsor_name' => 'Sponsor Name',
    'edit'         => 'Edit',
    'delete'       => 'Delete'
);

    