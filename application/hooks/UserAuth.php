<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: surajgupta
 * Date: 7/8/17
 * Time: 9:22 PM
 */
class UserAuth
{
    private $_CI;

    function __construct()
    {
        $this->_CI = &get_instance();
    }

    function user_auth()
    {
        $this->_CI->load->helper('url');

        $module = $this->_CI->router->fetch_module();
        $class  = $this->_CI->router->fetch_class();
        $method = $this->_CI->router->fetch_method();
        $params = $this->_CI->uri->uri_to_assoc();

        if ($module == 'admin')
        {
            $redirectToLogin = TRUE;

            if (($method == 'login' && is_array($params) && isset($params['login']) && $params['login'] == 'redirectForcefully') ||
                ($method == 'validate' && is_array($params) && isset($params['validate']) && $params['validate'] == 'redirectForcefully')
            )
            {
                $redirectToLogin = FALSE;
            }

            $method_check = array('', 'login', 'logout', 'validate', 'dashboard');

            if ($redirectToLogin == TRUE)
            {

                $online_exam_admin = get_loggedin_user_data();
                $loggedin         = isset($online_exam_admin['id']) ? $online_exam_admin['id'] : NULL;

                if (empty($loggedin))
                {
                    $message = $this->_CI->session->flashdata('login_operation_message');
                    if ( ! empty($message))
                    {
                        $this->_CI->session->set_flashdata('login_operation_message', $message);
                    }
                    redirect('admin/user/login/redirectForcefully');
                }
                else
                {

                }
            }
        }
        else if ($module == 'public')
        {
        }
    }

    function user_permission()
    {
        $module       = $this->_CI->router->fetch_module();
        $class        = $this->_CI->router->fetch_class();
        $method       = $this->_CI->router->fetch_method();
        $params       = $this->_CI->uri->uri_to_assoc();
        $method_check = array('', 'login', 'logout', 'validate', 'dashboard');

        if ($module == 'admin')
        {
            $online_exam_admin = isset($this->_CI->session->userdata['online_exam_admin']['user']) ? $this->_CI->session->userdata['online_exam_admin']['user'] : array();
            $role = isset($online_exam_admin['type']) ? $online_exam_admin['type'] : null;

            if (in_array($role, $this->_CI->allowed_user_role) || in_array($method, $method_check))
            {

            }
            else
            {
                $message = "Invalid operation performed.";
                if ( ! empty($message))
                {
                    $this->_CI->session->set_flashdata('user_operation_message', $message);
                }
                redirect('admin/dashboard');
            }
        }
        else if ($module == 'public')
        {

        }
    }
}