<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Languageloader
{
    function initialize()
    {
        $CI = &get_instance();
        $CI->load->helper('language');

        $language = getCustomConfigItem("default_language");

        $site_session = $CI->session->userdata('online_exam_admin');
        $site_lang    = isset($site_session['site_lang']) ? $site_session['site_lang'] : '';

        if (empty($site_lang))
        {
            $lang         = array(
                'site_lang' => $language
            );
            $site_session = empty($site_session) ? array() : $site_session;
            $site_session = array_merge($site_session, $lang);
            $CI->session->set_userdata('online_exam_admin', $site_session);
            $site_lang = $language;
        }
        $CI->lang->load($site_lang, $site_lang);
    }

}
    