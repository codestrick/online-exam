<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	https://codeigniter.com/user_guide/general/hooks.html
|
*/

$hook['post_controller_constructor'][] = array(
    'class'    => 'publicBooking',
    'function' => 'check_booking_timeout',
    'filename' => 'publicBooking.php',
    'filepath' => 'hooks',
    'params'   => array()
);

$hook['post_controller_constructor'][] = array(
    'class'    => 'Languageloader',
    'function' => 'initialize',
    'filename' => 'Languageloader.php',
    'filepath' => 'hooks',
    'params'   => array()
);

$hook['post_controller_constructor'][] = array(
    'class'    => 'UserAuth',
    'function' => 'user_auth',
    'filename' => 'UserAuth.php',
    'filepath' => 'hooks',
    'params'   => array()
);

$hook['post_controller_constructor'][] = array(
    'class'    => 'UserAuth',
    'function' => 'user_permission',
    'filename' => 'UserAuth.php',
    'filepath' => 'hooks',
    'params'   => array()
);