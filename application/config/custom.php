<?php

if ( ! defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

$config['custom']["default_language"] = 'english';

$config['custom']["excel_upload"] = array(
    'upload_path'   => 'assets/file/',
    'allowed_types' => 'xlsx|xls|csv|ods',
    'overwrite'     => FALSE,
    'max_size'      => 3072,
    'default'       => ''
);

$config['custom']["email_config"] = array(
    'protocol'  => 'smtp',
    'smtp_host' => 'ssl://smtp.googlemail.com',
    'smtp_port' => 465,
    'smtp_user' => 'webcompany27@gmail.com',
    'smtp_pass' => 'Webcompany123#',
    'mailtype'  => 'html',
    'newline'   => '\r\n',
    'wordwrap'  => TRUE,
    'charset'   => 'iso-8859-1'
);

$config['custom']["pages"] = array(
    'Home'       => 'Home',
    'About-Us'   => 'About Us',
    'Contact-Us' => 'Contact Us',
    'Terms'      => 'Terms',
);

$config['custom']["import_rate_file"] = array(
    'upload_path'   => 'assets/document/',
    'allowed_types' => 'csv',
    'overwrite'     => TRUE,
);

$config['custom']['status'] = array(
    "Active"  => "Active",
    "Blocked" => "Blocked",
);

$config['custom']['slider_status'] = array(
    "Active"  => "Active",
    "Blocked" => "Blocked",
);

$config['custom']["smstemplates"] = array(
    'Signup Sms'          => 'Signup Sms',
    'Forgot Password Sms' => 'Forgot Password Sms',
    'Reference Sms'       => 'Reference Sms'
);

$config['custom']["lettertemplates"] = array(
    'New interviewer' => 'New interviewer',
    'Cleaner welcome' => 'Cleaner welcome',
);

$config['custom']["pagetemplates"] = array(
    'Terms Conditions' => 'Terms Conditions',
);

$config['custom']["gender"] = array(
    "male"   => "male",
    "female" => "female",
);

$config['custom']["news_images"] = array(
    'upload_path'   => FCPATH . 'assets/uploads/news_images/',
    'allowed_types' => 'jpg|jpeg|png',
    'overwrite'     => TRUE,
    'default'       => 'default.jpg'
);

$config['custom']['pagination_config'] = array(
    'use_page_numbers' => TRUE,
    'num_links'        => 2,
    'full_tag_open'    => '<ul class="pagination-custom list-unstyled list-inline">',
    'full_tag_close'   => '</ul>',
    'first_tag_open'   => '<li>',
    'first_tag_close'  => '</li>',
    'last_tag_open'    => '<li>',
    'last_tag_close'   => '</li>',
    'next_tag_open'    => '<li>',
    'next_tag_close'   => '</li>',
    'prev_tag_open'    => '<li>',
    'prev_tag_close'   => '</li>',
    'num_tag_open'     => '<li>',
    'num_tag_close'    => '</li>',
    'cur_tag_open'     => '<li><a  class="btn btn-sm btn-primary">',
    'cur_tag_close'    => '</a></li>',
    'next_link'        => '&raquo;',
    'prev_link'        => '&laquo;',
);

$config['custom']['language_files_arr'] = array(
    "English" => "english",
    "Arabic"  => "arabic",
);
$config['custom']['map_language_arr']   = array(
    "english" => "en",
    "arabic"  => "ar",
);

$config['custom']['template_type'] = array(
    'Email'  => 'Email',
    'Letter' => 'Letter',
    'Sms'    => 'Sms',
);

$config['custom']['user_type'] = array(
    'super-admin'  => 'Super Admin',
    'admin'        => 'Admin',
);


$config['custom']['recaptcha_info'] = array(
    'recaptcha_site_key'   => '6LdTaCYTAAAAAF2vWMO-o9jugKEf_CdKEuEraUg1',
    'recaptcha_secret_key' => '6LdTaCYTAAAAABfCBuGGfe-rfxmR74AdY8SgC5Zm',
    'recaptcha_lang'       => 'en',
);

$config['custom']['youtube_embed_large'] = array(
    'width'  => 560,
    'height' => 315,
);

$config['custom']['youtube_embed_small'] = array(
    'width'  => 333,
    'height' => 187,
);

$config['custom']['meta_data'] = array(
    'meta:title'          => 'UK Jewish Film',
    'meta:description'    => 'We entertain, educate and enlighten diverse audiences in the UK and internationally, provide support to emerging filmmakers, and organise educational events',
    'meta:keywords'       => 'We entertain, educate and enlighten diverse audiences in the UK and internationally, provide support to emerging filmmakers, and organise educational events',
    'og:url'              => '',
    'og:image'            => '',
    'og:type'             => '',
    'og:title'            => '',
    'og:description'      => '',
    'twitter:card'        => '',
    'twitter:site'        => '',
    'twitter:title'       => '',
    'twitter:description' => '',
    'twitter:image'       => '',
    'meta:robots'         => 'index, follow'
);

$config['custom']["staff_image_config"] = array(
    'upload_path'   => FCPATH . 'assets/uploads/staff/images/',
    'allowed_types' => 'jpg|jpeg|png',
    'overwrite'     => FALSE,
    'max_size'      => 2048,
    'default'       => 'default.jpg'
);

defined('SITENAME') OR define('SITENAME', 'online_exam');
defined('EMAIL_FROM_NAME') OR define('EMAIL_FROM_NAME', 'online_exam');
defined('EMAIL_FROM_EMAIL') OR define('EMAIL_FROM_EMAIL', 'info@sharmatech.org');

//QUIZ CONSTANTS
defined('QUESTION_LIMIT') OR define('QUESTION_LIMIT', 10);
defined('QUESTION_MARKS') OR define('QUESTION_MARKS', 1);

    
