<?php

if ( ! defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

$CI = &get_instance();
$CI->load->library('session'); //if it's not auto-loaded in your CI setup

$site_session = $CI->session->userdata('online_exam_admin');
$site_lang    = isset($site_session['site_lang']) ? $site_session['site_lang'] : 'english';


//----- DEFAULT ASSETS -----
$config['default_css'] = array();
$config['default_js']  = array();


//----- PUBLIC ASSETS -----
$config['public_default_css'] = array(
    'bootstrap' => array('name' => 'assets/public/css/bootstrap.css'),
    'style'     => array('name' => 'assets/public/css/style.css'),
    'ken-burns' => array('name' => 'assets/public/css/ken-burns.css'),
    'animate'   => array('name' => 'assets/public/css/animate.min.css'),
);

$config['public_default_js'] = array(
    'jquery'    => array('name' => 'assets/public/js/jquery.min.js'),
    'bootstrap' => array('name' => 'assets/public/js/bootstrap.min.js'),
);


//----- ADMIN ASSETS -----
$config['admin_default_css'] = array(
    'bootstrap' => array('name' => 'assets/admin/css/bootstrap.min.css'),
    'icons'     => array('name' => 'assets/admin/css/icons.css'),
    'metismenu' => array('name' => 'assets/admin/css/metismenu.min.css'),
    'style'     => array('name' => 'assets/admin/css/style.css'),

    'general' => array('name' => 'assets/admin/css/general.css'),
);

$config['admin_default_js'] = array(
    'jquery'                       => array('name' => 'assets/admin/js/jquery.min.js'),
    'tether'                       => array('name' => 'assets/admin/js/tether.min.js'),
    'bootstrap'                    => array('name' => 'assets/admin/js/bootstrap.min.js'),
    'metisMenu'                    => array('name' => 'assets/admin/js/metisMenu.min.js'),
    'waves'                        => array('name' => 'assets/admin/js/waves.js'),
    'jquery.slimscroll'            => array('name' => 'assets/admin/js/jquery.slimscroll.js'),
    'jquery.core'                  => array('name' => 'assets/admin/js/jquery.core.js'),
    'jquery.app'                   => array('name' => 'assets/admin/js/jquery.app.js'),

    //JQuery Validation
    'jquery-validation'            => array('name' => 'assets/js/jquery-validation/jquery.validate.js'),
    'jquery-validation-additional' => array('name' => 'assets/js/jquery-validation/additional-methods.js'),

    'general' => array('name' => 'assets/js/general.js'),
);


//----- EXTRA ASSETS -----
$config['css_arr'] = array(
    'dataTables.bootstrap4' => array('name' => 'assets/admin/plugins/datatables/dataTables.bootstrap4.min.css'),
    'responsive.bootstrap4' => array('name' => 'assets/admin/plugins/datatables/responsive.bootstrap4.min.css'),

    'select2' => array('name' => 'assets/admin/plugins/select2/css/select2.min.css'),
);

$config['js_arr'] = array(
    'jquery.dataTables'       => array('name' => 'assets/admin/plugins/datatables/jquery.dataTables.min.js'),
    'dataTables.bootstrap4'   => array('name' => 'assets/admin/plugins/datatables/dataTables.bootstrap4.min.js'),
    'dataTables.fixedColumns' => array('name' => 'assets/admin/plugins/datatables/dataTables.responsive.min.js'),
    'dataTables.responsive'   => array('name' => 'assets/admin/plugins/datatables/responsive.bootstrap4.min.js'),

    'select2'        => array('name' => 'assets/admin/plugins/select2/js/select2.min.js'),
    'jquery.blockUI' => array('name' => 'assets/public/js/jquery.blockUI.js'),

    'parsley' => array('name' => 'assets/admin/plugins/parsleyjs/parsley.min.js'),

    'course'          => array('name' => 'assets/admin/js/pages/course.js'),
    'question'        => array('name' => 'assets/admin/js/pages/question.js'),
    'result_records'  => array('name' => 'assets/admin/js/pages/result_records.js'),
    'settings'        => array('name' => 'assets/admin/js/pages/settings.js'),
    'student'         => array('name' => 'assets/admin/js/pages/student.js'),
    'user'            => array('name' => 'assets/admin/js/pages/user.js'),
    'change-password' => array('name' => 'assets/admin/js/pages/change-password.js'),
);

//----- Login Assets -----
$config['login_register_css'] = array();
$config['login_register_js']  = array();
