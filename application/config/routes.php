<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	https://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There are three reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router which controller/method to use if those
  | provided in the URL cannot be matched to a valid route.
  |
  |	$route['translate_uri_dashes'] = FALSE;
  |
  | This is not exactly a route, but allows you to automatically route
  | controller and method names that contain dashes. '-' isn't a valid
  | class or method name character, so it requires translation.
  | When you set this option to TRUE, it will replace ALL dashes in the
  | controller and method URI segments.
  |
  | Examples:	my-controller/index	-> my_controller/index
  |		my-controller/my-method	-> my_controller/my_method
 */

/********DEFAULT*******/
$route['default_controller'] = 'public/index/home';
$route['404_override']       = '';
/********DEFAULT*******/


/*********************** Public Side ****************** */

$route['home']               = 'public/index/home';
$route['about-us']           = 'public/index/about_us';
$route['contact-us']         = 'public/index/contact_us';
$route['under-construction'] = 'public/index/under_construction';

$route['student/login']           = 'public/student/login';
$route['student/logout']          = 'public/student/logout';
$route['student/change-password'] = 'public/student/change_password';
$route['student/profile']         = 'public/student/student_profile';

$route['quiz']          = 'public/quiz/index';
$route['result']        = 'public/quiz/result';
$route['quiz/time-out'] = 'public/quiz/time_out';


/************************************Admin Side*************************************** */
$route['admin']                 = 'admin/user/dashboard';
$route['admin/dashboard']       = 'admin/user/dashboard';
$route['admin/change-password'] = 'admin/user/change_password';
$route['admin/forgot-password'] = 'admin/user/forgot_password';


/******** USER ROUTES *******/
$route['admin/user/add-user']           = 'admin/user/add_user';
$route['admin/user/list-user']          = 'admin/user/list_user';
$route['admin/user/delete-user/(:num)'] = 'admin/user/delete_user/$1';
$route['admin/user/edit-user/(:num)']   = 'admin/user/add_user/$1';
$route['admin/user/select/(:num)']      = 'admin/user/select/$1';


/******** STUDENT ROUTES *******/
$route['admin/student/add-student']           = 'admin/student/add_student';
$route['admin/student/list-student']          = 'admin/student/index';
$route['admin/student/delete-student/(:num)'] = 'admin/student/delete_student/$1';
$route['admin/student/edit-student/(:num)']   = 'admin/student/add_student/$1';


/******** COURSE ROUTES *******/
$route['admin/course/add-course']           = 'admin/course/add_course';
$route['admin/course/list-course']          = 'admin/course/index';
$route['admin/course/delete-course/(:num)'] = 'admin/course/delete_course/$1';
$route['admin/course/edit-course/(:num)']   = 'admin/course/add_course/$1';

/******** STAFF ROUTES *******/
$route['admin/staff/add-staff']           = 'admin/staff/add_staff';
$route['admin/staff/list-staff']          = 'admin/staff/index';
$route['admin/staff/delete-staff/(:num)'] = 'admin/staff/delete_staff/$1';
$route['admin/staff/edit-staff/(:num)']   = 'admin/staff/add_staff/$1';


/******** QUESTION ROUTES *******/
$route['admin/question/add-question']           = 'admin/question/add_question';
$route['admin/question/bulk-add-question']      = 'admin/question/bulk_question_add';
$route['admin/question/list-question']          = 'admin/question/index';
$route['admin/question/download']               = 'admin/question/download_demo_file';
$route['admin/question/delete-question/(:num)'] = 'admin/question/delete_question/$1';
$route['admin/question/edit-question/(:num)']   = 'admin/question/add_question/$1';

/***************  Settings ******************/
$route['admin/settings'] = 'admin/settings/index';

/********************* Result_records **************/
$route['admin/result-records'] = 'admin/result_records/index';


/******** BASIC INFORMATION ROUTES *******/
$route['admin/basic-information']           = 'admin/basicInformation/index';
$route['admin/basic-information/(:num)']           = 'admin/basicInformation/index/$1';

$route['admin/about-us']           = 'admin/aboutUs/index';
$route['admin/about-us/(:num)']           = 'admin/aboutUs/index/$1';

/************************************Admin Side*************************************** */