<?php

if ( ! defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

if ( ! isset($config))
{
    $config = array();
}

$config['datatable_class'] = "dyntable table table-bordered table-hover dataTable";

$config['user_listing_headers'] = array(
    'name'       => array(
        'jsonField' => 'name',
        'width'     => '10%'
    ),
    'email'      => array(
        'jsonField' => 'email',
        'width'     => '10%'
    ),
    'username'   => array(
        'jsonField' => 'username',
        'width'     => '10%'
    ),
    'status'     => array(
        'jsonField' => 'status',
        'width'     => '10%'
    ),
    'user_type'  => array(
        'jsonField' => 'user_type',
        'width'     => '10%'
    ),
    'created_at' => array(
        'jsonField' => 'created_at',
        'width'     => '10%'
    ),
    'updated_at' => array(
        'jsonField' => 'updated_at',
        'width'     => '10%'
    ),
    'edit'       => array(
        'isSortable'     => FALSE,
        'systemDefaults' => TRUE,
        'type'           => 'EDIT_ICON',
        'isLink'         => TRUE,
        'linkParams'     => array('id'),
        'linkTarget'     => 'admin/user/edit-user/',
        'width'          => '5%',
        'align'          => 'center'
    ),
    'delete'     => array(
        'isSortable'     => FALSE,
        'systemDefaults' => TRUE,
        'type'           => 'DELETE_ICON',
        'confirmBox'     => TRUE,
        'isLink'         => TRUE,
        'linkParams'     => array('id'),
        'linkTarget'     => 'admin/user/delete-user/',
        'width'          => '5%',
        'align'          => 'center'
    ),
    'select'     => array(
        'isSortable'     => FALSE,
        'systemDefaults' => TRUE,
        'type'           => 'HAND_ICON',
        'confirmBox'     => TRUE,
        'isLink'         => TRUE,
        'linkParams'     => array('id'),
        'linkTarget'     => 'admin/user/select/',
        'width'          => '5%',
        'align'          => 'center'
    )
);

$config['course_listing_headers'] = array(
    'name'        => array(
        'jsonField' => 'name',
        'width'     => '10%'
    ),
    'user_id'    => array(
        'jsonField' => 'user_id',
        'width'     => '5%',
        'align'     => 'center'
    ),
        'created_at' => array(
            'jsonField' => 'created_at',
            'width'     => '10%'
        ),
        'updated_at' => array(
            'jsonField' => 'updated_at',
            'width'     => '10%'
        ),
    'edit'        => array(
        'isSortable'     => FALSE,
        'systemDefaults' => TRUE,
        'type'           => 'EDIT_ICON',
        'isLink'         => TRUE,
        'linkParams'     => array('id'),
        'linkTarget'     => 'admin/course/edit-course/',
        'width'          => '3%',
        'align'          => 'center'
    ),
    'delete'      => array(
        'isSortable'     => FALSE,
        'systemDefaults' => TRUE,
        'type'           => 'DELETE_ICON',
        'confirmBox'     => TRUE,
        'isLink'         => TRUE,
        'linkParams'     => array('id'),
        'linkTarget'     => 'admin/course/delete-course/',
        'width'          => '3%',
        'align'          => 'center'
    )
);


$config['staff_listing_headers'] = array(
    'name'        => array(
        'jsonField' => 'name',
        'width'     => '10%'
    ),
    'user_id'    => array(
        'jsonField' => 'user_id',
        'width'     => '5%',
        'align'     => 'center'
    ),
    'created_at' => array(
        'jsonField' => 'created_at',
        'width'     => '10%'
    ),
    'updated_at' => array(
        'jsonField' => 'updated_at',
        'width'     => '10%'
    ),
    'edit'        => array(
        'isSortable'     => FALSE,
        'systemDefaults' => TRUE,
        'type'           => 'EDIT_ICON',
        'isLink'         => TRUE,
        'linkParams'     => array('id'),
        'linkTarget'     => 'admin/staff/edit-staff/',
        'width'          => '3%',
        'align'          => 'center'
    ),
);

$config['question_listing_headers'] = array(
    'question'        => array(
        'jsonField' => 'question',
        'width'     => '10%'
    ),
    'option_1'        => array(
        'jsonField' => 'option_1',
        'width'     => '10%'
    ),
    'option_2'        => array(
        'jsonField' => 'option_2',
        'width'     => '10%'
    ),
    'option_3'        => array(
        'jsonField' => 'option_3',
        'width'     => '10%'
    ),
    'option_4'        => array(
        'jsonField' => 'option_4',
        'width'     => '10%'
    ),
    'right_option'        => array(
        'jsonField' => 'right_option',
        'width'     => '10%'
    ),
    'course_name'        => array(
        'jsonField' => 'course_name',
        'width'     => '10%'
    ),
    'active_status'        => array(
        'jsonField' => 'active_status',
        'width'     => '10%'
    ),
    'user_id'    => array(
        'jsonField' => 'user_id',
        'width'     => '5%',
        'align'     => 'center'
    ),
    'created_at' => array(
        'jsonField' => 'created_at',
        'width'     => '10%'
    ),
    'updated_at' => array(
        'jsonField' => 'updated_at',
        'width'     => '10%'
    ),
    'edit'        => array(
        'isSortable'     => FALSE,
        'systemDefaults' => TRUE,
        'type'           => 'EDIT_ICON',
        'isLink'         => TRUE,
        'linkParams'     => array('id'),
        'linkTarget'     => 'admin/question/edit-question/',
        'width'          => '3%',
        'align'          => 'center'
    ),
    'delete'      => array(
        'isSortable'     => FALSE,
        'systemDefaults' => TRUE,
        'type'           => 'DELETE_ICON',
        'confirmBox'     => TRUE,
        'isLink'         => TRUE,
        'linkParams'     => array('id'),
        'linkTarget'     => 'admin/question/delete-question/',
        'width'          => '3%',
        'align'          => 'center'
    )
);

$config['student_listing_headers'] = array(
    'student_roll'        => array(
        'jsonField' => 'student_roll',
        'width'     => '10%'
    ),
    'name'        => array(
        'jsonField' => 'name',
        'width'     => '10%'
    ),
    'email'        => array(
        'jsonField' => 'email',
        'width'     => '10%'
    ),
    'address'        => array(
        'jsonField' => 'address',
        'width'     => '10%'
    ),
    'pincode'        => array(
        'jsonField' => 'pincode',
        'width'     => '10%'
    ),
    'mobile'        => array(
        'jsonField' => 'mobile',
        'width'     => '10%'
    ),
    'active_status'        => array(
        'jsonField' => 'active_status',
        'width'     => '10%'
    ),
    'created_at' => array(
        'jsonField' => 'created_at',
        'width'     => '10%'
    ),
    'updated_at' => array(
        'jsonField' => 'updated_at',
        'width'     => '10%'
    ),
    'edit'        => array(
        'isSortable'     => FALSE,
        'systemDefaults' => TRUE,
        'type'           => 'EDIT_ICON',
        'isLink'         => TRUE,
        'linkParams'     => array('id'),
        'linkTarget'     => 'admin/student/edit-student/',
        'width'          => '3%',
        'align'          => 'center'
    ),
    'delete'      => array(
        'isSortable'     => FALSE,
        'systemDefaults' => TRUE,
        'type'           => 'DELETE_ICON',
        'confirmBox'     => TRUE,
        'isLink'         => TRUE,
        'linkParams'     => array('id'),
        'linkTarget'     => 'admin/student/delete-student/',
        'width'          => '3%',
        'align'          => 'center'
    )
);
$config['result_records_listing_headers'] = array(
    'student_name'        => array(
        'jsonField' => 'student_name',
        'width'     => '10%'
    ),
    'student_roll'        => array(
        'jsonField' => 'student_roll',
        'width'     => '10%'
    ),
    'user_id'        => array(
        'jsonField' => 'user_id',
        'width'     => '10%'
    ),
    'course_name'        => array(
        'jsonField' => 'course_name',
        'width'     => '10%'
    ),
    'total_marks'        => array(
        'jsonField' => 'total_marks',
        'width'     => '10%'
    ),
    'marks_obtained'        => array(
        'jsonField' => 'marks_obtained',
        'width'     => '10%'
    ),
    'delete'      => array(
        'isSortable'     => FALSE,
        'systemDefaults' => TRUE,
        'type'           => 'DELETE_ICON',
        'confirmBox'     => TRUE,
        'isLink'         => TRUE,
        'linkParams'     => array('id'),
        'linkTarget'     => 'admin/results-records/delete-records/',
        'width'          => '3%',
        'align'          => 'center'
    )
);