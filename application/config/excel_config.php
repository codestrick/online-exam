<?php

    if (!defined('BASEPATH'))
        exit('No direct script access allowed');

    if (!isset($config))
    {
        $config = array();
    }

    $config["excel_config"] = array(
        'company_rates_heading' => array(
            'Postcode Sector' => array(
                'field_name' => 'postcode_sector',
                'size' => '20',
            ),
            'Service' => array(
                'field_name' => 'service_title',
                'size' => '20',
            ),
            'Minimum Hours' => array(
                'field_name' => 'min_hours',
                'size' => '15',
            ),
            'Rate' => array(
                'field_name' => 'rate',
                'size' => '10',
            ),
        ),
    );
    