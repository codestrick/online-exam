<?php

if ( ! defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

class AboutUs extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('common');
        $this->load->library('form_validation');
        $this->load->library('commonlib');
        $this->load->model('About_us_model');
        $this->load->setTemplate('admin');
    }


    public function index($id = NULL)
    {
        $About_us = $this->About_us_model->get_detail([]);

        if(!empty($About_us['id'])){
            $id = $About_us['id'];
        }

        $this->form_validation->set_rules('content', 'Content', "required");
        $this->form_validation->set_rules('heading', 'Heading', "required");


        if ($this->form_validation->run() == FALSE)
        {
            $dataArray['local_css'] = array(
                'select2',
            );
            $dataArray['local_js']  = array(
                'parsley',
                'select2',
            );

            if ($id != NULL)
            {
                $About_us = $this->About_us_model->get_detail(['id' => $id]);
                if (empty($About_us))
                {
                    $this->session->set_flashdata('flash_message', 'Invalid operation performed.');
                    $this->session->set_flashdata('flash_message_status', FALSE);
                    redirect('admin/about-us');
                }
                else
                {
                    $dataArray['id']     = $About_us['id'];
                    $dataArray['content']   = $About_us['content'];
                    $dataArray['heading']   = $About_us['heading'];


                    $points = json_decode($About_us['points'],1);
                    $fun_facts = json_decode($About_us['fun_facts'],1);
                    $what_we_do = json_decode($About_us['what_we_do'],1);

                    $dataArray['point_1']   = !empty($points['point_1']) ? $points['point_1'] : '';
                    $dataArray['point_2']   = !empty($points['point_2']) ? $points['point_2'] : '';
                    $dataArray['point_3']   = !empty($points['point_3']) ? $points['point_3'] : '';
                    $dataArray['point_4']   = !empty($points['point_4']) ? $points['point_4'] : '';
                    $dataArray['point_5']   = !empty($points['point_5']) ? $points['point_5'] : '';

                    $dataArray['fun_percentage_1']   = !empty($fun_facts['fun_percentage_1']) ? $fun_facts['fun_percentage_1'] : '';
                    $dataArray['fun_percentage_2']   = !empty($fun_facts['fun_percentage_2']) ? $fun_facts['fun_percentage_2'] : '';
                    $dataArray['fun_percentage_3']   = !empty($fun_facts['fun_percentage_3']) ? $fun_facts['fun_percentage_3'] : '';
                    $dataArray['fun_percentage_4']   = !empty($fun_facts['fun_percentage_4']) ? $fun_facts['fun_percentage_4'] : '';
                    $dataArray['fun_point_1']   = !empty($fun_facts['fun_point_1']) ? $fun_facts['fun_point_1'] : '';
                    $dataArray['fun_point_2']   = !empty($fun_facts['fun_point_2']) ? $fun_facts['fun_point_2'] : '';
                    $dataArray['fun_point_3']   = !empty($fun_facts['fun_point_3']) ? $fun_facts['fun_point_3'] : '';
                    $dataArray['fun_point_4']   = !empty($fun_facts['fun_point_4']) ? $fun_facts['fun_point_4'] : '';
                    $dataArray['fun_description_1']   = !empty($fun_facts['fun_description_1']) ? $fun_facts['fun_description_1'] : '';
                    $dataArray['fun_description_2']   = !empty($fun_facts['fun_description_2']) ? $fun_facts['fun_description_2'] : '';
                    $dataArray['fun_description_3']   = !empty($fun_facts['fun_description_3']) ? $fun_facts['fun_description_3'] : '';
                    $dataArray['fun_description_4']   = !empty($fun_facts['fun_description_4']) ? $fun_facts['fun_description_4'] : '';

                    $dataArray['we_do_heading_1']   = !empty($what_we_do['we_do_heading_1']) ? $what_we_do['we_do_heading_1'] : '';
                    $dataArray['we_do_heading_2']   = !empty($what_we_do['we_do_heading_2']) ? $what_we_do['we_do_heading_2'] : '';
                    $dataArray['we_do_description_1']   = !empty($what_we_do['we_do_description_1']) ? $what_we_do['we_do_description_1'] : '';
                    $dataArray['we_do_description_2']   = !empty($what_we_do['we_do_description_2']) ? $what_we_do['we_do_description_2'] : '';


                    $dataArray['form_caption'] = 'Edit about us page content';

                }
            }
            else
            {
                $dataArray['form_caption'] = 'Add about us page content';
            }
            $dataArray['flash_message'] = $this->session->flashdata('flash_message');
            $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $this->load->view('aboutUs/add', $dataArray);
        }
        else
        {

            $point_array = [
                'point_1' => $this->input->post('point_1'),
                'point_2' => $this->input->post('point_2'),
                'point_3' => $this->input->post('point_3'),
                'point_4' => $this->input->post('point_4'),
                'point_5' => $this->input->post('point_5'),
            ];

            $points = json_encode($point_array);

            $fun_facts_array = [
                'fun_percentage_1' => $this->input->post('fun_percentage_1'),
                'fun_percentage_2' => $this->input->post('fun_percentage_2'),
                'fun_percentage_3' => $this->input->post('fun_percentage_3'),
                'fun_percentage_4' => $this->input->post('fun_percentage_4'),
                'fun_point_1' => $this->input->post('fun_point_1'),
                'fun_point_2' => $this->input->post('fun_point_2'),
                'fun_point_3' => $this->input->post('fun_point_3'),
                'fun_point_4' => $this->input->post('fun_point_4'),
                'fun_description_1' => $this->input->post('fun_description_1'),
                'fun_description_2' => $this->input->post('fun_description_2'),
                'fun_description_3' => $this->input->post('fun_description_3'),
                'fun_description_4' => $this->input->post('fun_description_4'),
            ];

            $fun_facts = json_encode($fun_facts_array);

            $what_we_do_array = [
                'we_do_heading_1' => $this->input->post('we_do_heading_1'),
                'we_do_heading_2' => $this->input->post('we_do_heading_2'),
                'we_do_description_1' => $this->input->post('we_do_description_1'),
                'we_do_description_2' => $this->input->post('we_do_description_2'),
            ];

            $what_we_do = json_encode($what_we_do_array);

            //save Information data
            $arr_course = array(
                'id'        => $id,
                'content'      => $this->input->post('content'),
                'heading'      => $this->input->post('heading'),
                'points'      => $points,
                'fun_facts'      => $fun_facts,
                'what_we_do'      => $what_we_do,
            );

            $about_us_id = $this->About_us_model->save($arr_course);

            if ( ! empty($about_us_id))
            {
                $this->session->set_flashdata('flash_message', 'About Us page content successfully saved');
                redirect('admin/about-us/'.$about_us_id);
            }
            else
            {
                show_error('Some error occured go back and try again!');
            }
        }
    }



}
