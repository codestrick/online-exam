<?php

if ( ! defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

class Student extends MY_Controller
{

    private $_student_listing_headers = 'student_listing_headers';

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('common');
        $this->load->library('form_validation');
        $this->load->library('commonlib');
        $this->load->model('Student_model');
        $this->load->model('Course_model');
        $this->load->setTemplate('admin');
    }


    public function add_student($id = NULL)
    {
        $this->form_validation->set_rules('name', 'Student name', "required");
        $this->form_validation->set_rules('course_id', 'Course Name', "required");
        $this->form_validation->set_rules('email', 'Student email', "required");
        $this->form_validation->set_rules('mobile', 'Student Mobile', "required");
        $this->form_validation->set_rules('pincode', 'Student pin code', "required");
        $this->form_validation->set_rules('active_status', 'Student Status', "required");


        //check user session for storing user id in each student
        $session_data = $this->session->userdata('online_exam_admin');

        if(!empty($session_data['user']['user_id']))
        {
            $user_id = $session_data['user']['user_id'];
        }
        else
        {
            $this->session->set_flashdata('flash_message', 'Invalid operation performed.');
            $this->session->set_flashdata('flash_message_status', FALSE);
            redirect('admin/student/list-student');
        }

        if ($this->form_validation->run() == FALSE)
        {
            $dataArray['local_css'] = array(
                'select2',
            );
            $dataArray['local_js']  = array(
                'parsley',
                'select2',
                'student',
            );

            if ($id != NULL)
            {
                $param    = array('id' => $id);
                $student_det = $this->Student_model->get_student_detail($param);

                if (empty($student_det))
                {
                    $this->session->set_flashdata('flash_message', 'Invalid operation performed.');
                    $this->session->set_flashdata('flash_message_status', FALSE);
                    redirect('admin/student/list-student');
                }

                else
                {
                    $dataArray['id']     = $student_det['id'];
                    $dataArray['name']   = $student_det['name'];
                    $dataArray['email']   = $student_det['email'];
                    $dataArray['address']   = $student_det['address'];
                    $dataArray['mobile']   = $student_det['mobile'];
                    $dataArray['pincode']   = $student_det['pincode'];
                    $dataArray['active_status']   = $student_det['active_status'];
                    $dataArray['course_id']       = $student_det['course_id'];

                    $dataArray['user_id']   = $student_det['user_id'];

                    $dataArray['form_caption'] = 'Edit Student';

                    //check student editable or not for this user
                    $this->user_check_for_edit_student($dataArray['user_id'],$user_id);
                }
            }
            else
            {
                $dataArray['form_caption'] = 'Add Student';
            }

            $dataArray['all_courses'] = $this->Course_model->get_course_details(['user_id' => $user_id]);

            $this->load->view('student/student-form', $dataArray);
        }
        else
        {
            //save student data
            $arr_student = array(
                'id'            => $id,
                'name'          => $this->input->post('name'),
                'email'         => $this->input->post('email'),
                'mobile'        => $this->input->post('mobile'),
                'address'       => $this->input->post('address'),
                'password'      =>  md5('password'),
                'active_status' => $this->input->post('active_status'),
                'course_id' => $this->input->post('course_id'),
                'pincode'       => $this->input->post('pincode'),
            );

            if (empty($id))
            {
                $arr_student['user_id'] = $user_id;
                $arr_student['created_at'] = date('Y-m-d H:i:s');
                $arr_student['updated_at'] = date('Y-m-d H:i:s');
            }
            else
            {
                $arr_student['updated_at'] = date('Y-m-d H:i:s');

            }

            $return_value = $this->Student_model->save_student_details($arr_student);
            $student_id = $return_value['id'];
            if ( ! empty($student_id))
            {
                //For Create User ID
                if($return_value['type'] == 'insert'){
                    $created_student_id = '00'.$student_id;
                    $return_value = $this->Student_model->save_student_details(['id' => $student_id,'student_roll' => $created_student_id]);
                }
                $this->session->set_flashdata('flash_message', $id == NULL ? 'Student successfully created' : 'Student successfully updated');
                redirect('admin/student/list-student');
            }
            else
            {
                show_error('Some error occured go back and try again!');
            }
        }
    }

    private function user_check_for_edit_student($dataArrayUser_id,$user_id)
    {
        if($dataArrayUser_id != $user_id)
        {
            $this->session->set_flashdata('flash_message', 'Invalid operation performed.');
            $this->session->set_flashdata('flash_message_status', FALSE);
            redirect('admin/student/list-student');
        }

    }

    public function index()
    {
        $this->load->library('Datatable');

        $message        = $this->session->flashdata('flash_message');
        $message_status = $this->session->flashdata('flash_message_status');

        $table_config = array(
            'source'          => site_url('admin/student/list_student_json'),
            'datatable_class' => $this->config->config["datatable_class"],
        );

        $dataArray = array(
            'table'          => $this->datatable->make_table($this->_student_listing_headers, $table_config),
            'message'        => $message,
            'message_status' => $message_status
        );

        $dataArray['local_css'] = array(
            'dataTables.bootstrap4',
            'responsive.bootstrap4',
        );

        $dataArray['local_js'] = array(
            'jquery.dataTables',
            'dataTables.bootstrap4'
        );

        $dataArray['form_caption'] = 'List Student';

        $this->load->view('student/index', $dataArray);
    }

    public function list_student_json()
    {
        $this->load->library('Datatable');

        $arr  = $this->config->config[$this->_student_listing_headers];
        $cols = array_keys($arr);

        $pagingParams = $this->datatable->get_paging_params($cols);

        $resultdata = $this->Student_model->get_all_student($pagingParams);

        $json_output = $this->datatable->get_json_output($resultdata, $this->_student_listing_headers);

        $this->load->setTemplate('json');
        $this->load->view('json', $json_output);
    }

    public function delete_student($id)
    {
        $res = $this->Student_model->delete_student($id);

        $this->session->set_flashdata('flash_message', $res['msg']);
        $this->session->set_flashdata('flash_message_status', $res['status']);

        redirect('admin/student/list-student');

    }

}
