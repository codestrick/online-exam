<?php

if ( ! defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

class User extends MY_Controller
{

    private $_user_listing_headers = 'user_listing_headers';

    public function __construct()
    {
        parent::__construct();

        $this->load->setTemplate('blank');

        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('common');
        $this->load->library('form_validation');
        $this->load->library('commonlib');
        $this->load->model('User_model');
    }

    public function index()
    {
        redirect('/admin/user/validate/');
    }

    public function dashboard()
    {
        $this->load->library('Commonlibrary');
        $this->load->setTemplate('admin');
        $this->load->helper('url');

        $dataArray                   = array();
        $userdata                    = get_loggedin_user_data();
        $user_type                   = $userdata['type'];
        $dataArray['template_title'] = lang("dashboard") . ' | ' . lang("SITENAME");
        $message                     = $this->session->flashdata('user_operation_message');
        $dataArray['message']        = $message;

        $this->load->view('dashboard', $dataArray);
    }

    public function login()
    {
        $dataArray['flash_message']        = $this->session->flashdata('flash_message');
        $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

        $this->load->view('login-form', $dataArray);
    }

    public function logout()
    {
        $this->session->unset_userdata('online_exam_admin');

        redirect('admin/user/login', 'refresh');
    }

    public function validate()
    {

        $this->load->helper('url');
        $this->load->library('form_validation');

        $dataArray[] = array();

        $this->form_validation->set_rules('useremail', 'Useremail', 'required|min_length[4]|max_length[50]');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[4]|max_length[50]');

        if ($this->form_validation->run() == FALSE)
        {
            $dataArray['flash_message']        = $this->session->flashdata('flash_message');
            $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $this->load->view('login-form', $dataArray);
        }
        else
        {
            $encrypted_password = md5($this->input->post('password'));

            $userdata = $this->User_model->login($this->input->post('useremail'), $encrypted_password);

            if ($userdata === FALSE)
            {
                $message = $this->lang->line('error_login_auth');

                $this->session->set_flashdata('flash_message', $message);
                $this->session->set_flashdata('flash_message_status', FALSE);

                $dataArray['flash_message']        = $this->session->flashdata('flash_message');
                $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

                $this->load->view('login-form', $dataArray);
            }
            else
            {
                unset($userdata['password']);

                $existing_session = $this->session->userdata('online_exam_admin');

                $session_user_data = array(
                    'user' => array(
                        'id'         => $userdata['id'],
                        'user_id'    => $userdata['user_id'],
                        'name'       => $userdata['name'],
                        'username'   => $userdata['username'],
                        'email'      => $userdata['email'],
                        'type'       => $userdata['user_type'],
                        'status'     => $userdata['status'],
                    )
                );
                $existing_session  = array_merge($existing_session, $session_user_data);

                $this->session->set_userdata('online_exam_admin', $existing_session);
                $user_type = $userdata['user_type'];

                redirect(base_url('admin/dashboard'));
            }
        }
    }

    public function select($id){

        $param    = array('id' => $id);
        $user_det = $this->User_model->get_user_by_id($param);

        if (empty($user_det))
        {
            $this->session->set_flashdata('flash_message', 'Invalid operation performed.');
            $this->session->set_flashdata('flash_message_status', FALSE);
            redirect('admin/user/list-user');
        }
        else
        {
            $existing_session = $this->session->userdata('online_exam_admin');

            $session_user_data = array(
                'user' => array(
                    'id'         => $user_det['id'],
                    'user_id'    => $user_det['user_id'],
                    'name'       => $user_det['name'],
                    'username'   => $user_det['username'],
                    'email'      => $user_det['email'],
                    'type'       => 'super-admin',
                    'status'     => $user_det['status'],
                )
            );

            $existing_session  = array_merge($existing_session, $session_user_data);

            $this->session->set_userdata('online_exam_admin', $existing_session);

            redirect(base_url('admin/dashboard'));
        }
    }



    public function forgot_password()
    {
        $this->load->view('forgot-password');
    }

    /*
     * User Add, Update, Delete Section
     */

    public function add_user($id = NULL)
    {
        $this->load->setTemplate('admin');

        $session_user = get_login_user_session();

        if(!empty($session_user))
        {
           $type = $session_user['type'];

           if($type != 'super-admin')
           {
               redirect('admin/dashboard');
           }
        }

        $this->form_validation->set_rules('email', 'User Email ', "required|valid_email|unique[user.email.id.{$id}]");
        $this->form_validation->set_rules('username', 'Username', "required|unique[user.username.id.{$id}]");
        $this->form_validation->set_rules('mobile', 'Mobile No', "required|unique[user.mobile.id.{$id}]");
        $this->form_validation->set_rules('name', 'Name is ', 'required|min_length[3]');
        $this->form_validation->set_rules('user_type', 'User Type is ', 'required');

        if (empty($id))
        {
            $this->form_validation->set_rules('password', 'User Password is ', 'required');
        }
        if ($this->form_validation->run() == FALSE)
        {
            $dataArray['local_css'] = array(
                'select2',
            );
            $dataArray['local_js']  = array(
                'parsley',
                'select2',
                'user'
            );

            if ($id != NULL)
            {
                $param    = array('id' => $id);
                $user_det = $this->User_model->get_user_by_id($param);

                if (empty($user_det))
                {
                    $this->session->set_flashdata('flash_message', 'Invalid operation performed.');
                    $this->session->set_flashdata('flash_message_status', FALSE);
                    redirect('admin/user/list-user');
                }

                if ($user_det['user_type'] == 'super-admin')
                {

                    $this->session->set_flashdata('flash_message', "You Can't Edit/Update Super Admin details.");
                    $this->session->set_flashdata('flash_message_status', FALSE);

                    redirect('admin/user/list-user');
                }
                else
                {
                    $dataArray['id']  = $user_det['id'];
                    $dataArray['name']     = $user_det['name'];
                    $dataArray['email']    = $user_det['email'];
                    $dataArray['mobile']    = $user_det['mobile'];
                    $dataArray['username'] = $user_det['username'];
                    $dataArray['usertype'] = $user_det['user_type'];

                    $dataArray['form_caption'] = 'Edit User';
                }
            }
            else
            {
                $dataArray['form_caption'] = 'Add User';
            }

            $dataArray['user_type'] = getCustomConfigItem('user_type');

            unset($dataArray['user_type']['super-admin']);
            $this->load->view('user/user-form', $dataArray);
        }
        else
        {
            //save user data
            $arr_user = array(
                'id'        => $id,
                'email'     => $this->input->post('email'),
                'name'      => $this->input->post('name'),
                'username'  => $this->input->post('username'),
                'mobile'    => $this->input->post('mobile'),
                'user_type' => $this->input->post('user_type'),
                'status'    => 'active',
                'exam_status'    => 'inactive',
            );

            $password = $this->input->post('password');

            if ( ! empty($password))
            {
                $arr_user['password'] = md5($password);
            }
            if (empty($id))
            {
                $arr_user['created_at'] = date('Y-m-d H:i:s');
            }

            $return_value = $this->User_model->save_user($arr_user);

            $user_id = $return_value['id'];

            if ( ! empty($user_id))
            {
                //For Create User ID
                if($return_value['type'] == 'insert'){
                    $created_user_id = 'SUB00'.$user_id;
                    $return_value = $this->User_model->save_user(['id' => $user_id,'user_id' => $created_user_id]);
                }

                $this->session->set_flashdata('flash_message', $id == NULL ? 'User successfully created' : 'User successfully updated');
                redirect('admin/user/list-user');
            }
            else
            {
                show_error('Some error occured go back and try again!');
            }
        }
    }

    public function list_user()
    {
        $session_user = get_login_user_session();

        if(!empty($session_user))
        {
            $type = $session_user['type'];

            if($type != 'super-admin')
            {
                redirect('admin/dashboard');
            }
        }

        $this->load->setTemplate('admin');

        $this->load->library('Datatable');

        $message        = $this->session->flashdata('flash_message');
        $message_status = $this->session->flashdata('flash_message_status');

        $table_config = array(
            'source'          => site_url('admin/user/list_user_json'),
            'datatable_class' => $this->config->config["datatable_class"],
        );

        $dataArray = array(
            'table'          => $this->datatable->make_table($this->_user_listing_headers, $table_config),
            'message'        => $message,
            'message_status' => $message_status
        );

        $dataArray['local_css'] = array(
            'dataTables.bootstrap4',
            'responsive.bootstrap4',
        );

        $dataArray['local_js'] = array(
            'jquery.dataTables',
            'dataTables.bootstrap4'
        );

        $dataArray['form_caption'] = 'List User';

        $this->load->view('user/index', $dataArray);
    }

    public function list_user_json()
    {
        $this->load->library('Datatable');

        $arr  = $this->config->config[$this->_user_listing_headers];
        $cols = array_keys($arr);

        $pagingParams = $this->datatable->get_paging_params($cols);

        $resultdata = $this->User_model->get_all_user($pagingParams);

        $json_output = $this->datatable->get_json_output($resultdata, $this->_user_listing_headers);

        $this->load->setTemplate('json');
        $this->load->view('json', $json_output);
    }

    public function delete_user($id)
    {
        $this->load->setTemplate('admin');

        $params = array(
            'id' => $id,
        );

        $user_det = $this->User_model->get_user_by_id($params);

        if ($user_det['user_type'] == 'super-admin')
        {
            $this->session->set_flashdata('flash_message', "You Can't remove Super Admin.");
            $this->session->set_flashdata('flash_message_status', FALSE);

            redirect('admin/user/list-user');
        }
        else
        {
            $res = $this->User_model->delete_user($id);

            $this->session->set_flashdata('flash_message', $res['msg']);
            $this->session->set_flashdata('flash_message_status', $res['status']);

            if ($res['status'] == TRUE)
            {
                $this->User_model->delete_cinema_admin($id);
            }

            redirect('admin/user/list-user');
        }
    }

    public function change_password()
    {
        $this->form_validation->set_rules('old_password', 'Old Password ', "required");
        $this->form_validation->set_rules('new_password', 'New Password ', "required");
        $this->form_validation->set_rules('re_enter_new_password', 'Re-enter Password ', "required");

        $this->load->setTemplate('admin');

        $user_id = get_user_id();

        if (empty($user_id))
        {
            redirect('admin/dashboard');
        }

        $params = array('user_id' => $user_id);

        $user_det      = $this->User_model->get_user_by_id($params);

        if ($this->form_validation->run() == TRUE)
        {
            $old_password      = $this->input->post('old_password');
            $new_password      = $this->input->post('new_password');
            $re_enter_new_password = $this->input->post('re_enter_new_password');

            if ($new_password == $re_enter_new_password)
            {
                $old_password = md5($old_password);

                if ($user_det['password'] == $old_password)
                {
                    $new_password = md5($new_password);
                    $params       = array('password' => $new_password, 'updated_at' => date("Y-m-d H:i:s"),);

                    $status = $this->User_model->update_user_using_user_id($params, $user_id);

                    if ($status == TRUE)
                    {
                        $this->session->set_flashdata('flash_message','Password Change successful');
                        $this->session->set_flashdata('flash_message_status',true);
                        redirect('admin/change-password');
                    }
                }
                else
                {
                    $this->session->set_flashdata('flash_message','Old password  incorrect');
                    $this->session->set_flashdata('flash_message_status',false);
                }
            }
            else
            {
                $this->session->set_flashdata('flash_message','Re-enter password not match');
                $this->session->set_flashdata('flash_message_status',false);
            }

        }
        else
        {
            $dataArray['local_css'] = array();
            $dataArray['local_js'] = array('change-password');

            $data['flash_message'] = $this->session->flashdata('flash_message');
            $data['flash_message_status'] = $this->session->flashdata('flash_message_status');
            $data['form_action'] = '';
            $this->load->view('change-password', $data);
        }


    }

}
    