<?php

if ( ! defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

class BasicInformation extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('common');
        $this->load->library('form_validation');
        $this->load->library('commonlib');
        $this->load->model('BasicInformation_model');
        $this->load->setTemplate('admin');
    }


    public function index($id = NULL)
    {
        $session_data = $this->session->userdata('online_exam_admin');
        $user_id = $session_data['user']['user_id'];

        $BasicInformation = $this->BasicInformation_model->get_info_detail(['user_id' => $user_id]);

        if(!empty($BasicInformation['id'])){
            $id = $BasicInformation['id'];
        }

        $this->form_validation->set_rules('phone_number', 'Phone Number', "required");
        $this->form_validation->set_rules('email', 'Email', "required");
        $this->form_validation->set_rules('address', 'Address', "required");
        $this->form_validation->set_rules('opening_time', 'Time', "required");
        $this->form_validation->set_rules('institution_name', 'Institution Name', "required");


        if ($this->form_validation->run() == FALSE)
        {
            $dataArray['local_css'] = array(
                'select2',
            );
            $dataArray['local_js']  = array(
                'parsley',
                'select2',
            );

            if ($id != NULL)
            {
                $BasicInformation = $this->BasicInformation_model->get_info_detail(['id' => $id,'user_id' => $user_id]);
                if (empty($BasicInformation))
                {
                    $this->session->set_flashdata('flash_message', 'Invalid operation performed.');
                    $this->session->set_flashdata('flash_message_status', FALSE);
                    redirect('admin/basic-information');
                }
                else
                {
                    $dataArray['id']     = $BasicInformation['id'];
                    $dataArray['phone_number']   = $BasicInformation['phone_number'];
                    $dataArray['email']   = $BasicInformation['email'];
                    $dataArray['address']   = $BasicInformation['address'];
                    $dataArray['fax']   = $BasicInformation['fax'];
                    $dataArray['opening_time']   = $BasicInformation['opening_time'];
                    $dataArray['institution_name']   = $BasicInformation['institution_name'];

                    $dataArray['contact_description']   = $BasicInformation['contact_description'];

                    $our_student = json_decode($BasicInformation['our_student'],1);

                    $dataArray['heading']   = $our_student['heading'];
                    $dataArray['content']   = $our_student['content'];
                    $dataArray['point_1']   = $our_student['point_1'];
                    $dataArray['point_2']   = $our_student['point_2'];
                    $dataArray['point_3']   = $our_student['point_3'];
                    $dataArray['point_4']   = $our_student['point_4'];
                    $dataArray['point_5']   = $our_student['point_5'];


                    $dataArray['form_caption'] = 'Edit Information';

                }
            }
            else
            {
                $dataArray['form_caption'] = 'Add Information';
            }
            $dataArray['flash_message'] = $this->session->flashdata('flash_message');
            $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $this->load->view('basicInformation/add', $dataArray);
        }
        else
        {

            $our_student_array = [
                'heading' => $this->input->post('heading'),
                'content' => $this->input->post('content'),
                'point_1' => $this->input->post('point_1'),
                'point_2' => $this->input->post('point_2'),
                'point_3' => $this->input->post('point_3'),
                'point_4' => $this->input->post('point_4'),
                'point_5' => $this->input->post('point_5'),
            ];

            $our_student = json_encode($our_student_array);

            //save Information data
            $arr_course = array(
                'id'        => $id,
                'user_id'        => $user_id,
                'phone_number'      => $this->input->post('phone_number'),
                'address'      => $this->input->post('address'),
                'email'      => $this->input->post('email'),
                'fax'      => $this->input->post('fax'),
                'opening_time'      => $this->input->post('opening_time'),
                'institution_name'      => $this->input->post('institution_name'),
                'contact_description'      => $this->input->post('contact_description'),
                'our_student'      => $our_student,
            );


            $information_id = $this->BasicInformation_model->save_info($arr_course);

            if ( ! empty($information_id))
            {
                $this->session->set_flashdata('flash_message', $id == NULL ? 'Basic Information successfully created' : 'Basic Information successfully updated');
                redirect('admin/basic-information/'.$information_id);
            }
            else
            {
                show_error('Some error occured go back and try again!');
            }
        }
    }



}
