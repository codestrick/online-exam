<?php

if ( ! defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

class Settings extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('common');
        $this->load->library('form_validation');
        $this->load->library('commonlib');
        $this->load->model('Question_model');
        $this->load->model('Course_model');
        $this->load->model('Settings_model');
        $this->load->setTemplate('admin');
    }

    public function settings_create()
    {
        $user_id = get_user_id();

        $all_course = $this->Course_model->get_course_detail_result_array(['user_id' => $user_id]);

        $settings_array = [];
        if(!empty($all_course))
        {
            foreach($all_course as $k=> $course)
            {
                $all_question = $this->Question_model->get_question_detail_result_array(['user_id' => $course['user_id'],'course_id' => $course['id'],'active_status' => 'active']);
                if(!empty($all_question))
                {

                    $settings_array[$course['id']] = array(
                        'course_id' => $course['id'],
                        'course_name' => $course['name'],
                        'total_question' => count($all_question),
                        'time' => count($all_question),
                        'fm' => count($all_question),
                        'exam-status' => 'off'
                    );
                }else
                {
                    $settings_array[$course['id']] = array(
                        'course_id' => $course['id'],
                        'course_name' => $course['name'],
                        'total_question' => 0,
                        'time' => '0',
                        'fm' => 0,
                        'exam-status' => 'off'
                    );
                }

            }
        }

        return $settings_array;

    }

    public function index()
    {
        $user_id = get_user_id();

        if(empty($user_id))
        {
            $this->session->set_flashdata('flash_message', 'Invalid operation performed.');
            $this->session->set_flashdata('flash_message_status', FALSE);
            redirect('admin');
        }

        $course_det = $this->Course_model->get_course_detail_result_array(['user_id' => $user_id]);

        if(empty($course_det))
        {
            $this->session->set_flashdata('flash_message', 'Please Insert a course');
            $this->session->set_flashdata('flash_message_status', FALSE);
            redirect('admin/course/add-course');
        }

        $items = $this->input->post('item');

        if ( ! empty($items))
        {
            $count = 1;
            foreach ($items as $index => $v)
            {
                $item_index = $index + 1;

                $course_name = !empty($v['course_name']) ? $v['course_name'] : $count;

                $this->form_validation->set_rules('item[' . $index . '][course_id]', '*' . $course_name . ' Course ID', 'required|numeric');
                $this->form_validation->set_rules('item[' . $index . '][course_name]', '*' . $course_name . ' Course Name', 'required');
                $this->form_validation->set_rules('item[' . $index . '][total_question]', '*' . $course_name . ' Total question', 'required|numeric');
                $this->form_validation->set_rules('item[' . $index . '][fm]', '*' . $course_name . ' Full marks', 'required|numeric');
                $this->form_validation->set_rules('item[' . $index . '][time]', '*' . $course_name . ' Time', 'required|numeric');
                $this->form_validation->set_rules('item[' . $index . '][exam-status]', '*' . $course_name . ' Exam Status', 'required');

                if($v['fm'] < 9 || $v['total_question'] < 9)
                {
                    $items[$index]['exam-status'] = 'off';
                }

                if($v['total_question'] < $v['fm'])
                {
                    $items[$index]['fm'] = $v['total_question'];
                    $items[$index]['time'] = $v['total_question'];
                }

                $count++;
            }

        }

        if($this->form_validation->run() == FALSE)
        {
            $settings_details =  $this->Settings_model->get_question_detail(['user_id' => $user_id]);

            if(!empty($settings_details))
            {
                $settings_array = json_decode($settings_details['settings'],1);
                //check if new course added in database or not
                $new_settings_array = $this->settings_create();

                $settings_count = count($settings_array);
                $new_settings_count = count($new_settings_array);

                if($settings_count != $new_settings_count)
                {
                    $settings_array_keys = array_keys($settings_array);
                    $new_settings_array_keys = array_keys($new_settings_array);


                    foreach($new_settings_array_keys as $k => $val)
                    {
                        if(!in_array($val,$settings_array_keys))
                        {
                            $settings_array[$val] = $new_settings_array[$val];
                        }
                    }
                }

                foreach($new_settings_array as $k => $val)
                {

                    if($settings_array[$k]['total_question'] != $val['total_question']){

                        $settings_array[$k]['total_question'] = $val['total_question'];

                        if($settings_array[$k]['total_question'] < $settings_array[$k]['fm'])
                        {
                            $settings_array[$k]['fm'] = $settings_array[$k]['total_question'];
                        }
                    }
                }

            }
            else
            {
                $settings_array = $this->settings_create();
            }

            $dataArray['local_css'] = array();
            $dataArray['local_js'] = array('settings');

            $dataArray['flash_message'] = $this->session->flashdata('flash_message');
            $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');
            $dataArray['settings_array'] = $settings_array;
            $dataArray['form_caption'] = 'Settings';

            $this->load->view('settings/settings-form', $dataArray);
        }
        else
        {
            $items_json = json_encode($items);
            $settings_details =  $this->Settings_model->get_question_detail(['user_id' => $user_id]);

            if(!empty($settings_details))
            {
                $params = array(
                    'user_id'      => $user_id,
                    'settings'       => $items_json,
                    'updated_at' => date('Y-m-d H:i:s')
                );

                $cond = ['user_id' => $user_id];

                $status = $this->Settings_model->update_settings_details($params,$cond);
            }
            else
            {
                $params = array(
                    'user_id'      => $user_id,
                    'settings'       => $items_json,
                    'updated_at' => date('Y-m-d H:i:s')
                );

                $status = $this->Settings_model->insert_settings_details($params);

            }

            if($status)
            {
                $this->session->set_flashdata('flash_message','Settings successfully updated');
                $this->session->set_flashdata('flash_message_status',true);
            }else{
                $this->session->set_flashdata('flash_message','Some Error occurred!');
                $this->session->set_flashdata('flash_message_status',false);
            }
            redirect('admin/settings');
        }





    }
}
