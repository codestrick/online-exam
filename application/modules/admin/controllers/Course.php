<?php

if ( ! defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

class Course extends MY_Controller
{

    private $_course_listing_headers = 'course_listing_headers';

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('common');
        $this->load->library('form_validation');
        $this->load->library('commonlib');
        $this->load->model('Course_model');
        $this->load->setTemplate('admin');
    }


    public function add_course($id = NULL)
    {
        $this->form_validation->set_rules('name', 'Username', "required");

        //check user session for storing user id in each course
        $session_data = $this->session->userdata('online_exam_admin');

        if(!empty($session_data['user']['user_id']))
        {
            $user_id = $session_data['user']['user_id'];
        }
        else
        {
            $this->session->set_flashdata('flash_message', 'Invalid operation performed.');
            $this->session->set_flashdata('flash_message_status', FALSE);
            redirect('admin/course/list-course');
        }

        if ($this->form_validation->run() == FALSE)
        {
            $dataArray['local_css'] = array(
                'select2',
            );
            $dataArray['local_js']  = array(
                'parsley',
                'select2',
                'course'
            );

            if ($id != NULL)
            {
                $param    = array('id' => $id);
                $course_det = $this->Course_model->get_course_detail($param);

                if (empty($course_det))
                {
                    $this->session->set_flashdata('flash_message', 'Invalid operation performed.');
                    $this->session->set_flashdata('flash_message_status', FALSE);
                    redirect('admin/course/list-course');
                }

                else
                {
                    $dataArray['id']     = $course_det['id'];
                    $dataArray['name']   = $course_det['name'];

                    $dataArray['user_id']   = $course_det['user_id'];

                    $dataArray['form_caption'] = 'Edit Course';

                    //check course editable or not for this user
                    $this->user_check_for_edit_course($dataArray['user_id'],$user_id);
                }
            }
            else
            {
                $dataArray['form_caption'] = 'Add Course';
            }
            $dataArray['flash_message'] = $this->session->flashdata('flash_message');
            $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $this->load->view('course/course-form', $dataArray);
        }
        else
        {
            //save course data
            $arr_course = array(
                'id'        => $id,
                'name'      => $this->input->post('name'),
            );

            if (empty($id))
            {
                $arr_course['user_id'] = $user_id;
                $arr_course['created_at'] = date('Y-m-d H:i:s');
                $arr_course['updated_at'] = date('Y-m-d H:i:s');
            }
        else
            {
                $arr_course['updated_at'] = date('Y-m-d H:i:s');

            }

            $course_id = $this->Course_model->save_course($arr_course);

            if ( ! empty($course_id))
            {
                $this->session->set_flashdata('flash_message', $id == NULL ? 'Course successfully created' : 'Course successfully updated');
                redirect('admin/course/list-course');
            }
            else
            {
                show_error('Some error occured go back and try again!');
            }
        }
    }

    private function user_check_for_edit_course($dataArrayUser_id,$user_id)
    {
        if($dataArrayUser_id != $user_id)
        {
            $this->session->set_flashdata('flash_message', 'Invalid operation performed.');
            $this->session->set_flashdata('flash_message_status', FALSE);
            redirect('admin/course/list-course');
        }

    }

    public function index()
    {
        $this->load->library('Datatable');

        $message        = $this->session->flashdata('flash_message');
        $message_status = $this->session->flashdata('flash_message_status');

        $table_config = array(
            'source'          => site_url('admin/course/list_course_json'),
            'datatable_class' => $this->config->config["datatable_class"],
        );

        $dataArray = array(
            'table'          => $this->datatable->make_table($this->_course_listing_headers, $table_config),
            'message'        => $message,
            'message_status' => $message_status
        );

        $dataArray['local_css'] = array(
            'dataTables.bootstrap4',
            'responsive.bootstrap4',
        );

        $dataArray['local_js'] = array(
            'jquery.dataTables',
            'dataTables.bootstrap4'
        );

        $dataArray['form_caption'] = 'List Course';

        $this->load->view('course/index', $dataArray);
    }

    public function list_course_json()
    {
        $this->load->library('Datatable');

        $arr  = $this->config->config[$this->_course_listing_headers];
        $cols = array_keys($arr);

        $pagingParams = $this->datatable->get_paging_params($cols);

        $resultdata = $this->Course_model->get_all_course($pagingParams);

        $json_output = $this->datatable->get_json_output($resultdata, $this->_course_listing_headers);

        $this->load->setTemplate('json');
        $this->load->view('json', $json_output);
    }

    public function delete_user($id)
    {
        $this->load->setTemplate('admin');

        $params = array(
            'id' => $id,
        );

        $user_det = $this->Course_model->get_user_by_id($params);

        if ($user_det['user_type'] == 'super-admin')
        {
            $this->session->set_flashdata('flash_message', "You Can't remove Super Admin.");
            $this->session->set_flashdata('flash_message_status', FALSE);

            redirect('admin/user/list-user');
        }
        else
        {
            $res = $this->Course_model->delete_user($id);

            $this->session->set_flashdata('flash_message', $res['msg']);
            $this->session->set_flashdata('flash_message_status', $res['status']);

            if ($res['status'] == TRUE)
            {
                $this->Course_model->delete_cinema_admin($id);
            }

            redirect('admin/user/list-user');
        }
    }

}
