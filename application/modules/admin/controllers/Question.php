<?php

if ( ! defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

class Question extends MY_Controller
{

    private $_question_listing_headers = 'question_listing_headers';

    public function __construct()
    {
        parent::__construct();

        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('common');
        $this->load->helper('download');

        $this->load->library('form_validation');
        $this->load->library('excel_reader');
        $this->load->library('commonlib');
        $this->load->library('commonlibrary');

        $this->load->model('Question_model');
        $this->load->model('Course_model');
        $this->load->model('Settings_model');

        $this->load->setTemplate('admin');
    }


    public function add_question($id = NULL)
    {
        $this->form_validation->set_rules('question', 'Question', "required");
        $this->form_validation->set_rules('option_1', 'Option 1', "required");
        $this->form_validation->set_rules('option_2', 'Option 2', "required");
        $this->form_validation->set_rules('option_3', 'Option 3', "required");
        $this->form_validation->set_rules('option_4', 'Option 4', "required");
        $this->form_validation->set_rules('right_option', 'Right Option', "required");
        $this->form_validation->set_rules('active_status', 'Active Status', "required");
        $this->form_validation->set_rules('course_id', 'Course Id', "required");

        //check user session for storing user id in each question
        $session_data = $this->session->userdata('online_exam_admin');

        if ( ! empty($session_data['user']['user_id']))
        {
            $user_id = $session_data['user']['user_id'];
        }
        else
        {
            $this->session->set_flashdata('flash_message', 'Invalid operation performed.');
            $this->session->set_flashdata('flash_message_status', FALSE);
            redirect('admin/question/list-question');
        }

        if ($this->form_validation->run() == FALSE)
        {
            $dataArray['local_css'] = array(
                'select2',
            );
            $dataArray['local_js']  = array(
                'parsley',
                'select2',
                'question',
            );

            if ($id != NULL)
            {
                $param        = array('id' => $id);
                $question_det = $this->Question_model->get_question_detail($param);

                if (empty($question_det))
                {
                    $this->session->set_flashdata('flash_message', 'Invalid operation performed.');
                    $this->session->set_flashdata('flash_message_status', FALSE);
                    redirect('admin/question/list-question');
                }
                else
                {
                    $dataArray['id']            = $question_det['id'];
                    $dataArray['course_id']     = $question_det['course_id'];
                    $dataArray['question']      = $question_det['question'];
                    $dataArray['option_1']      = $question_det['option_1'];
                    $dataArray['option_2']      = $question_det['option_2'];
                    $dataArray['option_3']      = $question_det['option_3'];
                    $dataArray['option_4']      = $question_det['option_4'];
                    $dataArray['right_option']  = $question_det['right_option'];
                    $dataArray['active_status'] = $question_det['active_status'];
                    $dataArray['course_id']     = $question_det['course_id'];

                    $dataArray['form_caption'] = 'Edit Question';
                }
            }
            else
            {
                $dataArray['form_caption'] = 'Add Question';
            }

            $dataArray['all_courses'] = $all_courses = $this->Course_model->get_course_details(['user_id' => $user_id]);

            $dataArray['bulk_upload'] = $this->load->viewPartial('question/bulk-upload', ['all_courses' => $all_courses, 'redirect_to' => 'add-question']);

            $this->load->view('question/question-form', $dataArray);
        }
        else
        {
            //save course data
            $arr_course = array(
                'id'            => $id,
                'course_id'     => $this->input->post('course_id'),
                'question'      => $this->input->post('question'),
                'option_1'      => $this->input->post('option_1'),
                'option_2'      => $this->input->post('option_2'),
                'option_3'      => $this->input->post('option_3'),
                'option_4'      => $this->input->post('option_4'),
                'right_option'  => $this->input->post('right_option'),
                'active_status' => $this->input->post('active_status'),
            );

            if (empty($id))
            {
                $arr_course['user_id']    = $user_id;
                $arr_course['created_at'] = date('Y-m-d H:i:s');
                $arr_course['updated_at'] = date('Y-m-d H:i:s');
            }
            else
            {
                $arr_course['updated_at'] = date('Y-m-d H:i:s');

            }

            $question_id = $this->Question_model->save_question_details($arr_course);

            if ( ! empty($question_id))
            {
                $this->settings_update();
                $this->session->set_flashdata('flash_message', $id == NULL ? 'Question successfully created' : 'Question successfully updated');
                redirect('admin/question/list-question');
            }
            else
            {
                show_error('Some error occurred go back and try again!');
            }
        }
    }

    private function settings_update()
    {
        $user_id          = get_user_id();
        $settings_details = $this->Settings_model->get_question_detail(['user_id' => $user_id]);

        if ( ! empty($settings_details))
        {
            $settings_array = json_decode($settings_details['settings'], 1);

            foreach ($settings_array as $k => $val)
            {
                $all_question       = $this->Question_model->get_question_detail_result_array(['user_id' => $user_id, 'course_id' => $val['course_id'], 'active_status' => 'active']);
                $all_question_count = count($all_question);

                $settings_array[$k]['total_question'] = $all_question_count;
            }

            $settings_details_json = json_encode($settings_array);


            $params = array(
                'user_id'    => $user_id,
                'settings'   => $settings_details_json,
                'updated_at' => date('Y-m-d H:i:s')
            );

            $cond = ['user_id' => $user_id];

            $status = $this->Settings_model->update_settings_details($params, $cond);
        }
    }

    public function index()
    {
        $this->load->library('Datatable');

        $message        = $this->session->flashdata('flash_message');
        $message_status = $this->session->flashdata('flash_message_status');

        $table_config = array(
            'source'          => site_url('admin/question/list_question_json'),
            'datatable_class' => $this->config->config["datatable_class"],
        );

        $dataArray = array(
            'table'          => $this->datatable->make_table($this->_question_listing_headers, $table_config),
            'message'        => $message,
            'message_status' => $message_status
        );

        $dataArray['local_css'] = array(
            'dataTables.bootstrap4',
            'responsive.bootstrap4',
        );

        $dataArray['local_js'] = array(
            'jquery.dataTables',
            'dataTables.bootstrap4'
        );

        $dataArray['form_caption'] = 'List Question';

        $all_courses = $this->Course_model->get_course_details([]);

        $dataArray['bulk_upload'] = $this->load->viewPartial('question/bulk-upload', ['all_courses' => $all_courses, 'redirect_to' => 'list-question']);

        $this->load->view('question/index', $dataArray);
    }

    public function list_question_json()
    {
        $this->load->library('Datatable');

        $arr  = $this->config->config[$this->_question_listing_headers];
        $cols = array_keys($arr);

        $pagingParams = $this->datatable->get_paging_params($cols);

        $resultdata = $this->Question_model->get_all_questions($pagingParams);

        $json_output = $this->datatable->get_json_output($resultdata, $this->_question_listing_headers);

        $this->load->setTemplate('json');
        $this->load->view('json', $json_output);
    }

    public function delete_question($id)
    {
        $res = $this->Question_model->delete_question(['id' => $id]);

        $this->session->set_flashdata('flash_message', $res['msg']);
        $this->session->set_flashdata('flash_message_status', $res['status']);

        redirect('admin/question/list-question');
    }

    /**
     * Distributor bulk insert
     */
    public function bulk_question_add()
    {
        $redirect_to = $this->input->post('redirect_to');

        if ( ! empty($redirect_to))
        {
            $redirect_to = ($redirect_to == 'list-question' || $redirect_to == 'add-question') ? $redirect_to : 'list-question';
        }

        if ( ! file_exists(APPPATH . '../assets/file'))
        {
            mkdir(APPPATH . '../assets/file', 0777, TRUE);
        }

        if ($this->commonlibrary->is_file_uploaded('excel_file'))
        {
            $excel_upload = getCustomConfigItem('excel_upload');

            $this->load->library('upload', $excel_upload);
            $uploded_file_name = $this->upload->upload_file("excel_file", $excel_upload['upload_path'], $excel_upload);

            if (empty($uploded_file_name))
            {
                $this->session->set_flashdata('flash_message', $this->upload->display_errors());
                $this->session->set_flashdata('flash_message_status', FALSE);

                redirect('admin/question/' . $redirect_to);
            }
            else
            {
                $session_data = $this->session->userdata('online_exam_admin');

                if ( ! empty($session_data['user']['user_id']))
                {
                    $user_id = $session_data['user']['user_id'];
                }
                else
                {
                    $user_id = '';
                }

                $file_path = APPPATH . '../assets/file/' . $uploded_file_name;

                $results = $this->get_validate_data($file_path);

                $course_id = $this->input->post('course_id');

                if ( ! empty($results) && $results['status'] == TRUE)
                {
                    foreach ($results['data'] as $k => $result)
                    {
                        $params = array(
                            'course_id'     => $course_id,
                            'question'      => $result[0],
                            'option_1'      => $result[1],
                            'option_2'      => $result[2],
                            'option_3'      => $result[3],
                            'option_4'      => $result[4],
                            'right_option'  => is_numeric($result[5]) ? $result[5] : '',
                            'active_status' => 'Active',
                            'user_id'       => $user_id,
                            'created_at'    => date("Y-m-d H:i:s"),
                            'updated_at'    => date("Y-m-d H:i:s"),
                        );

                        $question_id[] = $this->Question_model->save_question_by_condition($params);
                    }

                    if ( ! empty($question_id))
                    {
                        $this->session->set_flashdata('flash_message', 'Questions created successfully');
                        $this->session->set_flashdata('flash_message_status', TRUE);

                        redirect('admin/question/list-question');
                    }
                    else
                    {
                        $this->session->set_flashdata('flash_message', 'Already exist.');
                        $this->session->set_flashdata('flash_message_status', FALSE);

                        redirect(get_uri_segment() . '/question/' . $redirect_to);
                    }
                }
                else
                {
                    $this->session->set_flashdata('flash_message', $results['message']);
                    $this->session->set_flashdata('flash_message_status', FALSE);

                    redirect('admin/question/' . $redirect_to);
                }
            }
        }
        else
        {
            $this->session->set_flashdata('flash_message', 'File Required!');
            $this->session->set_flashdata('flash_message_status', FALSE);

            redirect('admin/question/' . $redirect_to);
        }
    }

    /**
     * @param null $file_path
     *
     * @return array
     */
    private function get_validate_data($file_path = NULL)
    {
        if ( ! empty($file_path))
        {
            $all_cols = array('Question', 'Option 1', 'Option 2', 'Option 3', 'Option 4', 'Right Option');

            $file = fopen($file_path, "r");

            $i = 0;

            $column_name_error = FALSE;

            $data         = [];
            $invalid_data = [];

            while ( ! feof($file))
            {
                $cols = $row = fgetcsv($file);

                if ($i == 0)
                {
                    foreach ($all_cols as $k => $v)
                    {
                        if ($cols[$k] != $all_cols[$k])
                        {
                            $column_name_error = TRUE;
                            break;
                        }
                    }
                }
                else
                {
                    if ($cols && $row)
                    {
                        if ( ! empty($row[0]) && ! empty($row[1]) &&
                             ! empty($row[2]) && ! empty($row[3]) &&
                             ! empty($row[4]) && ! empty($row[5])
                        )
                        {
                            $data[] = $row;
                        }
                        else
                        {
                            $invalid_data[] = $row;
                        }
                    }
                }

                $i++;
            }

            fclose($file);
            unlink($file_path);

            if ($column_name_error)
            {
                return ['status' => FALSE, 'message' => 'Invalid column names.', 'data' => []];
            }
            else
            {
                if ( ! empty($data))
                {
                    return ['status' => TRUE, 'data' => $data];
                }
                else
                {
                    return ['status' => FALSE, 'message' => 'No valid record found', 'data' => []];
                }
            }
        }
        else
        {
            return ['status' => FALSE, 'message' => 'Invalid operation performed!', 'data' => []];
        }
    }

    /**
     * Download Bulk Question insert DEMO FILE
     */
    public function download_demo_file()
    {
        $file = FCPATH . 'assets/admin/demo-data/demo-question.csv';

        if ( ! empty($file) && is_file($file))
        {
            force_download($file, NULL);
        }
        else
        {
            $this->session->set_flashdata('flash_message', 'Your requested file not found.');
            $this->session->set_flashdata('flash_message_status', FALSE);

            redirect('admin/question/list-question');
        }
    }

}
