<?php

if ( ! defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

class Result_records extends MY_Controller
{

    private $_result_records_listing_headers = 'result_records_listing_headers';

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('common');
        $this->load->library('commonlib');
        $this->load->model('Result_records_model');
        $this->load->setTemplate('admin');
    }

    public function index()
    {
        $this->load->library('Datatable');

        $message        = $this->session->flashdata('flash_message');
        $message_status = $this->session->flashdata('flash_message_status');

        $table_config = array(
            'source'          => site_url('admin/result_records/list_results_records_json'),
            'datatable_class' => $this->config->config["datatable_class"],
        );

        $dataArray = array(
            'table'          => $this->datatable->make_table($this->_result_records_listing_headers, $table_config),
            'message'        => $message,
            'message_status' => $message_status
        );

        $dataArray['local_css'] = array(
            'dataTables.bootstrap4',
            'responsive.bootstrap4',
        );

        $dataArray['local_js'] = array(
            'jquery.dataTables',
            'dataTables.bootstrap4'
        );

        $dataArray['form_caption'] = 'List Result records';

        $this->load->view('result_records/index', $dataArray);
    }

    public function list_results_records_json()
    {
        $this->load->library('Datatable');

        $arr  = $this->config->config[$this->_result_records_listing_headers];
        $cols = array_keys($arr);

        $pagingParams = $this->datatable->get_paging_params($cols);

        $resultdata = $this->Result_records_model->get_all_result_records($pagingParams);

        $json_output = $this->datatable->get_json_output($resultdata, $this->_result_records_listing_headers);

        $this->load->setTemplate('json');
        $this->load->view('json', $json_output);
    }

    public function delete_result_record($id)
    {
        $this->load->setTemplate('admin');

        $params = array(
            'id' => $id,
        );

        $user_det = $this->Course_model->get_user_by_id($params);

        if ($user_det['user_type'] == 'super-admin')
        {
            $this->session->set_flashdata('flash_message', "You Can't remove Super Admin.");
            $this->session->set_flashdata('flash_message_status', FALSE);

            redirect('admin/user/list-user');
        }
        else
        {
            $res = $this->Course_model->delete_user($id);

            $this->session->set_flashdata('flash_message', $res['msg']);
            $this->session->set_flashdata('flash_message_status', $res['status']);

            if ($res['status'] == TRUE)
            {
                $this->Course_model->delete_cinema_admin($id);
            }

            redirect('admin/user/list-user');
        }
    }

}
