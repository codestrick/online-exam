<?php

if ( ! defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

class Staff extends MY_Controller
{

    private $_staff_listing_headers = 'staff_listing_headers';

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('common');
        $this->load->library('form_validation');
        $this->load->library('upload');
        $this->load->library('commonlib');
        $this->load->model('Staff_model');
        $this->load->setTemplate('admin');
    }


    public function add_staff($id = NULL)
    {


        $this->form_validation->set_rules('name', 'Name', "required");
        $this->form_validation->set_rules('pic', 'Image', "validate_file_upload[staff_image_config]");

        $session_data = $this->session->userdata('online_exam_admin');

        $config = getCustomConfigItem('staff_image_config');

        $student_image = ! empty($_FILES['pic']['name']) ? $_FILES['pic']['name'] : NULL;

        $upload_status = false;
        $upload_file_name = '';


//        (! empty($image_upload_status['file_name']) ? $image_upload_status['file_name'] : null)
        if(!empty($session_data['user']['user_id']))
        {
            $user_id = $session_data['user']['user_id'];
        }
        else
        {
            $this->session->set_flashdata('flash_message', 'Invalid operation performed.');
            $this->session->set_flashdata('flash_message_status', FALSE);
            redirect('admin/staff/list-staff');
        }

        if ($this->form_validation->run() == FALSE)
        {
            $dataArray['local_css'] = array(
                'select2',
            );
            $dataArray['local_js']  = array(
                'parsley',
                'select2',
                'course'
            );

            if ($id != NULL)
            {
                $param    = array('id' => $id);
                $staff_det = $this->Staff_model->get_detail($param);

                if (empty($staff_det))
                {
                    $this->session->set_flashdata('flash_message', 'Invalid operation performed.');
                    $this->session->set_flashdata('flash_message_status', FALSE);
                    redirect('admin/staff/list-staff');
                }
                else
                {
                    $dataArray['id']     = $staff_det['id'];
                    $dataArray['name']   = $staff_det['name'];

                    $dataArray['user_id']   = $staff_det['user_id'];
                    $dataArray['pic']   = $staff_det['pic'];
                    $dataArray['email']   = $staff_det['email_id'];
                    $dataArray['mobile_number']   = $staff_det['mobile_number'];
                    $dataArray['description']   = $staff_det['description'];


                    $dataArray['form_caption'] = 'Edit Staff';

                    $this->user_check_for_edit_staff($dataArray['user_id'],$user_id);
                }
            }
            else
            {
                $dataArray['form_caption'] = 'Add Staff';
            }
            $dataArray['staff_image_config']   = $config;
            $dataArray['flash_message'] = $this->session->flashdata('flash_message');
            $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $this->load->view('staff/staff-form', $dataArray);
        }
        else
        {

            if($student_image != NULL){
                if ( ! is_dir($config['upload_path']))
                {
                    mkdir($config['upload_path'], 0755, TRUE);
                }

                $this->upload->initialize($config);

                if ($this->upload->do_upload('pic'))
                {
                    $image_upload_info = $this->upload->data();

                    $upload_status = true;

                    $upload_file_name = $image_upload_info['file_name'];
                }
            }

            $arr_staff = array(
                'id'        => $id,
                'name'      => $this->input->post('name'),
                'email_id'      => $this->input->post('email'),
                'mobile_number'      => $this->input->post('mobile_number'),
                'description'      => $this->input->post('description'),
            );

            if($upload_status == true){
                $arr_staff['pic'] = $upload_file_name;
            }

            if (empty($id))
            {
                $arr_staff['user_id'] = $user_id;
                $arr_staff['created_at'] = date('Y-m-d H:i:s');
                $arr_staff['updated_at'] = date('Y-m-d H:i:s');
            }
            else
            {
                $arr_staff['updated_at'] = date('Y-m-d H:i:s');

            }

            $staff_id = $this->Staff_model->save($arr_staff);

            if ( ! empty($staff_id))
            {
                $this->session->set_flashdata('flash_message', $id == NULL ? 'Staff successfully created' : 'Staff successfully updated');
                redirect('admin/staff/list-staff');
            }
            else
            {
                show_error('Some error occured go back and try again!');
            }
        }
    }

    private function user_check_for_edit_staff($dataArrayUser_id,$user_id)
    {
        if($dataArrayUser_id != $user_id)
        {
            $this->session->set_flashdata('flash_message', 'Invalid operation performed.');
            $this->session->set_flashdata('flash_message_status', FALSE);
            redirect('admin/staff/list-staff');
        }

    }

    public function index()
    {
        $this->load->library('Datatable');

        $message        = $this->session->flashdata('flash_message');
        $message_status = $this->session->flashdata('flash_message_status');

        $table_config = array(
            'source'          => site_url('admin/staff/list_staff_json'),
            'datatable_class' => $this->config->config["datatable_class"],
        );

        $dataArray = array(
            'table'          => $this->datatable->make_table($this->_staff_listing_headers, $table_config),
            'message'        => $message,
            'message_status' => $message_status
        );

        $dataArray['local_css'] = array(
            'dataTables.bootstrap4',
            'responsive.bootstrap4',
        );

        $dataArray['local_js'] = array(
            'jquery.dataTables',
            'dataTables.bootstrap4'
        );

        $dataArray['form_caption'] = 'List Staff';

        $this->load->view('staff/index', $dataArray);
    }

    public function list_staff_json()
    {
        $this->load->library('Datatable');

        $arr  = $this->config->config[$this->_staff_listing_headers];
        $cols = array_keys($arr);

        $pagingParams = $this->datatable->get_paging_params($cols);

        $resultdata = $this->Staff_model->get_all($pagingParams);

        $json_output = $this->datatable->get_json_output($resultdata, $this->_staff_listing_headers);

        $this->load->setTemplate('json');
        $this->load->view('json', $json_output);
    }


}
