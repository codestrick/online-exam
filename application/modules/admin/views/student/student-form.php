<!-- Start content -->
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title float-left">Student</h4>

                    <ol class="breadcrumb float-right">
                        <li class="breadcrumb-item"><a href="#">Student</a></li>
                        <li class="breadcrumb-item active"><?php echo empty($form_caption) ? "" : $form_caption; ?></li>
                    </ol>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->


        <div class="col-lg-12">
            <div class="card-box">
                <div class="card-box-heading">
                    <h2 class="card-box-heading-text"><?php echo empty($form_caption) ? "" : $form_caption; ?></h2>
                    <div class="heading-elements">
                        <a href="<?php echo base_url('admin/student/list-student') ?>" class="btn btn-purple waves-effect w-md waves-light" style="font-size: large">
                            <i class="fa fa-list">&nbsp;&nbsp; List Student</i>
                        </a>
                    </div>
                </div>

                <!--jquery validation error container-->
                <div id="errorContainer" class="alert">
                    <p>
                        Please correct the following errors and try again:
                        <button type="button" class="close text-right" data-dismiss="alert" aria-hidden="true">&times;</button>
                    </p>
                    <ul></ul>
                </div>

                <?php
                if ( ! empty($flash_message))
                {
                    $flash_message_status = ! isset($flash_message_status) ? 1 : $flash_message_status;
                    ?>
                    <div class="alert <?php echo $flash_message_status == 1 ? 'alert-success' : 'alert-danger' ?>">
                        <button type="button" class="close text-right" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?php echo $flash_message; ?>
                    </div>
                    <?php
                }

                $validation_errors = validation_errors();
                if ( ! empty($validation_errors))
                {
                    ?>
                    <div class="col-sm-12">
                        <div class="col-xs-12 alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert"
                                    aria-hidden="true">
                                &times;
                            </button>
                            <?php echo $validation_errors; ?>
                        </div>
                    </div>
                    <?php
                }
                ?>

                <?php
                echo form_open_multipart(current_url(), array("id" => "form", "class" => "form-valid", "role" => "form"));
                ?>
                <div class="form-group row">
                    <label for="portfolio_name" class="col-sm-2 form-control-label">Student Name<span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                        <?php
                        $data = array(
                            'name'                       => 'name',
                            'id'                         => 'name',
                            'value'                      => set_value('name', empty($name) ? '' : $name),
                            'class'                      => 'form-control',
                            'placeholder'                => 'Student Name',
                            'required data-msg-required' => 'Please provide a Student name.'
                        );
                        echo form_input($data);
                        ?>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="portfolio_name" class="col-sm-2 form-control-label">Student Email<span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                        <?php
                        $data = array(
                            'name'                       => 'email',
                            'id'                         => 'email',
                            'value'                      => set_value('email', empty($email) ? '' : $email),
                            'class'                      => 'form-control',
                            'placeholder'                => 'Student Email',
                            'required data-msg-required' => 'Please provide a Student Email.'
                        );
                        echo form_input($data);
                        ?>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="portfolio_name" class="col-sm-2 form-control-label">Student Mobile No.<span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                        <?php
                        $data = array(
                            'name'                       => 'mobile',
                            'id'                         => 'mobile',
                            'value'                      => set_value('mobile', empty($mobile) ? '' : $mobile),
                            'class'                      => 'form-control',
                            'placeholder'                => 'Student Mobile No.',
                            'required data-msg-required' => 'Please provide a Student Mobile No.'
                        );
                        echo form_input($data);
                        ?>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="portfolio_image" class="col-sm-2 form-control-label">Address</label>
                    <div class="col-sm-6">
                        <?php
                        $data = array(
                            'name'        => 'address',
                            'id'          => 'address',
                            'value'       => set_value('address', empty($address) ? '' : $address),
                            'class'       => 'form-control',
                            'placeholder' => 'Address',
                        );
                        echo form_textarea($data);
                        ?>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="portfolio_name" class="col-sm-2 form-control-label">Pin Code<span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                        <?php
                        $data = array(
                            'name'                       => 'pincode',
                            'id'                         => 'pincode',
                            'value'                      => set_value('pincode', empty($pincode) ? '' : $pincode),
                            'class'                      => 'form-control',
                            'placeholder'                => 'Pin Code',
                            'required data-msg-required' => 'Please provide a Pin Code'
                        );
                        echo form_input($data);
                        ?>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="portfolio_category" class="col-sm-2 form-control-label">Student Status<span class="text-danger">*</span></label>
                    <div class="col-sm-6">

                        <?php
                        $active_status_options = [
                            ''         => 'Select a Status',
                            'active'   => 'Active',
                            'inactive' => 'Inactive',
                        ];

                        echo form_dropdown('active_status', empty($active_status_options) ? '' : $active_status_options, empty($active_status) ? '' : $active_status, 'class="form-control select2" required data-msg-required="Please select a Question Status."');
                        ?>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="portfolio_category" class="col-sm-2 form-control-label">Select Course<span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                        <select name="course_id" id="course_id" class="form-control select2" required data-msg-required="Please Select a Course">
                            <option value="">Select A Course</option>
                            <?php
                            if ( ! empty($all_courses))
                            {
                                foreach ($all_courses as $k => $v)
                                {
                                    $selected = ($v['id'] == (! empty($course_id) ? $course_id : NULL) ? 'selected' : '');
                                    echo '<option value="' . $v['id'] . '"' . $selected . '>' . $v['name'] . '</option>';
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="form-group row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-primary waves-effect waves-light">
                            Save
                        </button>
                    </div>
                </div>
                <?php echo form_close(); ?>

            </div>

        </div> <!-- end col -->
    </div>
    <!-- end row -->

</div> <!-- container -->

</div>
<!-- content -->