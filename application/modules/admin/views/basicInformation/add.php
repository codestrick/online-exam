<!-- Start content -->
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title float-left">Basic Information</h4>

                    <ol class="breadcrumb float-right">
                        <li class="breadcrumb-item"><a href="#">Basic Information</a></li>
                        <li class="breadcrumb-item active"><?php echo empty($form_caption) ? "" : $form_caption; ?></li>
                    </ol>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->


        <div class="col-lg-12">
            <div class="card-box">
                <div class="card-box-heading">
                    <h2 class="card-box-heading-text"><?php echo empty($form_caption) ? "" : $form_caption; ?></h2>

                </div>
                <!--jquery validation error container-->
                <div id="errorContainer" class="alert">
                    <p>
                        Please correct the following errors and try again:
                        <button type="button" class="close text-right" data-dismiss="alert" aria-hidden="true">&times;</button>
                    </p>
                    <ul></ul>
                </div>

                <?php
                if ( ! empty($flash_message))
                {
                    $flash_message_status = ! isset($flash_message_status) ? 1 : $flash_message_status;
                    ?>
                    <div class="alert <?php echo $flash_message_status == 1 ? 'alert-success' : 'alert-danger' ?>">
                        <button type="button" class="close text-right" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?php echo $flash_message; ?>
                    </div>
                    <?php
                }

                $validation_errors = validation_errors();
                if ( ! empty($validation_errors))
                {
                    ?>
                    <div class="col-sm-12">
                        <div class="col-xs-12 alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert"
                                    aria-hidden="true">
                                &times;
                            </button>
                            <?php echo $validation_errors; ?>
                        </div>
                    </div>
                    <?php
                }
                ?>

                <?php
                echo form_open_multipart(current_url(), array("id" => "form", "class" => "form-valid", "role" => "form"));
                ?>
                <h4 style="text-align: center; color: green">Basic Information & Contact Information</h4>
                <div class="form-group row">
                    <label for="institution_name" class="col-sm-2 form-control-label">Institution Name<span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" name="institution_name" id="institution_name" value="<?php echo !empty($institution_name) ? $institution_name : $this->input->post('institution_name'); ?>" class="form-control"
                               placeholder="Institution Name" required data-msg-required="Please provide an Institution Name">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="phone_number" class="col-sm-2 form-control-label">Phone Number<span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                        <input type="number" name="phone_number" id="phone_number" value="<?php echo !empty($phone_number) ? $phone_number : $this->input->post('phone_number'); ?>" class="form-control"
                               placeholder="Phone Number" required data-msg-required="Please provide a Phone Number">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="email" class="col-sm-2 form-control-label">Email<span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                        <input type="email" name="email" id="email" value="<?php echo !empty($email) ? $email : $this->input->post('email'); ?>" class="form-control"
                               placeholder="Email" required data-msg-required="Please provide an email">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="address" class="col-sm-2 form-control-label">Address<span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" name="address" id="address" value="<?php echo !empty($address) ? $address : $this->input->post('address'); ?>" class="form-control"
                               placeholder="Address" required data-msg-required="Please provide an address">
                    </div>
                </div>


                <div class="form-group row">
                    <label for="fax" class="col-sm-2 form-control-label">Fax</label>
                    <div class="col-sm-6">
                        <input type="text" name="fax" id="fax" value="<?php echo !empty($fax) ? $fax : $this->input->post('fax'); ?>" class="form-control"
                               placeholder="Fax" data-msg-required="Please provide Fax No.">
                    </div>
                </div>


                <div class="form-group row">
                    <label for="opening_time" class="col-sm-2 form-control-label">Time<span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" name="opening_time" id="opening_time" value="<?php echo !empty($opening_time) ? $opening_time : $this->input->post('opening_time'); ?>" class="form-control"
                               placeholder="Opne and close time" data-msg-required="Opne and close time required">
                    </div>
                </div>


                <h4 style="text-align: center; color: green">Contact Description</h4>

                <div class="form-group row">
                    <label for="contact_description" class="col-sm-2 form-control-label">Contact Description<span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                        <textarea type="text" name="contact_description" id="contact_description" class="form-control"
                                  placeholder="Contact Description" required data-msg-required="Please provide an address"><?php echo !empty($contact_description) ? $contact_description : $this->input->post('contact_description'); ?></textarea>
                    </div>
                </div>

                <h4 style="text-align: center; color: green">Our Students</h4>

                <div class="form-group row">
                    <label for="heading" class="col-sm-2 form-control-label">Heading<span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" name="heading" id="heading" value="<?php echo !empty($heading) ? $heading : $this->input->post('heading'); ?>" class="form-control"
                               placeholder="Heading" required data-msg-required="Please provide a Heading">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="content" class="col-sm-2 form-control-label">Content<span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                        <textarea type="text" name="content" id="content" class="form-control" placeholder="Content" required data-msg-required="Please provide a Phone Number"><?php echo !empty($content) ? $content : $this->input->post('content'); ?></textarea>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="point_1" class="col-sm-2 form-control-label">Point 1</label>
                    <div class="col-sm-6">
                        <input type="text" name="point_1" id="point_1" value="<?php echo !empty($point_1) ? $point_1 : $this->input->post('point_1'); ?>" class="form-control"
                               placeholder="Point">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="heading" class="col-sm-2 form-control-label">Point 2</label>
                    <div class="col-sm-6">
                        <input type="text" name="point_2" id="point_2" value="<?php echo !empty($point_2) ? $point_2 : $this->input->post('point_2'); ?>" class="form-control"
                               placeholder="Point">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="point_3" class="col-sm-2 form-control-label">Point 3</label>
                    <div class="col-sm-6">
                        <input type="text" name="point_3" id="point_3" value="<?php echo !empty($point_3) ? $point_3 : $this->input->post('point_3'); ?>" class="form-control"
                               placeholder="Point">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="point_4" class="col-sm-2 form-control-label">Point 4</label>
                    <div class="col-sm-6">
                        <input type="text" name="point_4" id="point_4" value="<?php echo !empty($point_4) ? $point_4 : $this->input->post('point_4'); ?>" class="form-control"
                               placeholder="Point">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="point_5" class="col-sm-2 form-control-label">Point 5</label>
                    <div class="col-sm-6">
                        <input type="text" name="point_5" id="point_5" value="<?php echo !empty($point_5) ? $point_5 : $this->input->post('point_5'); ?>" class="form-control"
                               placeholder="Point">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-primary waves-effect waves-light">
                            Save
                        </button>
                    </div>
                </div>
                <?php echo form_close(); ?>

            </div>

        </div> <!-- end col -->
    </div>
    <!-- end row -->

</div> <!-- container -->

</div>
<!-- content -->