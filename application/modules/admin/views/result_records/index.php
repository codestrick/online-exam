<!-- Start content -->
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title float-left">Result Records</h4>

                    <ol class="breadcrumb float-right">
                        <li class="breadcrumb-item"><a href="#">Result Records</a></li>
                        <li class="breadcrumb-item active">List Result Records</li>
                    </ol>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->


        <div class="row">
            <div class="col-md-12">
                <div class="card-box">
                    <div class="card-box-heading">
                        <h2 class="card-box-heading-text"><?php echo empty($form_caption) ? "" : $form_caption; ?></h2>
                        <div class="heading-elements"></div>
                    </div>

                    <?php
                    if ( ! empty($message))
                    {
                        $message_status = ! isset($message_status) ? 1 : $message_status;
                        ?>
                        <div class="alert <?php echo $message_status == 1 ? 'alert-success' : 'alert-icon alert-danger' ?> alert-dismissible fade show">
                            <button type="button" class="close text-right" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="mdi <?php echo $message_status == 1 ? 'mdi-check-all' : 'mdi-alert' ?>"></i>
                            <?php echo $message; ?>
                        </div>
                        <?php
                    }
                    ?>

                    <div class="table-responsive">
                        <?php echo $table; ?>
                    </div>
                </div>
                <!-- end p-20 -->
            </div>
            <!-- end col -->
        </div>
        <!-- end row -->
    </div>
    <!-- container -->
</div>
<!-- content -->