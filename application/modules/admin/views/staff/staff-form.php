<!-- Start content -->
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title float-left">Staff</h4>

                    <ol class="breadcrumb float-right">
                        <li class="breadcrumb-item"><a href="#">Staff</a></li>
                        <li class="breadcrumb-item active"><?php echo empty($form_caption) ? "" : $form_caption; ?></li>
                    </ol>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->


        <div class="col-lg-12">
            <div class="card-box">
                <div class="card-box-heading">
                    <h2 class="card-box-heading-text"><?php echo empty($form_caption) ? "" : $form_caption; ?></h2>
                    <div class="heading-elements">
                        <a href="<?php echo base_url('admin/staff/list-staff') ?>" class="btn btn-purple waves-effect w-md waves-light" style="font-size: large">
                            <i class="fa fa-list">&nbsp;&nbsp; List Staff</i>
                        </a>
                    </div>
                </div>
                <!--jquery validation error container-->
                <div id="errorContainer" class="alert">
                    <p>
                        Please correct the following errors and try again:
                        <button type="button" class="close text-right" data-dismiss="alert" aria-hidden="true">&times;</button>
                    </p>
                    <ul></ul>
                </div>

                <?php
                if ( ! empty($flash_message))
                {
                    $flash_message_status = ! isset($flash_message_status) ? 1 : $flash_message_status;
                    ?>
                    <div class="alert <?php echo $flash_message_status == 1 ? 'alert-success' : 'alert-danger' ?>">
                        <button type="button" class="close text-right" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?php echo $flash_message; ?>
                    </div>
                    <?php
                }

                $validation_errors = validation_errors();
                if ( ! empty($validation_errors))
                {
                    ?>
                    <div class="col-sm-12">
                        <div class="col-xs-12 alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert"
                                    aria-hidden="true">
                                &times;
                            </button>
                            <?php echo $validation_errors; ?>
                        </div>
                    </div>
                    <?php
                }
                ?>

                <?php
                echo form_open_multipart(current_url(), array("id" => "form", "class" => "form-valid", "role" => "form"));
                ?>

                <?php

                if($form_caption == 'Edit Staff'){
                    ?>
                    <div class="form-group row">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-6">
                            <?php $image = base_url().'assets/uploads/staff/images/'.$pic ?>
                            <img src="<?php echo $image; ?>" class="img-responsive" alt="/" style="height: 150px;width: 150px">

                        </div>
                    </div>

                <?php
                }
                ?>


                <div class="form-group row">
                    <label for="name" class="col-sm-2 form-control-label">Staff Name<span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                        <?php
                        $data = array(
                            'name'                       => 'name',
                            'id'                         => 'name',
                            'value'                      => set_value('name', empty($name) ? '' : $name),
                            'class'                      => 'form-control',
                            'placeholder'                => 'Staff Name',
                            'required data-msg-required' => 'Please provide a Staff name.'
                        );
                        echo form_input($data);
                        ?>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="description" class="col-sm-2 form-control-label">Small Description</label>
                    <div class="col-sm-6">
                    <input type="text" name="description" value="<?php echo !empty($description) ? $description : $this->input->post('description'); ?>" class="form-control" placeholder="Small Description">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="mobile_number" class="col-sm-2 form-control-label">Mobile Number</label>
                    <div class="col-sm-6">
                    <input type="text" name="mobile_number" value="<?php echo !empty($mobile_number) ? $mobile_number : $this->input->post('mobile_number'); ?>" class="form-control" placeholder="Mobile Number">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="email" class="col-sm-2 form-control-label">Email</label>
                    <div class="col-sm-6">
                    <input type="text" name="email" value="<?php echo !empty($email) ? $email : $this->input->post('email'); ?>" class="form-control" placeholder="Email">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="pic" class="col-sm-2 form-control-label">Image<span class="text-danger">*</span></label>
                        <div class="col-sm-6">
                        <input type="file" name="pic" id="pic" class="form-control" placeholder="Image"
                               value="<?php echo $this->input->post('pic'); ?>">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-primary waves-effect waves-light">
                            Save
                        </button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>

        </div> <!-- end col -->
    </div>
    <!-- end row -->

</div> <!-- container -->

</div>
<!-- content -->