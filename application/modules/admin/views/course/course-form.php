<!-- Start content -->
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title float-left">Course</h4>

                    <ol class="breadcrumb float-right">
                        <li class="breadcrumb-item"><a href="#">Course</a></li>
                        <li class="breadcrumb-item active"><?php echo empty($form_caption) ? "" : $form_caption; ?></li>
                    </ol>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->


        <div class="col-lg-12">
            <div class="card-box">
                <div class="card-box-heading">
                    <h2 class="card-box-heading-text"><?php echo empty($form_caption) ? "" : $form_caption; ?></h2>
                    <div class="heading-elements">
                        <a href="<?php echo base_url('admin/course/list-course') ?>" class="btn btn-purple waves-effect w-md waves-light" style="font-size: large">
                            <i class="fa fa-list">&nbsp;&nbsp; List Course</i>
                        </a>
                    </div>
                </div>
                <!--jquery validation error container-->
                <div id="errorContainer" class="alert">
                    <p>
                        Please correct the following errors and try again:
                        <button type="button" class="close text-right" data-dismiss="alert" aria-hidden="true">&times;</button>
                    </p>
                    <ul></ul>
                </div>

                <?php
                if ( ! empty($flash_message))
                {
                    $flash_message_status = ! isset($flash_message_status) ? 1 : $flash_message_status;
                    ?>
                    <div class="alert <?php echo $flash_message_status == 1 ? 'alert-success' : 'alert-danger' ?>">
                        <button type="button" class="close text-right" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?php echo $flash_message; ?>
                    </div>
                    <?php
                }

                $validation_errors = validation_errors();
                if ( ! empty($validation_errors))
                {
                    ?>
                    <div class="col-sm-12">
                        <div class="col-xs-12 alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert"
                                    aria-hidden="true">
                                &times;
                            </button>
                            <?php echo $validation_errors; ?>
                        </div>
                    </div>
                    <?php
                }
                ?>

                <?php
                echo form_open_multipart(current_url(), array("id" => "form", "class" => "form-valid", "role" => "form"));
                ?>
                <div class="form-group row">
                    <label for="portfolio_name" class="col-sm-2 form-control-label">Course Name<span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                        <?php
                        $data = array(
                            'name'                       => 'name',
                            'id'                         => 'name',
                            'value'                      => set_value('name', empty($name) ? '' : $name),
                            'class'                      => 'form-control',
                            'placeholder'                => 'Course Name',
                            'required data-msg-required' => 'Please provide a Course name.'
                        );
                        echo form_input($data);
                        ?>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-primary waves-effect waves-light">
                            Save
                        </button>
                    </div>
                </div>
                <?php echo form_close(); ?>

            </div>

        </div> <!-- end col -->
    </div>
    <!-- end row -->

</div> <!-- container -->

</div>
<!-- content -->