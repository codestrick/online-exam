<!-- Start content -->
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title float-left">Settings</h4>

                    <ol class="breadcrumb float-right">
                        <li class="breadcrumb-item"><a href="#">Settings</a></li>
                        <li class="breadcrumb-item active"><?php echo empty($form_caption) ? "" : $form_caption; ?></li>
                    </ol>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->


        <div class="col-lg-12">
            <div class="card-box">
                <div class="card-box-heading">
                    <h2 class="card-box-heading-text"><?php echo empty($form_caption) ? "" : $form_caption; ?></h2>
                    <div class="heading-elements"></div>
                </div>

                <?php
                if ( ! empty($flash_message))
                {
                    $flash_message_status = ! isset($flash_message_status) ? 1 : $flash_message_status;
                    ?>
                    <div class="alert <?php echo $flash_message_status == 1 ? 'alert-success' : 'alert-danger' ?>">
                        <button type="button" class="close text-right" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?php echo $flash_message; ?>
                    </div>
                    <?php
                }

                $validation_errors = validation_errors();
                if ( ! empty($validation_errors))
                {
                    ?>
                    <div class="col-sm-12">
                        <div class="col-xs-12 alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert"
                                    aria-hidden="true">
                                &times;
                            </button>
                            <?php echo $validation_errors; ?>
                        </div>
                    </div>
                    <?php
                }
                ?>

                <?php
                echo form_open(current_url(), array("id" => "form", "class" => "", "role" => "form"));
                ?>

                <?php
                foreach ($settings_array as $k => $val)
                {
                    ?>
                    <br>
                    <h4><?php echo $val['course_name']; ?>
                        <hr>
                    </h4>
                    <input type="hidden" name="<?php echo 'item[' . $k . '][course_id]'; ?>" value="<?php echo $val['course_id']; ?>"/>
                    <input type="hidden" name="<?php echo 'item[' . $k . '][course_name]'; ?>" value="<?php echo $val['course_name']; ?>"/>
                    <div class="form-group row">
                        <label for="portfolio_name" class="col-sm-2 form-control-label">Total Question<span class="text-danger">*</span></label>
                        <div class="col-sm-6">
                            <input type="text" name="<?php echo 'item[' . $k . '][total_question]'; ?>" value="<?php echo $val['total_question']; ?>" class="class form-control"
                                   placeholder="Total Question"
                                   required data-msg-required="Required total question" readonly/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="portfolio_name" class="col-sm-2 form-control-label">Full Marks<span class="text-danger">*</span></label>
                        <div class="col-sm-6">
                            <input type="text" name="<?php echo 'item[' . $k . '][fm]'; ?>" value="<?php echo $val['fm']; ?>" class="class form-control"
                                   placeholder="Full Marks"
                                   required data-msg-required="Required full marks"/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="portfolio_name" class="col-sm-2 form-control-label">Times<span class="text-danger">*</span></label>
                        <div class="col-sm-6">
                            <input type="text" name="<?php echo 'item[' . $k . '][time]'; ?>" value="<?php echo $val['time']; ?>" class="class form-control"
                                   placeholder="Times"
                                   required data-msg-required="Required times"/>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="portfolio_name" class="col-sm-2 form-control-label">Exam status<span class="text-danger">*</span></label>
                        <div class="col-sm-6">
                            <select class="class form-control" name="<?php echo 'item[' . $k . '][exam-status]'; ?>">
                                <option value="on" <?php echo !empty($val['exam-status']) ? $val['exam-status'] == 'on' ? 'selected' : '' : ''; ?>>On</option>
                                <option value="off" <?php echo !empty($val['exam-status']) ? $val['exam-status'] == 'off' ? 'selected' : '' : 'selected'; ?>>Off</option>
                            </select>
                        </div>
                    </div>
                    <?php
                }
                ?>

                <hr>
                <div class="form-group row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-primary waves-effect waves-light">
                            Save
                        </button>
                    </div>
                </div>
                <?php echo form_close(); ?>

            </div>

        </div> <!-- end col -->
    </div>
    <!-- end row -->

</div> <!-- container -->

</div>
<!-- content -->