<button class="btn btn-purple waves-effect w-md waves-light" style="font-size: large" data-toggle="modal" data-target="#myCategoryBulkUploadModal"><i class="fa fa-plus"></i>
    Add Bulk Question
</button>

<!-- User Bulk Upload Modal -->
<div id="myCategoryBulkUploadModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: gainsboro">
                <h2 class="modal-title"><strong>Add Bulk Question</strong></h2>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <br>
            </div>

            <form action="<?php echo base_url('admin/question/bulk-add-question'); ?>" method="post" class="form-horizontal bulk-form-valid" id="bulk_question_add"
                  enctype="multipart/form-data">
                <div class="modal-body">
                    <p style="color: red" class="text-center">Please use proper data structure using .xlsx, .xls, .csv, .ods file format.</p>

                    <div id="errorContainerModal">
                        <p>Please correct the following errors and try again:</p>
                        <ul></ul>
                    </div>
                    <br>


                    <div class="form-group row">
                        <label for="portfolio_category" class="col-sm-3 form-control-label">Select Course<span class="text-danger">*</span></label>
                        <div class="col-sm-9">
                            <select name="course_id" id="course_id" class="form-control select2" required data-msg-required="Please select a course">
                                <option value="">Select A Course</option>
                                <?php
                                if ( ! empty($all_courses))
                                {
                                    foreach ($all_courses as $k => $v)
                                    {
                                        $selected = ($v['id'] == (! empty($course_id) ? $course_id : NULL) ? 'selected' : '');
                                        echo '<option value="' . $v['id'] . '"' . $selected . '>' . $v['name'] . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="portfolio_category" class="col-sm-3 form-control-label">Select File <span class="text-danger">*</span></label>
                        <div class="col-sm-9">
                            <input type="file" name="excel_file" id="excel_file" class="form-control"
                                   placeholder="Upload Excel" required data-msg-required="Please select a file">

                            <a href="<?php echo base_url('admin/question/download'); ?>" style="color: red"><strong>*</strong> Download Sample File <strong>*</strong></a>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-purple">Save <i class="icon-arrow-right14 position-right"></i></button>
                    <button type="button" class="btn btn-purple" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- User Bulk Upload Modal -->