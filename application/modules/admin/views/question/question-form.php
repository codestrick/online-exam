<!-- Start content -->
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title float-left">Question</h4>

                    <ol class="breadcrumb float-right">
                        <li class="breadcrumb-item"><a href="#">Question</a></li>
                        <li class="breadcrumb-item active"><?php echo empty($form_caption) ? "" : $form_caption; ?></li>
                    </ol>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->


        <div class="col-lg-12">
            <div class="card-box">
                <div class="card-box-heading">
                    <h2 class="card-box-heading-text"><?php echo empty($form_caption) ? "" : $form_caption; ?></h2>
                    <div class="heading-elements">
                        <?php echo !empty($bulk_upload) ? $bulk_upload : ''; ?>
                        <a href="<?php echo base_url('admin/question/list-question') ?>" class="btn btn-purple waves-effect w-md waves-light" style="font-size: large">
                            <i class="fa fa-list">&nbsp;&nbsp; List Question</i>
                        </a>
                    </div>
                </div>
                <!--jquery validation error container-->
                <div id="errorContainer" class="alert">
                    <p>
                        Please correct the following errors and try again:
                        <button type="button" class="close text-right" data-dismiss="alert" aria-hidden="true">&times;</button>
                    </p>
                    <ul></ul>
                </div>

                <?php
                if ( ! empty($flash_message))
                {
                    $flash_message_status = ! isset($flash_message_status) ? 1 : $flash_message_status;
                    ?>
                    <div class="alert <?php echo $flash_message_status == 1 ? 'alert-success' : 'alert-danger' ?>">
                        <button type="button" class="close text-right" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?php echo $flash_message; ?>
                    </div>
                    <?php
                }

                $validation_errors = validation_errors();
                if ( ! empty($validation_errors))
                {
                    ?>
                    <div class="col-sm-12">
                        <div class="col-xs-12 alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert"
                                    aria-hidden="true">
                                &times;
                            </button>
                            <?php echo $validation_errors; ?>
                        </div>
                    </div>
                    <?php
                }
                ?>

                <?php
                echo form_open_multipart(current_url(), array("id" => "form", "class" => "form-valid", "role" => "form"));
                ?>
                <div class="form-group row">
                    <label for="portfolio_image" class="col-sm-2 form-control-label">Question<span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                        <?php
                        $data = array(
                            'name'        => 'question',
                            'id'          => 'question',
                            'value'       =>set_value('question', empty($question) ? '' : $question),
                            'class'       => 'form-control',
                            'placeholder' => 'Question',
                            'required data-msg-required'=>"Please provide a Question"
                        );
                        echo form_textarea($data);
                        ?>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="portfolio_image" class="col-sm-2 form-control-label">Option 1<span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                        <?php
                        $data = array(
                            'name'        => 'option_1',
                            'id'          => 'option_1',
                            'value'       => set_value('option_1', empty($option_1) ? '' : $option_1),
                            'class'       => 'form-control',
                            'placeholder' => 'Option 1',
                            'required data-msg-required'=>"Please provide a Option 1"
                        );
                        echo form_textarea($data);
                        ?>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="portfolio_image" class="col-sm-2 form-control-label">Option 2<span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                        <?php
                        $data = array(
                            'name'        => 'option_2',
                            'id'          => 'option_2',
                            'value'       => set_value('option_2', empty($option_2) ? '' : $option_2),
                            'class'       => 'form-control',
                            'placeholder' => 'Option 2',
                            'required data-msg-required'=>"Please provide a Option 2"
                        );
                        echo form_textarea($data);
                        ?>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="portfolio_image" class="col-sm-2 form-control-label">Option 3<span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                        <?php
                        $data = array(
                            'name'        => 'option_3',
                            'id'          => 'option_3',
                            'value'       => set_value('option_3', empty($option_3) ? '' : $option_3),
                            'class'       => 'form-control',
                            'placeholder' => 'Option 3',
                            'required data-msg-required'=>"Please provide a Option 3"
                        );
                        echo form_textarea($data);
                        ?>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="portfolio_image" class="col-sm-2 form-control-label">Option 4<span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                        <?php
                        $data = array(
                            'name'        => 'option_4',
                            'id'          => 'option_4',
                            'value'       => set_value('option_4', empty($option_4) ? '' : $option_4),
                            'class'       => 'form-control',
                            'placeholder' => 'Option 4',
                            'required data-msg-required'=>"Please provide a Option 4"
                        );
                        echo form_textarea($data);
                        ?>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="portfolio_category" class="col-sm-2 form-control-label">Right Option<span class="text-danger">*</span></label>
                    <div class="col-sm-6">

                        <?php
                        $right_options = [
                            ''         => 'Select a Option',
                            '1'        => 'Option 1',
                            '2'        => 'Option 2',
                            '3'        => 'Option 3',
                            '4'        => 'Option 4',
                        ];

                        echo form_dropdown('right_option', empty($right_options) ? '' : $right_options, empty($right_option) ? '' : $right_option, 'class="form-control select2" required data-msg-required="Please select a Right option."');
                        ?>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="portfolio_category" class="col-sm-2 form-control-label">Question Status<span class="text-danger">*</span></label>
                    <div class="col-sm-6">

                        <?php
                        $active_status_options = [
                            ''              => 'Select a Status',
                            'active'        => 'Active',
                            'inactive'    => 'Inactive',
                        ];

                        echo form_dropdown('active_status', empty($active_status_options) ? '' : $active_status_options, empty($active_status) ? '' : $active_status, 'class="form-control select2" required data-msg-required="Please select a Question Status."');
                        ?>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="portfolio_category" class="col-sm-2 form-control-label">Select Course<span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                        <select name="course_id" id="course_id" class="form-control select2" required data-msg-required="Please Select a Course">
                            <option value="">Select A Course</option>
                           <?php
                            if ( ! empty($all_courses))
                            {
                                foreach ($all_courses as $k => $v)
                                {
                                    $selected = ($v['id'] == (! empty($course_id) ? $course_id : NULL) ? 'selected' : '');
                                    echo '<option value="' . $v['id'] . '"' . $selected . '>' . $v['name'] . '</option>';
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-primary waves-effect waves-light">
                            Save
                        </button>
                    </div>
                </div>
                <?php echo form_close(); ?>

            </div>

        </div> <!-- end col -->
    </div>
    <!-- end row -->

</div> <!-- container -->

</div>
<!-- content -->