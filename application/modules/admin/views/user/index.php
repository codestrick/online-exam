<!-- Start content -->
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title float-left">Dashboard</h4>

                    <ol class="breadcrumb float-right">
                        <li class="breadcrumb-item"><a href="#">User</a></li>
                        <li class="breadcrumb-item active">List User</li>
                    </ol>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->


        <div class="row">
            <div class="col-md-12">
                <div class="card-box">
                    <div class="card-box-heading">
                        <h2 class="card-box-heading-text"><?php echo empty($form_caption) ? "" : $form_caption; ?></h2>
                        <div class="heading-elements">
                            <a href="<?php echo base_url('admin/user/add-user') ?>" class="btn btn-purple waves-effect w-md waves-light" style="font-size: large">
                                <i class="fa fa-plus">&nbsp;&nbsp; Add User</i>
                            </a>
                        </div>
                    </div>

                    <?php
                    if ( ! empty($message))
                    {
                        $message_status = ! isset($message_status) ? 1 : $message_status;
                        ?>
                        <div class="alert <?php echo $message_status == 1 ? 'alert-success' : 'alert-danger' ?>">
                            <button type="button" class="close text-right" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?php echo $message; ?>
                        </div>
                        <?php
                    }
                    ?>

                    <div class="table-responsive">
                        <?php echo $table; ?>
                    </div>
                </div>
                <!-- end p-20 -->
            </div>
            <!-- end col -->
        </div>
        <!-- end row -->
    </div>
    <!-- container -->
</div>
<!-- content -->