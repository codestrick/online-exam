<!-- Start content -->
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title float-left">User</h4>

                    <ol class="breadcrumb float-right">
                        <li class="breadcrumb-item"><a href="#">User</a></li>
                        <li class="breadcrumb-item active"><?php echo empty($form_caption) ? "" : $form_caption; ?></li>
                    </ol>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->


        <div class="col-lg-12">
            <div class="card-box">
                <div class="card-box-heading">
                    <h2 class="card-box-heading-text"><?php echo empty($form_caption) ? "" : $form_caption; ?></h2>
                    <div class="heading-elements">
                        <a href="<?php echo base_url('admin/user/list-user') ?>" class="btn btn-purple waves-effect w-md waves-light" style="font-size: large">
                            <i class="fa fa-list">&nbsp;&nbsp; List User</i>
                        </a>
                    </div>
                </div>
                <!--jquery validation error container-->
                <div id="errorContainer" class="alert">
                    <p>
                        Please correct the following errors and try again:
                        <button type="button" class="close text-right" data-dismiss="alert" aria-hidden="true">&times;</button>
                    </p>
                    <ul></ul>
                </div>
                <?php
                if (!empty($error))
                {
                    ?>
                    <div class="col-sm-12">
                        <div class="col-xs-12 alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert"
                                    aria-hidden="true">
                                &times;
                            </button>
                            <?php echo $error; ?>
                        </div>
                    </div>
                    <?php
                }

                $validation_errors = validation_errors();
                if (!empty($validation_errors))
                {
                    ?>
                    <div class="col-sm-12">
                        <div class="col-xs-12 alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert"
                                    aria-hidden="true">
                                &times;
                            </button>
                            <?php echo $validation_errors; ?>
                        </div>
                    </div>
                    <?php
                }
                ?>

                <?php
                echo form_open_multipart(current_url(), array("id" => "form", "class" => "form-valid", "role" => "form"));
                ?>
                    <div class="form-group row">
                        <label for="name" class="col-sm-2 form-control-label">Name<span class="text-danger">*</span></label>
                        <div class="col-sm-6">
                            <?php
                            $data = array(
                                'name'=>'name',
                                'id'=>'name',
                                'value'=>  set_value('name',empty($name)?'':$name),
                                'class'=>'form-control',
                                'placeholder'=>'Name',
                                'required data-msg-required'=>'Please provide a name'
                            );
                            echo form_input($data);
                            ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="email" class="col-sm-2 form-control-label">E-mail<span class="text-danger">*</span></label>
                        <div class="col-sm-6">
                            <?php
                            $data = array(
                                'name'=>'email',
                                'id'=>'email',
                                'value'=>  set_value('email',empty($email)?'':$email),
                                'class'=>'form-control',
                                'placeholder'=>'Email',
                                'required data-msg-required'=>'Please provide a Email'
                            );
                            echo form_input($data);
                            ?>
                        </div>
                    </div>

                <div class="form-group row">
                        <label for="mobile" class="col-sm-2 form-control-label">Mobile<span class="text-danger">*</span></label>
                        <div class="col-sm-6">
                            <?php
                            $data = array(
                                'name'=>'mobile',
                                'id'=>'mobile',
                                'value'=>  set_value('mobile',empty($mobile)?'':$mobile),
                                'class'=>'form-control',
                                'placeholder'=>'Mobile',
                                'required data-msg-required'=>'Please provide a Mobile No'
                            );
                            echo form_input($data);
                            ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="username" class="col-sm-2 form-control-label">Username<span class="text-danger">*</span></label>
                        <div class="col-sm-6">
                            <?php
                            $data = array(
                                'name'=>'username',
                                'id'=>'username',
                                'value'=>  set_value('username',empty($username)?'':$username),
                                'class'=>'form-control',
                                'placeholder'=>'User name',
                                'required data-msg-required'=>'Please provide a username'
                            );
                            echo form_input($data);
                            ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="password" class="col-sm-2 form-control-label">Password<span class="text-danger">*</span></label>
                        <div class="col-sm-6">
                            <?php
                            $data = array(
                                'name'=>'password',
                                'id'=>'password',
                                'type'=>'password',
                                'value'=>'',
                                'class'=>'form-control',
                                'placeholder'=>'Password',
                            );
                            echo form_input($data);
                            ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="user_type" class="col-sm-2 form-control-label">User Type<span class="text-danger">*</span></label>
                        <div class="col-sm-6">
                            <?php
                            echo form_dropdown('user_type',empty($user_type)?'':$user_type,empty($usertype)?'':$usertype,'class="form-control select2"');
                            ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-6">
                            <button type="submit" class="btn btn-primary waves-effect waves-light">
                                Save
                            </button>
                        </div>
                    </div>
                <?php echo form_close(); ?>

            </div>

        </div> <!-- end col -->
    </div>
    <!-- end row -->

</div> <!-- container -->

</div>
<!-- content -->