<!-- Start content -->
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title float-left">Dashboard</h4>

                    <ol class="breadcrumb float-right">
                        <li class="breadcrumb-item"><a href="#">Right Angle</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->


        <div class="row">
            <div class="col-md-12">
                <div class="card-box">
                    <div class="text-center">
                        <h3 class="m-b-30 m-t-20">Welcome to Right Angle Admin section.</h3>
                        <p>
                            Right Angle is an India based Web Development Company, offering wide variety of services including Search Engine Optimization (SEO), social media marketing (SMM), pay per click (PPC) Management and web content writing to the clients ranging from individuals, professionals and small sized Business in India and throughout the world. We are well known for delivering quality services within the stipulated time to our clients.
                        </p>
                    </div>
                </div> <!-- end p-20 -->
            </div> <!-- end col -->
        </div>
        <!-- end row -->
    </div> <!-- container -->

</div>
<!-- content -->