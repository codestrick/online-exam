<!-- Start content -->
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title float-left">Change Password</h4>

                    <ol class="breadcrumb float-right">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item active">Change Password</li>
                    </ol>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->


        <div class="row">
            <div class="col-md-12">
                <div class="card-box">
                    <div class="card-box-heading">
                        <h2 class="card-box-heading-text">Change Password</h2>
                        <div class="heading-elements"></div>
                    </div>
                    <!--jquery validation error container-->
                    <div id="errorContainer" class="alert">
                        <p>
                            Please correct the following errors and try again:
                            <button type="button" class="close text-right" data-dismiss="alert" aria-hidden="true">&times;</button>
                        </p>
                        <ul></ul>
                    </div>
                    <?php
                    if ( ! empty($flash_message))
                    {
                        $flash_message_status = ! isset($flash_message_status) ? 1 : $flash_message_status;
                        ?>
                        <div class="alert <?php echo $flash_message_status == 1 ? 'alert-success' : 'alert-danger' ?>">
                            <button type="button" class="close text-right" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?php echo $flash_message; ?>
                        </div>
                        <?php
                    }

                    $validation_errors = validation_errors();
                    if ( ! empty($validation_errors))
                    {
                        ?>
                        <div class="col-sm-12">
                            <div class="col-xs-12 alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">
                                    &times;
                                </button>
                                <?php echo $validation_errors; ?>
                            </div>
                        </div>
                        <?php
                    }
                    ?>

                    <form class="form-horizontal form-valid" id="form" action="<?php echo current_url(); ?>" method="post">

                        <div class="form-group row">
                            <label for="portfolio_category" class="col-sm-2 form-control-label">Old Password<span class="text-danger">*</span></label>
                            <div class="col-sm-6">
                                <input class="form-control" type="password" id="old_password" name="old_password" required data-msg-required="Please provide a old password" placeholder="Old Password">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="portfolio_category" class="col-sm-2 form-control-label">New Password<span class="text-danger">*</span></label>
                            <div class="col-sm-6">
                                <input class="form-control" type="password" id="new_password" name="new_password" required data-msg-required="Please provide a new password" placeholder="New Password">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="portfolio_category" class="col-sm-2 form-control-label">Re - Enter New Password<span class="text-danger">*</span></label>
                            <div class="col-sm-6">
                                <input class="form-control" type="password" id="re_enter_new_password" name="re_enter_new_password" required data-msg-required="Please re - enter new password" placeholder="Re - Enter New Password">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-primary waves-effect waves-light">
                                    Reset Password
                                </button>
                            </div>
                        </div>

                    </form>
                </div> <!-- end p-20 -->
            </div> <!-- end col -->
        </div>
        <!-- end row -->
    </div> <!-- container -->

</div>
<!-- content -->