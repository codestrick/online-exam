<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Admin Login | Online Exam Portal</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- App favicon -->
    <link rel="icon" href="<?php echo base_url('assets/images/header-icon.png'); ?>" type="image/png">

    <!-- App css -->
    <link href="<?php echo base_url(); ?>assets/admin/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/admin/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/admin/css/metismenu.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/admin/css/style.css" rel="stylesheet" type="text/css" />

    <script src="<?php echo base_url(); ?>assets/admin/js/modernizr.min.js"></script>

</head>


<body class="bg-accpunt-pages">

<!-- HOME -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">

                <div class="wrapper-page">

                    <div class="account-pages">
                        <div class="account-box">
                            <div class="account-logo-box">
                                <h2 class="text-uppercase text-center">
                                    <a href="<?php echo base_url('');?>" class="text-success">
                                        <span><img src="<?php echo base_url(); ?>assets/admin/images/logo/right-angle-logo.jpg" alt="" height="100"></span>
                                    </a>
                                </h2>
                                <h5 class="text-uppercase font-bold m-b-5 m-t-50">Sign In</h5>
                                <p class="m-b-0">Login to your Admin account</p>
                            </div>

                            <div class="account-content">
                                <?php
                                $validation_errors = validation_errors();
                                if ( ! empty($validation_errors))
                                {
                                    ?>
                                    <div class="col-sm-12">
                                        <div class="col-xs-12 alert alert-danger">
                                            <button type="button" class="close" data-dismiss="alert"
                                                    aria-hidden="true">
                                                &times;
                                            </button>
                                            <?php echo $validation_errors; ?>
                                        </div>
                                    </div>
                                    <?php
                                }

                                if ( ! empty($flash_message))
                                {
                                    $flash_message_status = ! isset($flash_message_status) ? 1 : $flash_message_status;
                                    ?>
                                    <div class="alert <?php echo $flash_message_status == 1 ? 'alert-success' : 'alert-icon alert-danger' ?> alert-dismissible fade show">
                                        <button type="button" class="close text-right" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <i class="mdi <?php echo $flash_message_status == 1 ? 'mdi-check-all' : 'mdi-alert' ?>"></i>
                                        <?php echo $flash_message; ?>
                                    </div>
                                    <?php
                                }
                                ?>
                                <form class="form-horizontal" id="login-form" method="post" action="<?php echo base_url('admin/user/validate/redirectForcefully'); ?>">

                                    <div class="form-group m-b-20 row">
                                        <div class="col-12">
                                            <label for="emailaddress">Email address</label>
                                            <input class="form-control" type="text" id="useremail" name="useremail" required="" placeholder="Username">
                                        </div>
                                    </div>

                                    <div class="form-group row m-b-20">
                                        <div class="col-12">
<!--                                            <a href="--><?php //echo base_url('admin/forgot-password'); ?><!--" class="text-muted pull-right"><small>Forgot your password?</small></a>-->
                                            <label for="password">Password</label>
                                            <input class="form-control" type="password" required="" id="password" name="password" placeholder="Enter your password">
                                        </div>
                                    </div>

                                    <div class="form-group row text-center m-t-10">
                                        <div class="col-12">
                                            <button class="btn btn-md btn-block btn-primary waves-effect waves-light" type="submit">Sign In</button>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div>
                    <!-- end card-box-->


                </div>
                <!-- end wrapper -->

            </div>
        </div>
    </div>
</section>
<!-- END HOME -->



<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/tether.min.js"></script><!-- Tether for Bootstrap -->
<script src="<?php echo base_url(); ?>assets/admin/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/metisMenu.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/waves.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.slimscroll.js"></script>

<!-- App js -->
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.core.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.app.js"></script>

</body>
</html>