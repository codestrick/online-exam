<!-- Start content -->
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title float-left">About Us page content</h4>

                    <ol class="breadcrumb float-right">
                        <li class="breadcrumb-item"><a href="#">About Us</a></li>
                        <li class="breadcrumb-item active"><?php echo empty($form_caption) ? "" : $form_caption; ?></li>
                    </ol>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->


        <div class="col-lg-12">
            <div class="card-box">
                <div class="card-box-heading">
                    <h2 class="card-box-heading-text"><?php echo empty($form_caption) ? "" : $form_caption; ?></h2>

                </div>
                <!--jquery validation error container-->
                <div id="errorContainer" class="alert">
                    <p>
                        Please correct the following errors and try again:
                        <button type="button" class="close text-right" data-dismiss="alert" aria-hidden="true">&times;</button>
                    </p>
                    <ul></ul>
                </div>

                <?php
                if ( ! empty($flash_message))
                {
                    $flash_message_status = ! isset($flash_message_status) ? 1 : $flash_message_status;
                    ?>
                    <div class="alert <?php echo $flash_message_status == 1 ? 'alert-success' : 'alert-danger' ?>">
                        <button type="button" class="close text-right" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?php echo $flash_message; ?>
                    </div>
                    <?php
                }

                $validation_errors = validation_errors();
                if ( ! empty($validation_errors))
                {
                    ?>
                    <div class="col-sm-12">
                        <div class="col-xs-12 alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert"
                                    aria-hidden="true">
                                &times;
                            </button>
                            <?php echo $validation_errors; ?>
                        </div>
                    </div>
                    <?php
                }
                ?>

                <?php
                echo form_open_multipart(current_url(), array("id" => "form", "class" => "form-valid", "role" => "form"));
                ?>


                <h4 style="text-align: center; color: green">About Us</h4>
                <div class="form-group row">
                    <label for="heading" class="col-sm-2 form-control-label">Heading<span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" name="heading" id="heading" value="<?php echo !empty($heading) ? $heading : $this->input->post('heading'); ?>" class="form-control"
                               placeholder="Heading" required data-msg-required="Please provide a Heading">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="content" class="col-sm-2 form-control-label">Content<span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                        <textarea type="text" name="content" id="content" class="form-control" placeholder="Content" required data-msg-required="Please provide a Phone Number"><?php echo !empty($content) ? $content : $this->input->post('content'); ?></textarea>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="point_1" class="col-sm-2 form-control-label">Point 1</label>
                    <div class="col-sm-6">
                        <input type="text" name="point_1" id="point_1" value="<?php echo !empty($point_1) ? $point_1 : $this->input->post('point_1'); ?>" class="form-control"
                               placeholder="Point">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="heading" class="col-sm-2 form-control-label">Point 2</label>
                    <div class="col-sm-6">
                        <input type="text" name="point_2" id="point_2" value="<?php echo !empty($point_2) ? $point_2 : $this->input->post('point_2'); ?>" class="form-control"
                               placeholder="Point">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="point_3" class="col-sm-2 form-control-label">Point 3</label>
                    <div class="col-sm-6">
                        <input type="text" name="point_3" id="point_3" value="<?php echo !empty($point_3) ? $point_3 : $this->input->post('point_3'); ?>" class="form-control"
                               placeholder="Point">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="point_4" class="col-sm-2 form-control-label">Point 4</label>
                    <div class="col-sm-6">
                        <input type="text" name="point_4" id="point_4" value="<?php echo !empty($point_4) ? $point_4 : $this->input->post('point_4'); ?>" class="form-control"
                               placeholder="Point">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="point_5" class="col-sm-2 form-control-label">Point 5</label>
                    <div class="col-sm-6">
                        <input type="text" name="point_5" id="point_5" value="<?php echo !empty($point_5) ? $point_5 : $this->input->post('point_5'); ?>" class="form-control"
                               placeholder="Point">
                    </div>
                </div>

                <h4 style="text-align: center; color: green">What we do</h4>

                <div class="form-group row">
                    <label for="we_do_heading_1" class="col-sm-2 form-control-label">What we do section 1</label>

                    <div class="col-sm-5">
                        <input type="text" name="we_do_heading_1" id="we_do_heading_1" value="<?php echo !empty($we_do_heading_1) ? $we_do_heading_1 : $this->input->post('we_do_heading_1'); ?>" class="form-control" placeholder="Heading">
                    </div>
                    <div class="col-sm-5">
                        <textarea name="we_do_description_1" id="we_do_description_1" class="form-control" placeholder="Description"><?php echo !empty($we_do_description_1) ? $we_do_description_1 : $this->input->post('we_do_description_1'); ?></textarea>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="we_do_heading_2" class="col-sm-2 form-control-label">What we do section 2</label>

                    <div class="col-sm-5">
                        <input type="text" name="we_do_heading_2" id="we_do_heading_2" value="<?php echo !empty($we_do_heading_2) ? $we_do_heading_2 : $this->input->post('we_do_heading_2'); ?>" class="form-control" placeholder="Heading">
                    </div>
                    <div class="col-sm-5">
                        <textarea name="we_do_description_2" id="we_do_description_2" class="form-control" placeholder="Description"><?php echo !empty($we_do_description_2) ? $we_do_description_2 : $this->input->post('we_do_description_2'); ?></textarea>
                    </div>
                </div>

                <h4 style="text-align: center; color: green">Fun Facts Points</h4>

                <div class="form-group row">
                    <label for="fun_percentage_1" class="col-sm-2 form-control-label">Fun Facts Point 1</label>
                    <div class="col-sm-2">

                        <input type="number" name="fun_percentage_1" id="fun_percentage_1" value="<?php echo !empty($fun_percentage_1) ? $fun_percentage_1 : $this->input->post('fun_percentage_1'); ?>" class="form-control" placeholder="Percentage">
                    </div>
                     <div class="col-sm-3">
                        <input type="text" name="fun_point_1" id="fun_point_1" value="<?php echo !empty($fun_point_1) ? $fun_point_1 : $this->input->post('fun_point_1'); ?>" class="form-control" placeholder="Point Heading">
                    </div>
                    <div class="col-sm-5">
                        <input type="text" name="fun_description_1" id="fun_description_1" value="<?php echo !empty($fun_description_1) ? $fun_description_1 : $this->input->post('fun_description_1'); ?>" class="form-control" placeholder="Point Description">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="fun_percentage_2" class="col-sm-2 form-control-label">Fun Facts Point 2</label>
                    <div class="col-sm-2">

                        <input type="number" name="fun_percentage_2" id="fun_percentage_2" value="<?php echo !empty($fun_percentage_2) ? $fun_percentage_2 : $this->input->post('fun_percentage_2'); ?>" class="form-control" placeholder="Percentage">
                    </div>
                    <div class="col-sm-3">
                        <input type="text" name="fun_point_2" id="fun_point_2" value="<?php echo !empty($fun_point_2) ? $fun_point_2 : $this->input->post('fun_point_2'); ?>" class="form-control" placeholder="Point Heading">
                    </div>
                    <div class="col-sm-5">
                        <input type="text" name="fun_description_2" id="fun_description_2" value="<?php echo !empty($fun_description_2) ? $fun_description_2 : $this->input->post('fun_description_2'); ?>" class="form-control" placeholder="Point Description">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="fun_percentage_3" class="col-sm-2 form-control-label">Fun Facts Point 3</label>
                    <div class="col-sm-2">

                        <input type="number" name="fun_percentage_3" id="fun_percentage_3" value="<?php echo !empty($fun_percentage_3) ? $fun_percentage_3 : $this->input->post('fun_percentage_3'); ?>" class="form-control" placeholder="Percentage">
                    </div>
                    <div class="col-sm-3">
                        <input type="text" name="fun_point_3" id="fun_point_3" value="<?php echo !empty($fun_point_3) ? $fun_point_3 : $this->input->post('fun_point_3'); ?>" class="form-control" placeholder="Point Heading">
                    </div>
                    <div class="col-sm-5">
                        <input type="text" name="fun_description_3" id="fun_description_3" value="<?php echo !empty($fun_description_3) ? $fun_description_3 : $this->input->post('fun_description_3'); ?>" class="form-control" placeholder="Point Description">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="fun_percentage_4" class="col-sm-2 form-control-label">Fun Facts Point 4</label>
                    <div class="col-sm-2">

                        <input type="number" name="fun_percentage_4" id="fun_percentage_4" value="<?php echo !empty($fun_percentage_4) ? $fun_percentage_4 : $this->input->post('fun_percentage_4'); ?>" class="form-control" placeholder="Percentage">
                    </div>
                    <div class="col-sm-3">
                        <input type="text" name="fun_point_4" id="fun_point_4" value="<?php echo !empty($fun_point_4) ? $fun_point_4 : $this->input->post('fun_point_4'); ?>" class="form-control" placeholder="Point Heading">
                    </div>
                    <div class="col-sm-5">
                        <input type="text" name="fun_description_4" id="fun_description_4" value="<?php echo !empty($fun_description_4) ? $fun_description_4 : $this->input->post('fun_description_4'); ?>" class="form-control" placeholder="Point Description">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-primary waves-effect waves-light">
                            Save
                        </button>
                    </div>
                </div>
                <?php echo form_close(); ?>

            </div>

        </div> <!-- end col -->
    </div>
    <!-- end row -->

</div> <!-- container -->

</div>
<!-- content -->