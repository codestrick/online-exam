<?php

if ( ! defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

class Student_model extends My_Model
{

    public $tbl_name             = 'student';
    public $tbl_select_db_cols   = 'id, student_roll, user_id, name, email, address, mobile, pincode, active_status, created_at, updated_at';
    public $tbl_list_search_key  = 'name';

    public function __construct()
    {
        parent::__construct();
    }

    public function save_student_details($dataValues)
    {
        $return_array = array(
            'id' => null,
            'type' => null
        );

        if ( ! empty($dataValues))
        {
            if ( ! empty($dataValues['id']))
            {
                $this->db->where('id', $dataValues['id']);

                if ($this->db->update($this->tbl_name, $dataValues))
                {
                    $id = $dataValues['id'];
                    $type = 'update';
                }
            }
            else
            {
                if ($this->db->insert($this->tbl_name, $dataValues))
                {
                    $id = $this->db->insert_id();
                    $type = 'insert';
                }
            }

        }

        return $return_array = array(
            'id' => $id,
            'type' => $type
        );
    }

    function get_student_detail($params)
    {
        $query = $this->db->get_where($this->tbl_name, $params);

        return $query->row_array();
    }

    function get_student_details($params)
    {
        $query = $this->db->get_where($this->tbl_name, $params);

        return $query->result_array();
    }

    function get_all_student($pagingParams = array())
    {
        $this->db->select('SQL_CALC_FOUND_ROWS 1', FALSE);
        $this->db->select($this->tbl_select_db_cols);

        if ( ! empty($pagingParams['order_by']))
        {
            if (empty($pagingParams['order_direction']))
            {
                $pagingParams['order_direction'] = '';
            }

            switch ($pagingParams['order_by'])
            {
                default:
                    $this->db->order_by($pagingParams['order_by'], $pagingParams['order_direction']);
                    break;
            }
        }

        $search = empty($pagingParams['search']) ? array() : $pagingParams['search'];
        if ( ! empty($search))
        {
            $this->db->like($this->tbl_list_search_key, $search);
            $this->db->or_like($this->tbl_list_search_key1, $search);
        }

        $return = $this->get_with_count($this->tbl_name, $pagingParams['records_per_page'], $pagingParams['offset']);

        return $return;
    }

    function delete_student($params)
    {
        $status = $this->db->delete($this->tbl_name, $params);

        if ($status)
        {
            $statusMessage = 'Student details successfully deleted.';
        }
        else
        {
            $statusMessage = 'The Student details you are trying to delete does not exist.';
        }

        return array(
            'status' => $status,
            'msg'    => $statusMessage,
        );
    }
}
