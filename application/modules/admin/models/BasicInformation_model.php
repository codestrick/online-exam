<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class BasicInformation_model extends My_Model
{

    public $tbl_name = 'basic_information';

    public function __construct()
    {
        parent::__construct();
    }

    public function save_info($dataValues)
    {
        $return = NULL;

        if (!empty($dataValues)) {
            if (!empty($dataValues['id'])) {
                $this->db->where('id', $dataValues['id']);

                if ($this->db->update($this->tbl_name, $dataValues)) {
                    $return = $dataValues['id'];
                }
            } else {
                if ($this->db->insert($this->tbl_name, $dataValues)) {
                    $return = $this->db->insert_id();
                }
            }

        }

        return $return;
    }

    function get_info_detail($cond)
    {
        $query = $this->db->get_where($this->tbl_name, $cond);

        return $query->row_array();
    }

}
