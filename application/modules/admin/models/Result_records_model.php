<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Result_records_model extends My_Model
{
    public $tbl_name = 'result_records';
    public $join_tbl_name = 'user';
    public $join_tbl_name1 = 'course';
    public $join_tbl_name2 = 'student';
    public $join_cond = "result_records.user_id = user.user_id";
    public $join_cond1 = "result_records.course_id = course.id";
    public $join_cond2 = "result_records.student_id = student.id";



    public $tbl_select_db_cols = 'result_records.id, result_records.student_id, result_records.user_id, result_records.course_id, result_records.total_marks, result_records.marks_obtained, result_records.created_at, 
    course.id as courseID, course.name as course_name, 
    user.id as userID,user.user_id, user.name as user_name, student.id as studentID, student.student_roll, student.name as student_name';

    public $tbl_list_search_key = 'student.student_roll';

    public function __construct()
    {
        parent::__construct();
    }

    function get_all_result_records($pagingParams = array())
    {
        $this->db->select('SQL_CALC_FOUND_ROWS 1', FALSE);
        $this->db->select($this->tbl_select_db_cols);
        $this->db->join($this->join_tbl_name, $this->join_cond);
        $this->db->join($this->join_tbl_name1, $this->join_cond1);
        $this->db->join($this->join_tbl_name2, $this->join_cond2);
        //get User_id
        $session_data = $this->session->userdata('online_exam_admin');
        $user_id = $session_data['user']['user_id'];

        $this->db->where(['result_records.user_id' => $user_id]);

        if (!empty($pagingParams['order_by'])) {
            if (empty($pagingParams['order_direction'])) {
                $pagingParams['order_direction'] = '';
            }

            switch ($pagingParams['order_by']) {
                default:
                    $this->db->order_by($pagingParams['order_by'], $pagingParams['order_direction']);
                    break;
            }
        }

        $search = empty($pagingParams['search']) ? array() : $pagingParams['search'];
        if (!empty($search)) {
            $this->db->like($this->tbl_list_search_key, $search);
        }

        $return = $this->get_with_count($this->tbl_name, $pagingParams['records_per_page'], $pagingParams['offset']);

        return $return;
    }
}