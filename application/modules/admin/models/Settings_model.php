<?php

if ( ! defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

class Settings_model extends My_Model
{

    public $tbl_name = 'settings';
    public function __construct()
    {
        parent::__construct();
    }

    function get_question_detail($cond = [])
    {
        $query = $this->db->get_where($this->tbl_name, $cond);

        return $query->row_array();
    }

    public function insert_settings_details($dataValues)
    {
       return $this->db->insert($this->tbl_name, $dataValues);
    }

    public function update_settings_details($dataValues,$cond)
    {
        $this->db->where($cond);
        return $this->db->update($this->tbl_name, $dataValues);
    }
}