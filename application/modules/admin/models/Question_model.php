<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Question_model extends My_Model
{

    public $tbl_name = 'question';
    public $join_tbl_name = 'course';
    public $tbl_select_db_cols = 'question.id, question.user_id, question.course_id, question.question, question.option_1, question.option_2, question.option_3, question.option_4, question.right_option, question.active_status, question.created_at, question.updated_at, course.id as course_id, course.name as course_name';
    public $tbl_list_search_key = 'question';

    public function __construct()
    {
        parent::__construct();

        $this->tbl_join_cond = "{$this->join_tbl_name}.id = {$this->tbl_name}.course_id";
    }

    public function save_question_details($dataValues)
    {
        $return = NULL;

        if (!empty($dataValues)) {
            if (!empty($dataValues['id'])) {
                $this->db->where('id', $dataValues['id']);

                if ($this->db->update($this->tbl_name, $dataValues)) {
                    $return = $dataValues['id'];
                }
            } else {
                if ($this->db->insert($this->tbl_name, $dataValues)) {
                    $return = $this->db->insert_id();
                }
            }

        }

        return $return;
    }

    function save_question_by_condition($params)
    {
        $id = NULL;

        $this->db->where('question', $params['question']);
        $dist_mobile = $this->db->get($this->tbl_name);

        $dist_mobile_qt = $dist_mobile->num_rows();

        if ($dist_mobile_qt <= 0)
        {
            if ($this->db->insert($this->tbl_name, $params))
            {
                $id = $this->db->insert_id();
            }
        }

        return $id;
    }

    function get_question_detail($cond)
    {
        $query = $this->db->get_where($this->tbl_name, $cond);

        return $query->row_array();
    }

    function get_question_detail_result_array($cond)
    {
        $query = $this->db->get_where($this->tbl_name, $cond);

        return $query->result_array();
    }

    function get_question_details($params)
    {
        $query = $this->db->get_where($this->tbl_name, $params);

        return $query->result_array();
    }

    function get_all_questions($pagingParams = array())
    {
        $this->db->select('SQL_CALC_FOUND_ROWS 1', FALSE);
        $this->db->select($this->tbl_select_db_cols);
        $this->db->join($this->join_tbl_name, $this->tbl_join_cond);

        if (!empty($pagingParams['order_by'])) {
            if (empty($pagingParams['order_direction'])) {
                $pagingParams['order_direction'] = '';
            }

            switch ($pagingParams['order_by']) {
                default:
                    $this->db->order_by($pagingParams['order_by'], $pagingParams['order_direction']);
                    break;
            }
        }

        $search = empty($pagingParams['search']) ? array() : $pagingParams['search'];
        if (!empty($search)) {
            $this->db->like($this->tbl_list_search_key, $search);
        }

        $return = $this->get_with_count($this->tbl_name, $pagingParams['records_per_page'], $pagingParams['offset']);

        return $return;
    }

    function delete_question($params)
    {
        $status = $this->db->delete($this->tbl_name, $params);

        if ($status) {
            $statusMessage = 'Question details successfully deleted.';
        } else {
            $statusMessage = 'The Question details you are trying to delete does not exist.';
        }

        return array(
            'status' => $status,
            'msg' => $statusMessage,
        );
    }
}
    