<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Course_model extends My_Model
{

    public $tbl_name = 'course';
    public $tbl_select_db_cols = 'id, name, user_id, created_at, updated_at';
    public $tbl_list_search_key = 'name';
    public $tbl_list_search_key1 = 'user_id';

    public function __construct()
    {
        parent::__construct();
    }

    public function save_course($dataValues)
    {
        $return = NULL;

        if (!empty($dataValues)) {
            if (!empty($dataValues['id'])) {
                $this->db->where('id', $dataValues['id']);

                if ($this->db->update($this->tbl_name, $dataValues)) {
                    $return = $dataValues['id'];
                }
            } else {
                if ($this->db->insert($this->tbl_name, $dataValues)) {
                    $return = $this->db->insert_id();
                }
            }

        }

        return $return;
    }

    function get_course_detail($cond)
    {
        $query = $this->db->get_where($this->tbl_name, $cond);

        return $query->row_array();
    }

    function get_course_detail_result_array($cond)
    {
        $query = $this->db->get_where($this->tbl_name, $cond);

        return $query->result_array();
    }


    function get_course_details($params)
    {
        $query = $this->db->get_where($this->tbl_name, $params);

        return $query->result_array();
    }

    function get_all_course($pagingParams = array())
    {
        $this->db->select('SQL_CALC_FOUND_ROWS 1', FALSE);
        $this->db->select($this->tbl_select_db_cols);

        //get User_id
        $session_data = $this->session->userdata('online_exam_admin');
        $user_id = $session_data['user']['user_id'];

        $this->db->where(['user_id' => $user_id]);

        if (!empty($pagingParams['order_by'])) {
            if (empty($pagingParams['order_direction'])) {
                $pagingParams['order_direction'] = '';
            }

            switch ($pagingParams['order_by']) {
                default:
                    $this->db->order_by($pagingParams['order_by'], $pagingParams['order_direction']);
                    break;
            }
        }

        $search = empty($pagingParams['search']) ? array() : $pagingParams['search'];
        if (!empty($search)) {
            $this->db->like($this->tbl_list_search_key, $search);
            $this->db->or_like($this->tbl_list_search_key1, $search);
        }

        $return = $this->get_with_count($this->tbl_name, $pagingParams['records_per_page'], $pagingParams['offset']);

        return $return;
    }

    function delete_portfolio($params)
    {
        $status = $this->db->delete($this->tbl_name, $params);

        if ($status) {
            $statusMessage = 'Portfolio details successfully deleted.';
        } else {
            $statusMessage = 'The Portfolio details you are trying to delete does not exist.';
        }

        return array(
            'status' => $status,
            'msg' => $statusMessage,
        );
    }
}
