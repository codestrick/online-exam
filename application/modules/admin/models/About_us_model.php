<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class About_us_model extends My_Model
{

    public $tbl_name = 'about_us';

    public function __construct()
    {
        parent::__construct();
    }

    public function save($dataValues)
    {
        $return = NULL;

        if (!empty($dataValues)) {
            if (!empty($dataValues['id'])) {
                $this->db->where('id', $dataValues['id']);

                if ($this->db->update($this->tbl_name, $dataValues)) {
                    $return = $dataValues['id'];
                }
            } else {
                if ($this->db->insert($this->tbl_name, $dataValues)) {
                    $return = $this->db->insert_id();
                }
            }

        }

        return $return;
    }

    function get_detail($cond)
    {
        $query = $this->db->get_where($this->tbl_name, $cond);

        return $query->row_array();
    }

}
