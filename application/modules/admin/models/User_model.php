<?php

if ( ! defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

class User_model extends My_Model
{

    public $tbl_name            = 'user';
    public $tbl_select_db_cols  = 'id, name, email, username, status, user_type, created_at, updated_at';
    public $tbl_list_search_key = 'name';

    public function __construct()
    {
        parent::__construct();
    }

    function update_user_using_user_id($dataValues,$user_id)
    {
        $status = false;
        $this->db->where('user_id', $user_id);
        $status = $this->db->update($this->tbl_name, $dataValues);
        return $status;
    }

    public function save_user($dataValues)
    {
        $return_array = array(
            'id' => null,
            'type' => null
        );

        if ( ! empty($dataValues))
        {
            if ( ! empty($dataValues['id']))
            {
                $this->db->where('id', $dataValues['id']);

                if ($this->db->update($this->tbl_name, $dataValues))
                {
                    $id = $dataValues['id'];
                    $type = 'update';
                }
            }
            else
            {
                if ($this->db->insert($this->tbl_name, $dataValues))
                {
                    $id = $this->db->insert_id();
                    $type = 'insert';
                }
            }

        }

        $return_array = array(
            'id' => $id,
            'type' => $type
        );

        return $return_array;
    }

    function get_user_by_id($params)
    {
        $query = $this->db->get_where($this->tbl_name, $params);

        return $query->row_array();
    }

    function delete_user($id)
    {
        $this->db->where('user_id', $id);
        $count_cinema_records = $this->db->count_all_results('user_cinema_trans');

        $this->db->where('user_id', $id);
        $count_cart_records = $this->db->count_all_results('cart');

        $this->db->where('user_id', $id);
        $count_order_records = $this->db->count_all_results('order');

        if ( ! empty($count_cinema_records) && ! empty($count_cart_records) && ! empty($count_order_records))
        {
            $status        = FALSE;
            $statusMessage = 'There are existing cinema, basket and order record(s) for the user and it can\'t be deleted.';
        }
        else if ( ! empty($count_cinema_records) || ! empty($count_cart_records) || ! empty($count_order_records))
        {
            if ( ! empty($count_cinema_records))
            {
                $status        = FALSE;
                $statusMessage = 'There are existing cinema record(s) for the user and it can\'t be deleted.';
            }
            if ( ! empty($count_cart_records))
            {
                $status        = FALSE;
                $statusMessage = 'There are existing basket record(s) for the user and it can\'t be deleted.';
            }
            if ( ! empty($count_order_records))
            {
                $status        = FALSE;
                $statusMessage = 'There are existing order record(s) for the user and it can\'t be deleted.';
            }
        }
        else
        {
            $status = $this->db->delete($this->tbl_name, array('id' => $id));
            if ($status)
            {
                $statusMessage = 'User details successfully deleted.';
            }
            else
            {
                $statusMessage = 'The User details you are trying to delete does not exist.';
            }
        }

        return array(
            'status' => $status,
            'msg'    => $statusMessage,
        );
    }

    function get_all_user($pagingParams = array())
    {
        $this->db->select('SQL_CALC_FOUND_ROWS 1', FALSE);
        $this->db->select($this->tbl_select_db_cols);

        if ( ! empty($pagingParams['order_by']))
        {
            if (empty($pagingParams['order_direction']))
            {
                $pagingParams['order_direction'] = '';
            }

            switch ($pagingParams['order_by'])
            {
                default:
                    $this->db->order_by($pagingParams['order_by'], $pagingParams['order_direction']);
                    break;
            }
        }

        $search = empty($pagingParams['search']) ? array() : $pagingParams['search'];
        if ( ! empty($search))
        {
            $this->db->like($this->tbl_list_search_key, $search);
        }

        $return = $this->get_with_count($this->tbl_name, $pagingParams['records_per_page'], $pagingParams['offset']);

        return $return;
    }

    /*
     * User Login
     */

    function login($username, $password)
    {
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        $query = $this->db->get($this->tbl_name);

        if ($query->num_rows() > 0)
        {
            $row = $query->row_array();

            return $row;
        }
        else
        {
            return FALSE;
        }
    }

    function get_users($user_type = "")
    {
        $return = array();
        $this->db->select('*');
        $this->db->from('user');
        if ($user_type != "")
        {
            $this->db->where('user_type', $user_type);
        }
        $query  = $this->db->get();
        $result = $query->result_array();
        if ( ! empty($result))
        {
            foreach ($result as $idx => $user)
            {
                $return[$user['id']] = $user['username'];
            }
        }

        return $return;
    }

}
    