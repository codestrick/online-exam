<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Student extends My_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->helper('url');
        $this->load->helper('form');

        $this->load->library('form_validation');

        $this->load->model('Public_student_model');
        $this->load->model('Public_app_model');
    }

    public function login()
    {
        $this->form_validation->set_rules('username', 'Username', 'required|min_length[4]|max_length[50]');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[4]|max_length[50]');

        $student_session = get_student_session();

        if (!empty($student_session)) {
            redirect('student/profile');
        }

        if ($this->form_validation->run() == FALSE) {
            $dataArray['flash_message'] = $this->session->flashdata('flash_message');
            $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $dataArray['header_theme'] = 'header2';
            $dataArray['title'] = 'Student Login';
            $dataArray['breadcrumb'] = 'Student Login';

            $dataArray['local_css'] = array();
            $dataArray['local_js'] = array();

            $this->load->view('login', $dataArray);
        } else {
            $encrypted_password = md5($this->input->post('password'));

            $studentdata = $this->Public_student_model->login($this->input->post('username'), $encrypted_password);

            if ($studentdata === FALSE) {
                $message = $this->lang->line('error_login_auth');

                $this->session->set_flashdata('flash_message', $message);
                $this->session->set_flashdata('flash_message_status', FALSE);

                redirect('student/login');
            } else {
                unset($studentdata['password']);

//                if(isset($studentdata['exam_status']) && $studentdata['exam_status'] == TRUE)
//                {
//                    $this->session->set_flashdata('flash_message', 'You are already logged in or you did not logout properly from your previous session.');
//                    $this->session->set_flashdata('flash_message_status', FALSE);
//
//                    redirect('student/login');
//                }

                $existing_session = $this->session->userdata('online_exam');

                $session_student_data = array(
                    'student' => array(
                        'is_logged_in' => TRUE,
                        'id' => $studentdata['id'],
                        'student_roll' => $studentdata['student_roll'],
                        'user_id' => $studentdata['user_id'],
                        'name' => $studentdata['name'],
                        'email' => $studentdata['email'],
                        'status' => $studentdata['active_status'],
                    )
                );

                if (!empty($existing_session)) {
                    $existing_session = array_merge($existing_session, $session_student_data);

                    $this->session->set_userdata('online_exam', $existing_session);
                } else {
                    $this->session->set_userdata('online_exam', $session_student_data);
                }

                redirect(base_url('student/profile'));
            }
        }
    }

    public function change_password()
    {
        $this->form_validation->set_rules('old_pass', 'Old Password ', "required");
        $this->form_validation->set_rules('new_pass', 'New Password ', "required");
        $this->form_validation->set_rules('re_enter_new_pass', 'Re-enter Password ', "required");

        $student_session = get_student_session();

        if (empty($student_session)) {
            redirect('student/logout');
        }

        if ($this->form_validation->run() == TRUE) {
            $user_det = $this->Public_student_model->get_student_by(['id' => $student_session['id']]);

            $old_password = $this->input->post('old_pass');
            $new_password = $this->input->post('new_pass');
            $re_enter_new_password = $this->input->post('re_enter_new_pass');

            if ($new_password == $re_enter_new_password) {
                $old_password = md5($old_password);

                if ($user_det['password'] == $old_password) {
                    $new_password = md5($new_password);

                    $params = array(
                        'password' => $new_password,
                        'updated_at' => date("Y-m-d H:i:s")
                    );

                    $status = $this->Public_student_model->save_student($params, $student_session['id']);

                    if (!empty($status)) {
                        $this->session->set_flashdata('flash_message', 'Password changed successfully');
                        $this->session->set_flashdata('flash_message_status', TRUE);

                        redirect('student/profile');
                    }
                } else {
                    $this->session->set_flashdata('flash_message', 'Old password  incorrect');
                    $this->session->set_flashdata('flash_message_status', FALSE);

                    redirect('student/profile');
                }
            } else {
                $this->session->set_flashdata('flash_message', 'New Password and Re-Enter password not match');
                $this->session->set_flashdata('flash_message_status', FALSE);

                redirect('student/profile');
            }
        } else {
            p(validation_errors());

            $this->session->set_flashdata('flash_message', validation_errors());
            $this->session->set_flashdata('flash_message_status', FALSE);

            redirect('student/profile');
        }
    }

    public function student_profile()
    {
        $student_session = get_student_session();

        if (!empty($student_session)) {
            $dataArray['student_details'] = $this->Public_student_model->get_student_by(['id' => $student_session['id']]);
            $dataArray['result_records'] = $this->Public_app_model->get_details_by('result_records', ['student_id' => $student_session['id']], 'result_array');
        } else {
            $dataArray['student_details'] = [];
            $dataArray['result_records'] = [];
        }

        $dataArray['flash_message'] = $this->session->flashdata('flash_message');
        $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

        $dataArray['header_theme'] = 'header2';
        $dataArray['title'] = 'My Profile';
        $dataArray['breadcrumb'] = 'My Profile';

        $dataArray['local_css'] = array();
        $dataArray['local_js'] = array();

        $this->load->view('student-profile', $dataArray);
    }

    public function logout()
    {
        $existing_session = $this->session->userdata('online_exam');

        if (isset($existing_session['student'])) {

//            $this->Public_student_model->save_student(['exam_status' => 0], $existing_session['student']['id']);

            unset($existing_session['student']);

            $this->session->set_userdata('online_exam', $existing_session);

            $this->session->set_flashdata('flash_message', 'You have been successfully logged out');
            $this->session->set_flashdata('flash_message_status', TRUE);

            redirect('student/login');
        } else {
            redirect('student/login');
        }
    }
}