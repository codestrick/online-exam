<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Index extends My_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->helper('url');
        $this->load->model('Public_about_us_model');
        $this->load->model('Public_basicInformation_model');
        $this->load->model('Public_staff_model');
    }

    public function home()
    {

        $result =  $this->Public_basicInformation_model->get_info_detail([]);

        $our_student = json_decode($result['our_student'],1);

        $dataArray['heading']   = !empty($our_student['heading']) ? $our_student['heading'] : '';
        $dataArray['content']   = !empty($our_student['content']) ? $our_student['content'] : '';
        $dataArray['point_1']   = !empty($our_student['point_1']) ? $our_student['point_1'] : '';
        $dataArray['point_2']   = !empty($our_student['point_2']) ? $our_student['point_2'] : '';
        $dataArray['point_3']   = !empty($our_student['point_3']) ? $our_student['point_3'] : '';
        $dataArray['point_4']   = !empty($our_student['point_4']) ? $our_student['point_4'] : '';
        $dataArray['point_5']   = !empty($our_student['point_5']) ? $our_student['point_5'] : '';

        $dataArray['header_theme'] = 'header1';

        $this->load->view('home', $dataArray);
    }

    public function about_us()
    {
        $result =  $this->Public_about_us_model->get_detail([]);
        $staffes =  $this->Public_staff_model->get_detail_result_array([]);

        $staffes_array = [];

        foreach($staffes as $k => $val){

            $arr1 = !empty($staffes[$k]) ? $staffes[$k] : [];
            $arr2 = !empty($staffes[$k+1]) ? $staffes[$k+1] : [];

           if($k == 0){
               $staffes_array[] = [$arr1,$arr2];
           }else if($k == 1){
               continue;
           }else{
               if($k%2 == 0){
                   $staffes_array[] = [$arr1,$arr2];
               }
           }
        }



        $dataArray['heading']   = $result['heading'];
        $dataArray['content']   = $result['content'];

        $points = json_decode($result['points'],1);
        $fun_facts = json_decode($result['fun_facts'],1);
        $what_we_do = json_decode($result['what_we_do'],1);

        $dataArray['point_1']   = !empty($points['point_1']) ? $points['point_1'] : '';
        $dataArray['point_2']   = !empty($points['point_2']) ? $points['point_2'] : '';
        $dataArray['point_3']   = !empty($points['point_3']) ? $points['point_3'] : '';
        $dataArray['point_4']   = !empty($points['point_4']) ? $points['point_4'] : '';
        $dataArray['point_5']   = !empty($points['point_5']) ? $points['point_5'] : '';

        $dataArray['fun_percentage_1']   = !empty($fun_facts['fun_percentage_1']) ? $fun_facts['fun_percentage_1'] : '';
        $dataArray['fun_percentage_2']   = !empty($fun_facts['fun_percentage_2']) ? $fun_facts['fun_percentage_2'] : '';
        $dataArray['fun_percentage_3']   = !empty($fun_facts['fun_percentage_3']) ? $fun_facts['fun_percentage_3'] : '';
        $dataArray['fun_percentage_4']   = !empty($fun_facts['fun_percentage_4']) ? $fun_facts['fun_percentage_4'] : '';
        $dataArray['fun_point_1']   = !empty($fun_facts['fun_point_1']) ? $fun_facts['fun_point_1'] : '';
        $dataArray['fun_point_2']   = !empty($fun_facts['fun_point_2']) ? $fun_facts['fun_point_2'] : '';
        $dataArray['fun_point_3']   = !empty($fun_facts['fun_point_3']) ? $fun_facts['fun_point_3'] : '';
        $dataArray['fun_point_4']   = !empty($fun_facts['fun_point_4']) ? $fun_facts['fun_point_4'] : '';
        $dataArray['fun_description_1']   = !empty($fun_facts['fun_description_1']) ? $fun_facts['fun_description_1'] : '';
        $dataArray['fun_description_2']   = !empty($fun_facts['fun_description_2']) ? $fun_facts['fun_description_2'] : '';
        $dataArray['fun_description_3']   = !empty($fun_facts['fun_description_3']) ? $fun_facts['fun_description_3'] : '';
        $dataArray['fun_description_4']   = !empty($fun_facts['fun_description_4']) ? $fun_facts['fun_description_4'] : '';

        $dataArray['we_do_heading_1']   = !empty($what_we_do['we_do_heading_1']) ? $what_we_do['we_do_heading_1'] : '';
        $dataArray['we_do_heading_2']   = !empty($what_we_do['we_do_heading_2']) ? $what_we_do['we_do_heading_2'] : '';
        $dataArray['we_do_description_1']   = !empty($what_we_do['we_do_description_1']) ? $what_we_do['we_do_description_1'] : '';
        $dataArray['we_do_description_2']   = !empty($what_we_do['we_do_description_2']) ? $what_we_do['we_do_description_2'] : '';

        $dataArray['header_theme'] = 'header2';
        $dataArray['title'] = 'About Us';
        $dataArray['breadcrumb'] = 'About Us';

        $dataArray['staffes'] = $staffes_array;

        $dataArray['local_css'] = array();
        $dataArray['local_js'] = array();

        $this->load->view('about-us', $dataArray);
    }

    public function contact_us()
    {
        $dataArray['contant_information'] = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend sed turpis. Pellentesque cursus arcu id magna euismod in elementum purus molestie. Curabitur pellentesque massa eu nulla consequat sed porttitor arcu porttitor.';

        $dataArray['header_theme'] = 'header2';
        $dataArray['title'] = 'Contact Us';
        $dataArray['breadcrumb'] = 'Contact Us';

        $dataArray['local_css'] = array();
        $dataArray['local_js'] = array();

        $this->load->view('contact-us', $dataArray);
    }

    public function under_construction()
    {
        $dataArray['header_theme'] = 'header2';
        $dataArray['title'] = 'Under Construction';
        $dataArray['breadcrumb'] = 'Under Construction';

        $this->load->view('under-construction', $dataArray);
    }
}