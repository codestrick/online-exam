<?php

if ( ! defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

class Quiz extends My_Controller
{

    public function __construct()
    {
        parent::__construct();

        $student_session = get_student_session();

        if (empty($student_session))
        {
            redirect('student/login');
        }

        $this->load->helper('url');
        $this->load->model('Public_question_model');
        $this->load->model('Public_settings_model');
        $this->load->model('Public_student_model');
    }

    public function index()
    {
        $student_session = get_student_session();
        $settings        = $this->Public_settings_model->get_settings_by();
        $student_detail  = $this->Public_student_model->get_student_by(['id' => $student_session['id']]);

        $questions = $all_questions = $course_details = [];

        if ( ! empty($settings) && ! empty($student_detail))
        {
            $settings_data = json_decode($settings['settings'], 1);

            if ( ! empty($settings_data))
            {
                $course_details = isset($settings_data[$student_detail['course_id']]) ? $settings_data[$student_detail['course_id']] : NULL;

                $quiz_time_session = get_quiz_session('quiz_time');

                if (empty($quiz_time_session))
                {
                    // in minutes
                    $exam_time = $settings_data[$student_detail['course_id']]['time'];

                    $selectedTime = date('h:i:s');
                    $endTime      = strtotime("+{$exam_time} minutes", strtotime($selectedTime));

                    $quiz_time = [
                        'quiz_start_time' => date('d-m-Y h:i:s', strtotime($selectedTime)),
                        'quiz_end_time'   => date('d-m-Y h:i:s', $endTime)
                    ];

                    $this->session->set_userdata('quiz_time', $quiz_time);
                }

                if ( ! empty($course_details))
                {
                    if ($course_details['exam-status'] == 'on')
                    {
                        $questions     = $this->Public_question_model->get_question_details_by(['course_id' => $student_detail['course_id']], 1);
                        $all_questions = $this->Public_question_model->get_question_details_by(['course_id' => $student_detail['course_id']]);
                    }
                }
            }
        }

//        $this->Public_student_model->save_student(['exam_status' => true], $student_session['id']);

        $dataArray['questions']     = $questions;
        $dataArray['all_questions'] = $all_questions;

        $dataArray['header_theme'] = 'header2';
        $dataArray['title']        = 'Online Exam';
        $dataArray['breadcrumb']   = 'Online Exam';

        $dataArray['local_css'] = array();
        $dataArray['local_js']  = array('jquery.blockUI');

        $this->load->view('quiz', $dataArray);
    }

    public function result()
    {
        $quiz_session          = $this->session->userdata('quiz');
        $quiz_complete_session = $this->session->userdata('quiz-complete');

        $result = 0;
        $question_limit = 0;

        /* have check condition quiz_start or quiz_complete in session exist or not */

        if ( ! empty($quiz_complete_session) && $quiz_complete_session == TRUE)
        {
            $this->session->unset_userdata('quiz');
            $this->session->unset_userdata('quiz-complete');

            if ( ! empty($quiz_session))
            {
                $student_session = get_student_session();
                $settings        = $this->Public_settings_model->get_settings_by();
                $student_detail  = $this->Public_student_model->get_student_by(['id' => $student_session['id']]);

//                $this->Public_student_model->save_student(['exam_status' => 0], $student_session['id']);

                $question_limit = 0;

                if ( ! empty($settings) && ! empty($student_detail))
                {
                    $settings_data = json_decode($settings['settings'], 1);

                    $course_details = isset($settings_data[$student_detail['course_id']]) ? $settings_data[$student_detail['course_id']] : NULL;

                    if ( ! empty($course_details))
                    {
                        if ($course_details['exam-status'] == 'on')
                        {
                            $question_limit = $settings_data[$student_detail['course_id']]['total_question'];
                        }
                    }
                }

                foreach ($quiz_session as $q_id => $ans)
                {
                    $question = $this->Public_question_model->get_question_detail_by(['id' => $q_id]);

                    if (substr($ans, 3) == $question['right_option'])
                    {
                        $result += QUESTION_MARKS;
                    }
                }

                $student_session = get_student_session();

                $result_id = $this->Public_student_model->save_student(
                    [
                        'student_id'     => $student_session['id'],
                        'user_id'        => $student_session['user_id'],
                        'total_marks'    => ($question_limit * QUESTION_MARKS),
                        'marks_obtained' => $result,
                    ],
                    NULL,
                    'result_records'
                );
            }
        }
        else
        {
            redirect('student/profile');
        }

        $dataArray['result'] = $result;
        $dataArray['question_limit'] = $question_limit;

        $dataArray['header_theme'] = 'header2';
        $dataArray['title']        = 'Online Exam Result';
        $dataArray['breadcrumb']   = 'Online Exam Result';

        $dataArray['local_css'] = array();
        $dataArray['local_js']  = array();

        $this->load->view('result', $dataArray);
    }

    public function ajax_get_questions()
    {
        $question = $this->input->post('q_id');
        $answer   = $this->input->post('answer');

        $session = $this->session->userdata('quiz');

        $student_session = get_student_session();
        $settings        = $this->Public_settings_model->get_settings_by();
        $student_detail  = $this->Public_student_model->get_student_by(['id' => $student_session['id']]);

        $question_limit = 0;

        if ( ! empty($settings) && ! empty($student_detail))
        {
            $settings_data = json_decode($settings['settings'], 1);

            $course_details = isset($settings_data[$student_detail['course_id']]) ? $settings_data[$student_detail['course_id']] : NULL;

            if ( ! empty($course_details))
            {
                if ($course_details['exam-status'] == 'on')
                {
                    $question_limit = $settings_data[$student_detail['course_id']]['total_question'];
                }
            }
        }

        /* have to set value of quiz_start or quiz_complete in session */

        if ( ! empty($question))
        {
            if (empty($session))
            {
                $data[$question] = $answer;

                $this->session->set_userdata('quiz', $data);
            }
            else
            {
                $session[$question] = $answer;

                $this->session->set_userdata('quiz', $session);
            }
        }

        $updated_session = $this->session->userdata('quiz');

        if ($question_limit == count($updated_session))
        {
            $this->session->set_userdata('quiz-complete', TRUE);

            echo json_encode(['mesg' => 'quiz_complete', 'redirect' => 'true']);
        }
        else
        {
            $sql = "SELECT * FROM question WHERE id NOT IN (" . implode(',', array_keys($updated_session)) . ") LIMIT 1";

            $question = $this->Public_question_model->get_question_by_query($sql);

            if ( ! empty($question))
            {
                $question['mesg'] = 'success';

                echo json_encode($question);
            }
            else
            {
                echo json_encode(['mesg' => 'error']);
            }
        }
    }

    public function ajax_get_questions_by_id()
    {
        $quiz_session = get_quiz_session();

        $question_id = $this->input->post('q_id');

        $question = $this->Public_question_model->get_question_detail_by(['id' => $question_id]);

        if ( ! empty($question))
        {
            $question['mesg'] = 'success';

            if ( ! empty($quiz_session))
            {
                if (isset($quiz_session[$question_id]) && ! empty($quiz_session[$question_id]))
                {
                    $ans = $quiz_session[$question_id];

                    $question['checked_status'] = 'true';
                    $question['checked_answer'] = substr($ans, 3);
                }
                else
                {
                    $question['checked_status'] = 'false';
                }
            }
            else
            {
                $question['checked_status'] = 'false';
            }

            echo json_encode($question);
        }
        else
        {
            echo json_encode(['mesg' => 'error']);
        }
    }

    public function ajax_check_quiz_time_out()
    {
        $current_time    = date('d-m-Y h:i:s');
        $student_session = get_student_session();

        if ( ! empty($student_session) && ! empty($current_time))
        {
            $quiz_time_session = get_quiz_session('quiz_time');

            if ( ! empty($quiz_time_session))
            {
                if (strtotime($current_time) == strtotime($quiz_time_session['quiz_end_time']))
                {
                    echo json_encode(['status' => 'true']);
                }
                else
                {
                    echo json_encode(['status' => 'false']);
                }
            }
            else
            {
                echo json_encode(['status' => 'false']);
            }
        }
        else
        {
            echo json_encode(['status' => 'false']);
        }
    }

    public function time_out()
    {
        $this->session->set_userdata('quiz-complete', TRUE);

        redirect('result');
    }
}