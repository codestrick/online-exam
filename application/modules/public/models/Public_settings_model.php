<?php

if ( ! defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

class Public_settings_model extends My_Model
{

    public $tbl_name = 'settings';

    /**
     * Public_settings_model constructor.
     */
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * @param array  $params
     * @param string $return_type
     *
     * @return mixed
     */
    function get_settings_by($params = [], $return_type = 'row_array')
    {
        if ( ! empty($params))
        {
            if ($return_type == 'result_array')
            {
                $result = $this->db->get_where($this->tbl_name, $params)->result_array();
            }
            else
            {
                $result = $this->db->get_where($this->tbl_name, $params)->row_array();
            }
        }
        else
        {
            if ($return_type == 'result_array')
            {
                $result = $this->db->get($this->tbl_name)->result_array();
            }
            else
            {
                $result = $this->db->get($this->tbl_name)->row_array();
            }
        }

        return $result;
    }
}
    