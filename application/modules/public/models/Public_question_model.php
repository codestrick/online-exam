<?php

if ( ! defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

class Public_question_model extends My_Model
{

    public $tbl_name = 'question';

    public function __construct()
    {
        parent::__construct();

    }

    function get_question_detail_by($params = [])
    {
        if ( ! empty($params))
        {
            $result = $this->db->get_where($this->tbl_name, $params)->row_array();
        }
        else
        {
            $result = $this->db->get($this->tbl_name)->row_array();
        }

        return $result;
    }

    function get_question_details_by($params = [], $limit = NULL)
    {
        if ( ! empty($params))
        {
            if ( ! empty($limit))
            {
                $result = $this->db->limit($limit)->get_where($this->tbl_name, $params)->result_array();
            }
            else
            {
                $result = $this->db->get_where($this->tbl_name, $params)->result_array();
            }
        }
        else
        {
            if ( ! empty($limit))
            {
                $result = $this->db->limit($limit)->get($this->tbl_name)->result_array();
            }
            else
            {
                $result = $this->db->get($this->tbl_name)->result_array();
            }
        }

        return $result;
    }

    function get_question_by_query($query)
    {
        $result = $this->db->query($query);

        return $result->row_array();
    }
}
    