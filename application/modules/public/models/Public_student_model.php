<?php

if ( ! defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

class Public_student_model extends My_Model
{
    public $tbl_name = 'student';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param array $dataValues
     * @param null  $id
     * @param null  $tbl_name
     *
     * @return null
     */
    public function save_student($dataValues = [], $id = NULL, $tbl_name = NULL)
    {
        $return = NULL;

        if(!empty($tbl_name))
        {
            $this->tbl_name = $tbl_name;
        }

        if ( ! empty($dataValues))
        {
            if ( ! empty($id))
            {
                $this->db->where('id', $id);
                if ($this->db->update($this->tbl_name, $dataValues))
                {
                    $return = $id;
                }
            }
            else
            {
                if ($this->db->insert($this->tbl_name, $dataValues))
                {
                    $return = $this->db->insert_id();
                }
            }
        }

        return $return;
    }

    /**
     * @param $params
     *
     * @return mixed
     */
    function get_student_by($params)
    {
        $query = $this->db->get_where($this->tbl_name, $params);

        return $query->row_array();
    }

    /**
     * @param $username
     * @param $password
     *
     * @return bool
     */
    function login($username, $password)
    {
        $this->db->where('email', $username);
        $this->db->where('password', $password);
        $query = $this->db->get($this->tbl_name);

        if ($query->num_rows() > 0)
        {
            $row = $query->row_array();

            return $row;
        }
        else
        {
            return FALSE;
        }
    }
}
    