<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Public_app_model extends My_Model
{
    public $tbl_name = 'student';

    /**
     * Public_app_model constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param string $table_name
     * @param array $params
     * @param string $return_type
     * @return null
     */
    function get_details_by($table_name = '', $params = [], $return_type = 'row_array')
    {
        $result = null;

        if (!empty($table_name)) {
            if (!empty($params)) {
                if ($return_type == 'result_array') {
                    $result = $this->db->get_where($table_name, $params)->result_array();
                } else {
                    $result = $this->db->get_where($table_name, $params)->row_array();
                }
            } else {
                if ($return_type == 'result_array') {
                    $result = $this->db->get($table_name)->result_array();
                } else {
                    $result = $this->db->get($table_name)->row_array();
                }
            }
        }

        return $result;
    }
}