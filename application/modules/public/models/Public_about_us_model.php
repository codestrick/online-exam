<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Public_about_us_model extends My_Model
{

    public $tbl_name = 'about_us';

    public function __construct()
    {
        parent::__construct();
    }

    function get_detail($cond)
    {
        $query = $this->db->get_where($this->tbl_name, $cond);

        return $query->row_array();
    }

}
