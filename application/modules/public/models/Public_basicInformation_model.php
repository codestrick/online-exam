<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Public_basicInformation_model extends My_Model
{

    public $tbl_name = 'basic_information';

    public function __construct()
    {
        parent::__construct();
    }


    function get_info_detail($cond)
    {
        $query = $this->db->get_where($this->tbl_name, $cond);

        return $query->row_array();
    }

}
