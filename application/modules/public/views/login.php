<div class="our-services-w3">
    <div class="container">
        <h3 class="tittle">Student Login</h3>
        <hr>
        <?php
        if ( ! empty($flash_message))
        {
            $flash_message_status = ! isset($flash_message_status) ? 1 : $flash_message_status;
            ?>
            <div class="alert <?php echo $flash_message_status == 1 ? 'alert-success' : 'alert-danger' ?>">
                <button type="button" class="close text-right" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $flash_message; ?>
            </div>
            <?php
        }

        $validation_errors = validation_errors();
        if ( ! empty($validation_errors))
        {
            ?>
            <div class="col-sm-12">
                <div class="col-xs-12 alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert"
                            aria-hidden="true">
                        &times;
                    </button>
                    <?php echo $validation_errors; ?>
                </div>
            </div>
            <?php
        }
        ?>
        <div class="ourser-grids">
            <div class="col-md-12 col-xs-12">
                <div class="row">
                    <div class="col-md-4 col-xs-1"></div>
                    <div class="col-md-4 col-xs-10">
                        <div style="border: 1px solid darkgrey; border-radius: 4px; padding: 10px">
                            <br>
                            <form action="<?php echo current_url(); ?>" method="post" id="user_login">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="text" name="username" id="username" placeholder="Username" class="form-control">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="password" name="password" id="password" placeholder="Password" class="form-control">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <br>
                                        <input type="submit" class="btn btn-danger" value="Login">
                                    </div>
                                </div>
                            </form>
                            <br>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-1"></div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>