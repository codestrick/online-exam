<!--about-->
<div class="about-w3">
    <div class="container">
        <h2 class="tittle">About Us</h2>
        <div class="about-grids">
            <div class="col-md-12">

                <h4><?php echo $heading; ?></h4>
                <p><?php echo $content; ?></p>
                <ul class="grid-part">

                    <?php
                    for($i = 0; $i < 5; $i++){
                        $key_name = 'point_'.($i+1);
                        ?>

                        <li><i class="glyphicon glyphicon-ok-sign"> </i><?php echo $$key_name ?></li>

                    <?php
                    }
                    ?>
                </ul>
            </div>

            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!--about-->
<!--What we do-->
<div class="what-w3">
    <div class="container">
        <h3 class="tittle">What we do</h3>
        <div class="what-grids">
            <div class="col-md-6 what-grid">
                <div class="mask">
                    <img src="<?php echo base_url(); ?>assets/public/images/9.jpg" class="img-responsive zoom-img"/>
                </div>
                <div class="what-bottom">
                    <h4><?php echo $we_do_heading_1 ?></h4>
                    <p><?php echo $we_do_description_1 ?></p>
                </div>
            </div>
            <div class="col-md-6 what-grid">
                <div class="what-bottom1">
                    <h4><?php echo $we_do_heading_2 ?></h4>
                    <p><?php echo $we_do_description_2 ?></p>
                </div>
                <div class="mask">
                    <img src="<?php echo base_url(); ?>assets/public/images/8.jpg" class="img-responsive zoom-img"/>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!--What we do-->
<!--statistics--->
<div class="statistics-w3">
    <div class="container">
        <h3 class="tittle">Fun Facts</h3>
        <div class="statistics-grids">
            <div class="col-md-3 statistics-grid">
                <div class="statistic">
                    <h4><?php echo $fun_percentage_1 ?>%</h4>
                    <h5><?php echo $fun_point_1 ?></h5>
                    <p><?php echo $fun_description_1 ?></p>
                </div>
            </div>
            <div class="col-md-3 statistics-grid">
                <div class="statistic">
                    <h4><?php echo $fun_percentage_2 ?>%</h4>
                    <h5><?php echo $fun_point_2 ?></h5>
                    <p><?php echo $fun_description_2 ?></p>
                </div>
            </div>
            <div class="col-md-3 statistics-grid">
                <div class="statistic">
                    <h4><?php echo $fun_percentage_3 ?>%</h4>
                    <h5><?php echo $fun_point_3 ?></h5>
                    <p><?php echo $fun_description_3 ?></p>
                </div>
            </div>
            <div class="col-md-3 statistics-grid">
                <div class="statistic">
                    <h4><?php echo $fun_percentage_4 ?>%</h4>
                    <h5><?php echo $fun_point_4 ?></h5>
                    <p><?php echo $fun_description_4 ?></p>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!--statistics--->
<!--staff--->
<div class="staff-w3l">
    <div class="container">
        <h3 class="tittle">Our Staff</h3>
        <?php

        if(!empty($staffes)){
            foreach($staffes as $k => $values){
                ?>
        <div class="staff-grids">

            <?php
            foreach($values as $key => $value){
                if(!empty($value['id'])) {
                    ?>
                    <div class="col-md-6 staff-grid">
                        <div class="staff-left">
                            <img src="<?php echo base_url(); ?>assets/uploads/staff/images/<?php echo $value['pic']; ?>" class="img-responsive" alt="/" style="width: 270px;height: 220px">
                        </div>
                        <div class="staff-right">
                            <h4><?php echo $value['name']; ?></h4>
                            <ul>
                                <li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i> Email : <?php echo $value['email_id']; ?></li>
                                <li><i class="glyphicon glyphicon-phone" aria-hidden="true"></i> Mobile : <?php echo $value['mobile_number']; ?></li>
                            </ul>

                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <?php
                }
            }
            ?>
            <div class="clearfix"></div>
        </div>
        <?php

            }
        }

        ?>

    </div>
</div>
<!--staff--->