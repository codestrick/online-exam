<div class="our-services-w3">
    <div class="container">
        <h3 class="tittle">My Profile</h3>
        <hr>
        <?php
        if ( ! empty($flash_message))
        {
            $flash_message_status = ! isset($flash_message_status) ? 1 : $flash_message_status;
            ?>
            <div class="alert <?php echo $flash_message_status == 1 ? 'alert-success' : 'alert-danger' ?>">
                <button type="button" class="close text-right" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $flash_message; ?>
            </div>
            <?php
        }


        $validation_errors = validation_errors();
        if ( ! empty($validation_errors))
        {
            ?>
            <div class="col-sm-12">
                <div class="col-xs-12 alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert"
                            aria-hidden="true">
                        &times;
                    </button>
                    <?php echo $validation_errors; ?>
                </div>
            </div>
            <?php
        }
        ?>
        <div class="ourser-grids">
            <div class="col-md-12 col-xs-12">

                <ul class="nav nav-tabs custom-tab">
                    <li class="active"><a data-toggle="tab" href="#home">Your Profile</a></li>
                    <li><a data-toggle="tab" href="#menu1">Results</a></li>
                    <li><a data-toggle="tab" href="#menu2">Change Password</a></li>
                    <li><a href="<?php echo base_url('student/logout'); ?>" style="background-color: red;color: whitesmoke">Logout</a></li>
                </ul>

                <div class="tab-content">
                    <div id="home" class="tab-pane fade in active">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <th>Roll Number</th>
                                <th>Name</th>
                                <th>E-mail</th>
                                <th>Mobile</th>
                                <th>Address</th>
                                <th>Pincode</th>
                                </thead>
                                <tbody>
                                <?php
                                if ( ! empty($student_details))
                                {
                                    echo '<td>' . $student_details['student_roll'] . '</td>';
                                    echo '<td>' . $student_details['name'] . '</td>';
                                    echo '<td>' . $student_details['email'] . '</td>';
                                    echo '<td>' . $student_details['mobile'] . '</td>';
                                    echo '<td>' . $student_details['address'] . '</td>';
                                    echo '<td>' . $student_details['pincode'] . '</td>';
                                }
                                else
                                {
                                    echo '<td colspan="6" class="text-center">' . 'No data found' . '</td>';
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="menu1" class="tab-pane fade">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Total Marks</th>
                                    <th>Marks Obtained</th>
                                    <th>Exam Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if ( ! empty($result_records))
                                {
                                    foreach ($result_records as $key => $val)
                                    {
                                        echo '<tr>';
                                        echo '<td>' . $val['total_marks'] . '</td>';
                                        echo '<td>' . $val['marks_obtained'] . '</td>';
                                        echo '<td>' . date('d-m-Y h:i:s', strtotime($val['created_at'])) . '</td>';
                                        echo '</tr>';
                                    }
                                }
                                else
                                {
                                    echo '<tr><th colspan="3" class="text-center"> No record found!</th></tr>';
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="menu2" class="tab-pane fade">
                        <h3>Change Password</h3>
                        <hr>
                        <form action="<?php echo base_url('student/change-password'); ?>" id="change_password" method="post">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-3">
                                        <input type="text" name="old_pass" id="old_pass" placeholder="Old Password" class="form-control">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <input type="password" name="new_pass" id="new_pass" placeholder="New Password" class="form-control">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <input type="password" name="re_enter_new_pass" id="re_enter_new_pass" placeholder="Re Enter New Password" class="form-control">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 text-center">
                                        <br>
                                        <input type="submit" class="btn btn-success" value="Submit">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div id="menu3" class="tab-pane fade">
                        <h3>Logout</h3>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>