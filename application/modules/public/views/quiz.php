<!--quiz-page -->
<div class="typo-w3">
    <div class="container">
        <h2 class="tittle">Online Exam</h2>
        <hr>
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="col-md-9">
                    <form id="quiz" method="post">
                        <?php
                        if ( ! empty($questions))
                        {
                            ?>
                            <table width="auto">
                                <?php
                                $session = get_quiz_session();

                                foreach ($questions as $key => $value)
                                {
                                    if ( ! empty($session))
                                    {
                                        if (isset($session[$value['id']]))
                                        {
                                            $choosed_answer = $session[$value['id']];
                                            $radio_option_1 = '';
                                            $radio_option_2 = '';
                                            $radio_option_3 = '';
                                            $radio_option_4 = '';

                                            if (substr($choosed_answer, 3) == 1)
                                            {
                                                $radio_option_1 = 'checked';
                                            }
                                            else if (substr($choosed_answer, 3) == 2)
                                            {
                                                $radio_option_2 = 'checked';
                                            }
                                            else if (substr($choosed_answer, 3) == 3)
                                            {
                                                $radio_option_3 = 'checked';
                                            }
                                            else if (substr($choosed_answer, 3) == 4)
                                            {
                                                $radio_option_4 = 'checked';
                                            }
                                        }
                                        else
                                        {
                                            $radio_option_1 = '';
                                            $radio_option_2 = '';
                                            $radio_option_3 = '';
                                            $radio_option_4 = '';
                                        }
                                    }
                                    else
                                    {
                                        $radio_option_1 = '';
                                        $radio_option_2 = '';
                                        $radio_option_3 = '';
                                        $radio_option_4 = '';
                                    }
                                    ?>
                                    <tr>
                                        <td><input type="hidden" name="q_id" id="q_id" value="<?php echo $value["id"]; ?>"></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>
                                                <h2 id="qstn"><?php echo 'Q: ' . $value["question"] ?></h2>
                                            </b>
                                            <hr>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="radio" name="answer" id="option_1" value="ans1" <?php echo $radio_option_1; ?>>
                                            &nbsp;
                                            <label id="ans1"> <?php echo $value["option_1"]; ?></label>
                                            <br><br>
                                            <input type="radio" name="answer" id="option_2" value="ans2"<?php echo $radio_option_2; ?>>
                                            &nbsp;
                                            <label id="ans2"> <?php echo $value["option_2"]; ?></label>
                                            <br><br>
                                            <input type="radio" name="answer" id="option_3" value="ans3"<?php echo $radio_option_3; ?>>
                                            &nbsp;
                                            <label id="ans3"> <?php echo $value["option_3"]; ?></label>
                                            <br><br>
                                            <input type="radio" name="answer" id="option_4" value="ans4"<?php echo $radio_option_4; ?>>
                                            &nbsp;
                                            <label id="ans4"> <?php echo $value["option_4"]; ?></label>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </table>
                            <div class="form-group text-center">
                                <hr>
                                <input type="submit" name="submit" value="Next >" class="btn btn-info">
                            </div><?php
                        }
                        else
                        {
                            echo '<h3>No Questions Found.</h3>';
                        }
                        ?>
                    </form>
                </div>
                <div class="col-md-3 text-center">
                    <?php
                    if ( ! empty($all_questions))
                    {
                        $quiz_session = get_quiz_session();

                        echo '<h5>Click below buttons to go questions:</h5>';
                        echo '<hr>';

                        $counter          = 0;
                        $question_counter = 0;
                        foreach ($all_questions as $key => $value)
                        {
                            $question_counter = $question_counter + 1;

                            if (count($all_questions) > 10)
                            {
                                if ($counter == 4)
                                {
                                    $counter = 0;
                                }
                            }
                            else
                            {
                                if ($counter == 5)
                                {
                                    $counter = 0;
                                }
                            }

                            if ( ! empty($quiz_session))
                            {
                                if (isset($quiz_session[$value['id']]))
                                {
                                    $btn_class = 'btn btn-success';
                                }
                                else
                                {
                                    $btn_class = 'btn btn-info';
                                }
                            }
                            else
                            {
                                $btn_class = 'btn btn-info';
                            }
                            ?>
                            <button type="button" class="<?php echo $btn_class; ?> button_questions"
                                    data-question="<?php echo $value['id']; ?>"><?php echo 'Q' . $question_counter; ?></button>
                            <?php
                            $counter = $counter + 1;

                            if (count($all_questions) > 10)
                            {
                                if ($counter == 4)
                                {
                                    echo '<hr>';
                                }
                            }
                            else
                            {
                                if ($counter == 5)
                                {
                                    echo '<hr>';
                                }
                            }
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- //quiz-page -->


<script id="source" language="javascript" type="text/javascript">

    window.onload = function () {
        document.onkeydown = function (e) {
            if (e.key == 'Control')
            {
                e.preventDefault();
                return (e.which || e.keyCode) != 16;
            }
            else
            {
                e.preventDefault();
                return (e.which || e.keyCode) != 116;
            }
        };
    }

    function check_quiz_time_out()
    {
        $.ajax({
            method: "post",
            url: base_url + 'public/quiz/ajax_check_quiz_time_out',
            dataType: 'json',
            success: function (response, e, x) {
                if (response.status == 'true')
                {
                    window.location.replace(base_url + 'quiz/time-out');
                }
            }
        });
    }

    $(document).ready(function () {

        window.setInterval(function () {
            check_quiz_time_out();
        }, 60000);

        $(".button_questions").click(function (e) {

            e.preventDefault();

            $.blockUI();

            $.ajax({
                method: "post",
                url: base_url + 'public/quiz/ajax_get_questions_by_id',
                data: {
                    q_id: $(this).data('question')
                },
                dataType: 'json',
                success: function (response, e, x) {

                    $.unblockUI();

                    console.log(response);

                    if (response.mesg == 'success')
                    {
                        $("#q_id").val(response.id);
                        $("#qstn").html('Q: ' + response.question);
                        $("#ans1").html(response.option_1);
                        $("#ans2").html(response.option_2);
                        $("#ans3").html(response.option_3);
                        $("#ans4").html(response.option_4);

                        $("input[name='answer']:checked").prop('checked', false);

                        if (response.checked_status == 'true')
                        {
                            if (response.checked_answer == 1)
                            {
                                $("#option_1").prop('checked', true);
                            }
                            else if (response.checked_answer == 2)
                            {
                                $("#option_2").prop('checked', true);
                            }
                            else if (response.checked_answer == 3)
                            {
                                $("#option_3").prop('checked', true);
                            }
                            else if (response.checked_answer == 4)
                            {
                                $("#option_4").prop('checked', true);
                            }
                        }
                    }
                    else
                    {
                        alert('Some Error Occurred!');
                    }
                }
            });
        });

        $("#quiz").submit(function (e) {

            e.preventDefault();

            $.blockUI();

            var SelectedAnswer = $("input[name='answer']:checked").val();
            var question = $('#q_id').val();

            if ($("input[name='answer']:checked").length > 0)
            {
                $('.button_questions').each(function (e) {

                    console.log(this);

                    if (question == $(this).data('question'))
                    {
                        if ($(this).hasClass('btn btn-info'))
                        {
                            $(this).removeClass('btn btn-info');
                            $(this).addClass('btn btn-success');
                        }
                    }
                });
            }

            $.ajax({
                method: "post",
                url: base_url + 'public/quiz/ajax_get_questions',
                data: {
                    q_id: $('#q_id').val(),
                    answer: $("input[name='answer']:checked").val()
                },
                dataType: 'json',
                success: function (response, e, x) {

                    $.unblockUI();

                    console.log(response);

                    if (response.mesg == 'success')
                    {
                        $("#q_id").val(response.id);
                        $("#qstn").html('Q: ' + response.question);
                        $("#ans1").html(response.option_1);
                        $("#ans2").html(response.option_2);
                        $("#ans3").html(response.option_3);
                        $("#ans4").html(response.option_4);

                        $("input[name='answer']:checked").prop('checked', false);
                    }
                    else if (response.mesg == 'quiz_complete')
                    {
                        window.location.href = 'result';
                    }
                    else
                    {
                        alert('Some Error Occurred!');
                    }
                }
            });
        });
    });
</script>