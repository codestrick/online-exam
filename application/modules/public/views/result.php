<div class="our-services-w3">
    <div class="container">
        <h3 class="tittle">Online Exam Result</h3>
        <hr>
        <div class="ourser-grids">
            <div class="col-md-12 col-xs-12">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <div class="panel panel-danger">
                        <div class="panel-heading text-center">Online Exam Result</div>
                        <div class="panel-body">
                            <table align="center">
                                <tr>
                                    <th class="text-right">Total Questions</th>
                                    <th style="padding-left: 15px;padding-right: 15px">:</th>
                                    <td><?php echo ! empty($question_limit) ? $question_limit : ''; ?></td>
                                </tr>
                                <tr>
                                    <th class="text-right">Total Score</th>
                                    <th style="padding-left: 15px;padding-right: 15px">:</th>
                                    <td><?php echo ! empty($question_limit) ? ($question_limit * QUESTION_MARKS) : ''; ?></td>
                                </tr>
                                <tr>
                                    <th class="text-right">Your Score</th>
                                    <th style="padding-left: 15px;padding-right: 15px">:</th>
                                    <td><?php echo ! empty($result) ? $result : 0; ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-4"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>