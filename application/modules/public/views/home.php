<!--welcome-->
<div class="welcome-w3">
    <div class="container">
        <h2 class="tittle">Welcome To Our University </h2>
        <div class="wel-grids">
            <div class="col-md-4 wel-grid">
                <div class="port-7 effect-2">
                    <div class="image-box">
                        <img src="<?php echo base_url(); ?>assets/public/images/w1.jpg" class="img-responsive" alt="Image-2">
                    </div>
                    <div class="text-desc">
                        <h4>Studies</h4>
                    </div>
                </div>
                <div class="port-7 effect-2">
                    <div class="image-box">
                        <img src="<?php echo base_url(); ?>assets/public/images/w2.jpg" class="img-responsive" alt="Image-2">
                    </div>
                    <div class="text-desc">
                        <h4>Studies</h4>
                    </div>
                </div>
            </div>
            <div class="col-md-4 wel-grid">
                <img src="<?php echo base_url(); ?>assets/public/images/w3.jpg"  class="img-responsive" alt="Image-2">
                <div class="text-grid">
                    <h4>Studies</h4>
                </div>
            </div>
            <div class="col-md-4 wel-grid">
                <div class="port-7 effect-2">
                    <div class="image-box">
                        <img src="<?php echo base_url(); ?>assets/public/images/w4.jpg" class="img-responsive" alt="Image-2">
                    </div>
                    <div class="text-desc">
                        <h4>Studies</h4>
                    </div>
                </div>
                <div class="port-7 effect-2">
                    <div class="image-box">
                        <img src="<?php echo base_url(); ?>assets/public/images/w5.jpg" class="img-responsive" alt="Image-2">
                    </div>
                    <div class="text-desc">
                        <h4>Studies</h4>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!--welcome-->
<!--student-->
<div class="student-w3ls">
    <div class="container">
        <h3 class="tittle"> Our Students</h3>
        <div class="student-grids">
            <div class="col-md-6 student-grid">
                <h4><?php echo $heading; ?></h4>
                <p><?php echo $content ?></p>
                <ul class="grid-part">

                    <?php
                    for($i = 0; $i < 5; $i++){
                        $key_name = 'point_'.($i+1);
                        ?>

                        <li><i class="glyphicon glyphicon-ok-sign"> </i><?php echo $$key_name; ?></li>

                        <?php
                    }
                    ?>
                </ul>

            </div>
            <div class="col-md-6 student-grid">
                <img src="<?php echo base_url(); ?>assets/public/images/w6.jpg" class="img-responsive" alt="Image-2">
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>



<!--student-->
<!--testimonials-->



<!--<div class="testimonials-w3">-->
<!--    <div class="container">-->
<!--        <h3 class="tittle2">Testimonials</h3>-->
<!--        <div class="test-grid">-->
<!--            <img src="--><?php //echo base_url(); ?><!--assets/public/images/quote.png" alt=/>-->
<!--            <h5>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu-->
<!--                fugiat nulla pariatur.</h5>-->
<!--            <p><i>Thomas Bishop</i></p>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->



<!--testimonials-->
<!--news-->


<!--<div class="new-w3agile">-->
<!--    <div class="container">-->
<!--        <div class="new-grids">-->
<!--            <div class="col-md-4 new-left">-->
<!--                <h3 class="tittle1">Latest Events</h3>-->
<!--                <div class="new-top">-->
<!--                    <h5>Monday june 16</h5>-->
<!--                    <p>Proin fermentum diam eu massa blandit, congue finibus ante pulvinar. Aliquam lacinia odio eros.</p>-->
<!--                </div>-->
<!--                <div class="new-top1">-->
<!--                    <h5>Tuesday june 16</h5>-->
<!--                    <p>Proin fermentum diam eu massa blandit, congue finibus ante pulvinar. Aliquam lacinia odio eros.</p>-->
<!--                </div>-->
<!--                <div class="new-top">-->
<!--                    <h5>Wednesday june 16</h5>-->
<!--                    <p>Proin fermentum diam eu massa blandit, congue finibus ante pulvinar. Aliquam lacinia odio eros.</p>-->
<!--                </div>-->
<!--                <div class="new-top1">-->
<!--                    <h5>Thursday june 16</h5>-->
<!--                    <p>Proin fermentum diam eu massa blandit, congue finibus ante pulvinar. Aliquam lacinia odio eros.</p>-->
<!--                </div>-->
<!--                <div class="new-top">-->
<!--                    <h5>Friday june 16</h5>-->
<!--                    <p>Proin fermentum diam eu massa blandit, congue finibus ante pulvinar. Aliquam lacinia odio eros.</p>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-md-8 new-right">-->
<!--                <h3 class="tittle1">Latest News</h3>-->
<!--                <h4>Excepteur sint occaecat cupidatat non proident</h4>-->
<!--                <p>Quisque at tellus ullamcorper, pharetra arcu a, suscipit purus. Nullam feugiat in augue in consequat. Sed ac dictum ligula, et pellentesque velit. In gravida eu felis sit amet molestie. Morbi sed ex ac enim finibus vulputate. Cras arcu magna, auctor ornare neque in, finibus tincidunt augue.</p>-->
<!--                <div class="new-bottom">-->
<!--                    <div class="new-bottom-left">-->
<!--                        <img src="--><?php //echo base_url(); ?><!--assets/public/images/w7.jpg" class="img-responsive" alt="">-->
<!--                    </div>-->
<!--                    <div class="new-bottom-right">-->
<!--                        <h5>Morbi sed ex ac enim finibus vulputate.</h5>-->
<!--                        <p>Nullam egestas diam eu felis dignissim, vitae posuere ex pretium. Morbi quam purus, rhoncus eget enim sed, laoreet venenatis arcu. Curabitur vestibulum, orci pulvinar pretium cursus, diam ante pretium elit, at finibus nisl ligula vitae enim. eu eleifend enim condimentum ac.</p>-->
<!--                    </div>-->
<!--                    <div class="clearfix"></div>-->
<!--                </div>-->
<!--                <h4>Excepteur sint occaecat cupidatat non proident</h4>-->
<!--                <p>Quisque at tellus ullamcorper, pharetra arcu a, suscipit purus. Nullam feugiat in augue in consequat. Sed ac dictum ligula, et pellentesque velit. In gravida eu felis sit amet molestie. Morbi sed ex ac enim finibus vulputate. Cras arcu magna, auctor ornare neque in, finibus tincidunt augue.</p>-->
<!--            </div>-->
<!--            <div class="clearfix"></div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->



<!--news-->