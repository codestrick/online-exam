<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

function check_access($module, $user_type, $permission_type, $redirect_status = FALSE, $redirect_to = "admin/dashboard")
{
    $CI = &get_instance();

    $return = FALSE;

    if (!empty($module) && !empty($user_type) && !empty($permission_type)) {
        if ($user_type == "1") {
            $return = TRUE;
        } else {
            $CI->load->model('Usertype_model');
            $return = $CI->Usertype_model->get_user_permission($module, $user_type, $permission_type);
        }
    }
    if (empty($return) && !empty($redirect_status)) {
        redirect($redirect_to);
    } else {
        return $return;
    }
}

function get_loggedin_user_data($index = 'online_exam_admin')
{
    $CI = &get_instance();

    if (!empty($CI->session->userdata[$index]['user'])) {
        $userdata = (array)$CI->session->userdata[$index]['user'];
    } else {
        $userdata = array();
    }

    return $userdata;
}

function GetLoggedinApplicantData($index = 'online_exam_admin')
{
    $CI = &get_instance();


    if (!empty($CI->session->userdata[$index]['applicationdata'])) {
        $applicantdata = (array)$CI->session->userdata[$index]['applicationdata'];
    } else {
        $applicantdata = array();
    }

    return $applicantdata;
}

function get_loggedin_admin_user_data()
{
    $CI = &get_instance();
    $userdata = (array)$CI->session->userdata;

    return $userdata;
}

function get_image($path)
{
    $return = '';
    if (!empty($path)) {
        $return = base_url() . "assets/uploads/" . $path;
    }

    return $return;
}

function get_recommended_movie_url($movie_id, $movie_name)
{
    $url = base_url() . "movie/" . url_title($movie_name) . "/" . $movie_id;

    return $url;
}

function get_preview_link($image, $config = '', $base_url = '')
{
    $image_custom = getCustomConfigItem($config);
    $system_path = '';
    $preview_link = '';
    $tmp_base_url = '';
    if (!isset($base_url) || $base_url == '') {
        $tmp_base_url = base_url();
    } else {
        $tmp_base_url = $base_url;
    }
    if (!empty($image)) {
        $system_path = set_realpath($image_custom["upload_path"]);
        $image_path = $system_path . $image;
        $img_url = base_url() . $image_custom['upload_path'] . $image;
        if (file_exists($image_path)) {
            $preview_link = '<a rel="' . $tmp_base_url . $image . '" href="' . $img_url . '" class="preview preview_link_show">Click here to preview <i class="action-icon fa fa-image"></i></a>';
        }
    }

    return $preview_link;
}

function get_movie_image($image, $config)

{
    $return = '';
    $system_path = set_realpath($config["upload_path"]);
    $image_path = $system_path . $image;
    $img_url = base_url() . $config['upload_path'] . $image;
    if (file_exists($image_path)) {

        $return = $img_url;
    }

    return $return;
}

function get_formatted_address($arr)
{
    $address = '';

    $arr_fields = array(
        'address_1',
        'address_2',
        'address_3',
        'postcode',
        'country',
    );

    foreach ($arr_fields as $field) {
        if (trim($field) != '') {
            $address .= $arr[$field] . ", ";
        }
    }

    if ($address != '') {
        $address = substr($address, 0, -2);
    }

    return $address;
}

function get_img_for_list($source_id, $other_data)
{

    $image_config = getCustomConfigItem("slider_image_config");

    $img_url = base_url() . $image_config['upload_path'] . $image_config['default'];
    if (!empty($other_data->image)) {
        $system_path = set_realpath($image_config["upload_path"]);
        $image_path = $system_path . $other_data->image;
        $img_url = base_url() . $image_config['upload_path'] . $other_data->image;
        if (!file_exists($image_path)) {
            $img_url = base_url() . $image_config['upload_path'] . $image_config['default'];
        }
    }

    $img = "<img src='" . $img_url . "' alt='" . $other_data->title . "' class='default_tag_img'>";
    return $img;
}

function get_login_user_session()
{
    $CI = &get_instance();
    $session = $CI->session->userdata('online_exam_admin');

    if(!empty($session['user']))
    {

       return $data = $session['user'];
    }
    else
    {
        return $data = [];
    }
}