<?php

if ( ! defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

function get_basic_info(){
    $CI = &get_instance();
    $CI->load->model('Public_basicInformation_model');

   return $result =  $CI->Public_basicInformation_model->get_info_detail([]);
}

function get_about_info(){
    $CI = &get_instance();
    $CI->load->model('Public_about_us_model');

   return $result =  $CI->Public_about_us_model->get_detail([]);
}

function generate_random_string($length = 10)
{
    $characters       = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString     = '';
    for ($i = 0; $i < $length; $i++)
    {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }

    return $randomString;
}

function object_to_array($obj)
{
    if (is_object($obj))
    {
        $obj = (array)$obj;
    }
    if (is_array($obj))
    {
        $new = array();
        foreach ($obj as $key => $val)
        {
            $new[$key] = object_to_array($val);
        }
    }
    else
    {
        $new = $obj;
    }

    return $new;
}

function getDefaultlanguage()
{
    $CI = &get_instance();
    $CI->load->model('Language_model');
    $language = $CI->Language_model->getDefaultlanguage();

    return $language;
}

function addhttp($url)
{
    if ( ! empty($url))
    {
        if ( ! preg_match("~^(?:f|ht)tps?://~i", $url))
        {
            $url = "http://" . $url;
        }
    }

    return $url;
}

function add_blank_option($options, $blank_option = '')
{
    if (is_array($options) && is_string($blank_option))
    {
        if (empty($blank_option))
        {
            $blank_option = array('' => '');
        }
        else
        {
            $blank_option = array('' => $blank_option);
        }
        $options = $blank_option + $options;

        return $options;
    }
    else
    {
        show_error("Wrong options array passed");
    }
}

function getCustomConfigItem($key, $config = 'custom')
{
    $CI = &get_instance();

    $arr_custom_config = $CI->config->item($config);

    $config_item = $arr_custom_config[$key];

    return $config_item;
}

function get_current_user_access()
{
    $CI                = &get_instance();
    $online_exam_admin = $CI->session->userdata('online_exam_admin');

    return $online_exam_admin;
}

function getsessionid()
{
    $sessionid = PHPSESSID;  //$CI->session->userdata('sessionid');

    return $sessionid;
}

function base64url_encode($data)
{

    return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
}

function base64url_decode($data)
{

    return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
}

function mydateformat($date, $from_format = "d-m-Y", $to_format = "Y-m-d")
{
    if ($date == "")
    {
        $return_date = "0000-00-00";
    }
    else
    {
        $date        = DateTime::createFromFormat($from_format, $date);
        $return_date = $date->format($to_format);
    }

    return $return_date;
}

function create_captcha_common()
{
    $CI = &get_instance();
    $CI->load->helper('captcha');

    $vals = array(
        'word'       => randomPassword(6),
        'img_path'   => APPPATH . 'uploads/captcha/images/',
        'img_url'    => base_url() . 'application/uploads/captcha/images/',
        'font_path'  => APPPATH . 'uploads/captcha/OpenSans-Regular.ttf',
        'img_width'  => 150,
        'img_height' => 60,
        'expiration' => 7200
    );
    $cap  = create_captcha($vals);

    $_SESSION['thetopupstore']['captcha'] = $cap;

    return $cap;
}

function randomPassword($len = 16)
{
    $alphabet    = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass        = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < $len; $i++)
    {
        $n      = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }

    return implode($pass); //turn the array into a string
}

function getclientip()
{
    if ( ! empty($_SERVER['HTTP_CLIENT_IP']))
    {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    }
    else if ( ! empty($_SERVER['HTTP_X_FORWARDED_FOR']))
    {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
        $ip = $_SERVER['REMOTE_ADDR'];
    }

    return $ip;
}

function getlocationfromip($ipAddr)
{
    $url = "http://api.ipinfodb.com/v3/ip-city/?key=5cfaab6c5af420b7b0f88d289571b990763e37b66761b2f053246f9db07ca913&ip=$ipAddr&format=json";
    $ch  = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $data = curl_exec($ch);
    curl_close($ch);;
    $arr = json_decode($data);
    if ( ! empty($arr))
    {
        $array['country'] = $arr->countryName;
        $array['state']   = $arr->regionName;
        $array['city']    = $arr->cityName;
    }
    else
    {
        return NULL;
    }

    return $array;
}

function relativetime($timestamp)
{

    if ( ! is_numeric($timestamp))
    {

        $timestamp = strtotime($timestamp);
        if ( ! is_numeric($timestamp))
        {
            return "";
        }
    }

    $difference = time() - $timestamp;
    // Customize in your own language.
    $periods = array("sec", "min", "hour", "day", "week", "month", "years", "decade");
    $lengths = array("60", "60", "24", "7", "4.35", "12", "10");

    if ($difference > 0)
    { // this was in the past
        $ending = "ago";
    }
    else
    { // this was in the future
        $difference = -$difference;
        $ending     = "to go";
    }
    for ($j = 0; $difference >= $lengths[$j] and $j < 7; $j++)
    {
        $difference /= $lengths[$j];
    }
    $difference = round($difference);
    if ($difference != 1)
    {
        // Also change this if needed for an other language
        $periods[$j] .= "s";
    }
    $text = "$difference $periods[$j] $ending";

    return $text;
}

function unique_multidim_array($array, $key)
{
    $temp_array = array();
    $i          = 0;
    $key_array  = array();

    foreach ($array as $val)
    {
        if ( ! in_array($val[$key], $key_array))
        {
            $key_array[$i]  = $val[$key];
            $temp_array[$i] = $val;
        }
        $i++;
    }

    return $temp_array;
}

function get_mime_type($file)
{
    $return = NULL;
    if (isset($_FILES[$file]['tmp_name']))
    {
        $finfo  = finfo_open(FILEINFO_MIME_TYPE);
        $mime   = finfo_file($finfo, $_FILES[$file]['tmp_name']);
        $return = $mime;
    }

    return $return;
}

function floatnumber($number)
{
    $result = number_format((float)$number, 2, '.', '');

    return $result;
}

function emptyElementExists($arr)
{
    return array_search("", $arr) !== FALSE;
}

function make_dropdown_array($arr, $key_col = 'id', $val_col = 'title')
{
    $return = array();
    if ( ! empty($arr))
    {
        foreach ($arr as $key => $value)
        {
            $return[$value[$key_col]] = $value[$val_col];
        }
    }

    return $return;
}

function generate_access_token($doctor_id)
{
    $random       = generate_random_string(10);
    $access_token = base64_encode($doctor_id . $random . time());

    return $access_token;
}

function dateRange($first, $last, $step = '+1 day', $format = 'Y-m-d')
{
    $dates   = [];
    $current = strtotime($first);
    $last    = strtotime($last);

    while ($current <= $last)
    {
        $dates[] = date($format, $current);
        $current = strtotime($step, $current);
    }

    return $dates;
}

function get_all_country_list()
{
    $countries = array(
        ""   => "",
        "AU" => "Australia",
        "AF" => "Afghanistan",
        "AL" => "Albania",
        "DZ" => "Algeria",
        "AS" => "American Samoa",
        "AD" => "Andorra",
        "AO" => "Angola",
        "AI" => "Anguilla",
        "AQ" => "Antarctica",
        "AG" => "Antigua & Barbuda",
        "AR" => "Argentina",
        "AM" => "Armenia",
        "AW" => "Aruba",
        "AT" => "Austria",
        "AZ" => "Azerbaijan",
        "BS" => "Bahamas",
        "BH" => "Bahrain",
        "BD" => "Bangladesh",
        "BB" => "Barbados",
        "BY" => "Belarus",
        "BE" => "Belgium",
        "BZ" => "Belize",
        "BJ" => "Benin",
        "BM" => "Bermuda",
        "BT" => "Bhutan",
        "BO" => "Bolivia",
        "BA" => "Bosnia/Hercegovina",
        "BW" => "Botswana",
        "BV" => "Bouvet Island",
        "BR" => "Brazil",
        "IO" => "British Indian Ocean Territory",
        "BN" => "Brunei Darussalam",
        "BG" => "Bulgaria",
        "BF" => "Burkina Faso",
        "BI" => "Burundi",
        "KH" => "Cambodia",
        "CM" => "Cameroon",
        "CA" => "Canada",
        "CV" => "Cape Verde",
        "KY" => "Cayman Is",
        "CF" => "Central African Republic",
        "TD" => "Chad",
        "CL" => "Chile",
        "CN" => "China, People's Republic of",
        "CX" => "Christmas Island",
        "CC" => "Cocos Islands",
        "CO" => "Colombia",
        "KM" => "Comoros",
        "CG" => "Congo",
        "CD" => "Congo, Democratic Republic",
        "CK" => "Cook Islands",
        "CR" => "Costa Rica",
        "CI" => "Cote d'Ivoire",
        "HR" => "Croatia",
        "CU" => "Cuba",
        "CY" => "Cyprus",
        "CZ" => "Czech Republic",
        "DK" => "Denmark",
        "DJ" => "Djibouti",
        "DM" => "Dominica",
        "DO" => "Dominican Republic",
        "TP" => "East Timor",
        "EC" => "Ecuador",
        "EG" => "Egypt",
        "SV" => "El Salvador",
        "GQ" => "Equatorial Guinea",
        "ER" => "Eritrea",
        "EE" => "Estonia",
        "ET" => "Ethiopia",
        "FK" => "Falkland Islands",
        "FO" => "Faroe Islands",
        "FJ" => "Fiji",
        "FI" => "Finland",
        "FR" => "France",
        "FX" => "France, Metropolitan",
        "GF" => "French Guiana",
        "PF" => "French Polynesia",
        "TF" => "French South Territories",
        "GA" => "Gabon",
        "GM" => "Gambia",
        "GE" => "Georgia",
        "DE" => "Germany",
        "GH" => "Ghana",
        "GI" => "Gibraltar",
        "GR" => "Greece",
        "GL" => "Greenland",
        "GD" => "Grenada",
        "GP" => "Guadeloupe",
        "GU" => "Guam",
        "GT" => "Guatemala",
        "GN" => "Guinea",
        "GW" => "Guinea-Bissau",
        "GY" => "Guyana",
        "HT" => "Haiti",
        "HM" => "Heard Island And Mcdonald Island",
        "HN" => "Honduras",
        "HK" => "Hong Kong",
        "HU" => "Hungary",
        "IS" => "Iceland",
        "IN" => "India",
        "ID" => "Indonesia",
        "IR" => "Iran",
        "IQ" => "Iraq",
        "IE" => "Ireland",
        "IL" => "Israel",
        "IT" => "Italy",
        "JM" => "Jamaica",
        "JP" => "Japan",
        "JT" => "Johnston Island",
        "JO" => "Jordan",
        "KZ" => "Kazakhstan",
        "KE" => "Kenya",
        "KI" => "Kiribati",
        "KP" => "Korea, Democratic Peoples Republic",
        "KR" => "Korea, Republic of",
        "KW" => "Kuwait",
        "KG" => "Kyrgyzstan",
        "LA" => "Lao People's Democratic Republic",
        "LV" => "Latvia",
        "LB" => "Lebanon",
        "LS" => "Lesotho",
        "LR" => "Liberia",
        "LY" => "Libyan Arab Jamahiriya",
        "LI" => "Liechtenstein",
        "LT" => "Lithuania",
        "LU" => "Luxembourg",
        "MO" => "Macau",
        "MK" => "Macedonia",
        "MG" => "Madagascar",
        "MW" => "Malawi",
        "MY" => "Malaysia",
        "MV" => "Maldives",
        "ML" => "Mali",
        "MT" => "Malta",
        "MH" => "Marshall Islands",
        "MQ" => "Martinique",
        "MR" => "Mauritania",
        "MU" => "Mauritius",
        "YT" => "Mayotte",
        "MX" => "Mexico",
        "FM" => "Micronesia",
        "MD" => "Moldavia",
        "MC" => "Monaco",
        "MN" => "Mongolia",
        "MS" => "Montserrat",
        "MA" => "Morocco",
        "MZ" => "Mozambique",
        "MM" => "Union Of Myanmar",
        "NA" => "Namibia",
        "NR" => "Nauru Island",
        "NP" => "Nepal",
        "NL" => "Netherlands",
        "AN" => "Netherlands Antilles",
        "NC" => "New Caledonia",
        "NZ" => "New Zealand",
        "NI" => "Nicaragua",
        "NE" => "Niger",
        "NG" => "Nigeria",
        "NU" => "Niue",
        "NF" => "Norfolk Island",
        "MP" => "Mariana Islands, Northern",
        "NO" => "Norway",
        "OM" => "Oman",
        "PK" => "Pakistan",
        "PW" => "Palau Islands",
        "PS" => "Palestine",
        "PA" => "Panama",
        "PG" => "Papua New Guinea",
        "PY" => "Paraguay",
        "PE" => "Peru",
        "PH" => "Philippines",
        "PN" => "Pitcairn",
        "PL" => "Poland",
        "PT" => "Portugal",
        "PR" => "Puerto Rico",
        "QA" => "Qatar",
        "RE" => "Reunion Island",
        "RO" => "Romania",
        "RU" => "Russian Federation",
        "RW" => "Rwanda",
        "WS" => "Samoa",
        "SH" => "St Helena",
        "KN" => "St Kitts & Nevis",
        "LC" => "St Lucia",
        "PM" => "St Pierre & Miquelon",
        "VC" => "St Vincent",
        "SM" => "San Marino",
        "ST" => "Sao Tome & Principe",
        "SA" => "Saudi Arabia",
        "SN" => "Senegal",
        "SC" => "Seychelles",
        "SL" => "Sierra Leone",
        "SG" => "Singapore",
        "SK" => "Slovakia",
        "SI" => "Slovenia",
        "SB" => "Solomon Islands",
        "SO" => "Somalia",
        "ZA" => "South Africa",
        "GS" => "South Georgia and South Sandwich",
        "ES" => "Spain",
        "LK" => "Sri Lanka",
        "XX" => "Stateless Persons",
        "SD" => "Sudan",
        "SR" => "Suriname",
        "SJ" => "Svalbard and Jan Mayen",
        "SZ" => "Swaziland",
        "SE" => "Sweden",
        "CH" => "Switzerland",
        "SY" => "Syrian Arab Republic",
        "TW" => "Taiwan, Republic of China",
        "TJ" => "Tajikistan",
        "TZ" => "Tanzania",
        "TH" => "Thailand",
        "TL" => "Timor Leste",
        "TG" => "Togo",
        "TK" => "Tokelau",
        "TO" => "Tonga",
        "TT" => "Trinidad & Tobago",
        "TN" => "Tunisia",
        "TR" => "Turkey",
        "TM" => "Turkmenistan",
        "TC" => "Turks And Caicos Islands",
        "TV" => "Tuvalu",
        "UG" => "Uganda",
        "UA" => "Ukraine",
        "AE" => "United Arab Emirates",
        "GB" => "United Kingdom",
        "UM" => "US Minor Outlying Islands",
        "US" => "USA",
        "HV" => "Upper Volta",
        "UY" => "Uruguay",
        "UZ" => "Uzbekistan",
        "VU" => "Vanuatu",
        "VA" => "Vatican City State",
        "VE" => "Venezuela",
        "VN" => "Vietnam",
        "VG" => "Virgin Islands (British)",
        "VI" => "Virgin Islands (US)",
        "WF" => "Wallis And Futuna Islands",
        "EH" => "Western Sahara",
        "YE" => "Yemen Arab Rep.",
        "YD" => "Yemen Democratic",
        "YU" => "Yugoslavia",
        "ZR" => "Zaire",
        "ZM" => "Zambia",
        "ZW" => "Zimbabwe"
    );

    asort($countries);

    return $countries;
}

function get_youtube_video_id($url)
{
    $youtube_id = '';
    preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match);

    if ( ! empty($match[1]))
    {
        $youtube_id = $match[1];
    }

    return $youtube_id;
}

function get_youtube_video_embed_code($url, $frametype = "", $width = 0, $height = 0)
{
    $code = '';

    if ( ! empty($url))
    {
        $video_id = get_youtube_video_id($url);

        if ( ! empty($video_id))
        {
            $arr_youtube_embed = getCustomConfigItem('youtube_embed_' . $frametype);
            if (empty($width))
            {
                $tmp_width = $arr_youtube_embed['width'];
            }
            else
            {
                $tmp_width = $width;
            }

            if (empty($height))
            {
                $tmp_height = $arr_youtube_embed['height'];
            }
            else
            {
                $tmp_height = $height;
            }

            $url = 'https://www.youtube.com/embed/' . $video_id;

            $code = '<iframe width="' . $tmp_width . '" height="' . $tmp_height . '" src="' . $url . '" frameborder="0" allowfullscreen></iframe>';
        }
    }

    return $code;
}

function my_currency_format($amount, $prefix_currency = FALSE, $currency = '£')
{
    $my_currency_format = $prefix_currency == TRUE ? $currency . " " : "" . number_format($amount, 2, '.', '');

    return $my_currency_format;
}

function current_session_member_id()
{
    $_CI              = &get_instance();
    $sharma_tech_sess = $_CI->session->userdata('sharma_tech');

    if ( ! empty($sharma_tech_sess['user']['member_id']))
    {
        return array('member_id' => $sharma_tech_sess['user']['member_id'], 'type' => 'member');
    }
    else if ( ! empty($sharma_tech_sess['guest']['guest_id']))
    {
        return array('member_id' => $sharma_tech_sess['guest']['guest_id'], 'type' => 'guest');
    }
    else
    {
        return array('member_id' => NULL, 'type' => NULL);
    }
}

function get_quiz_session($session_type = 'quiz')
{
    $_CI       = &get_instance();
    $quiz_sess = $_CI->session->userdata($session_type);

    if ( ! empty($quiz_sess))
    {
        return $quiz_sess;
    }
    else
    {
        return NULL;
    }
}

function get_user_id()
{
    $_CI       = &get_instance();
    $online_exam_admin_sess = $_CI->session->userdata('online_exam_admin');
    if ( ! empty($online_exam_admin_sess['user']['user_id']))
    {
        return $online_exam_admin_sess['user']['user_id'];
    }
    else
    {
        return NULL;
    }
}

function get_student_session()
{
    $_CI       = &get_instance();
    $student_sess = $_CI->session->userdata('online_exam');

    if (isset($student_sess['student']) && ! empty($student_sess['student']))
    {
        return $student_sess['student'];
    }
    else
    {
        return NULL;
    }
}