<?php
$user_admin_details = get_loggedin_user_data();
?>
<!-- Top Bar Start -->
<div class="topbar">

    <!-- LOGO -->
    <div class="topbar-left">
        <a href="<?php echo base_url('admin/dashboard')?>" class="logo">
            <span>
                <img src="<?php echo base_url(); ?>assets/admin/images/logo/right-angle-logo.jpg" alt="" height="70">
            </span>
            <i>
                <img src="<?php echo base_url(); ?>assets/admin/images/logo/logo.png" alt="" height="28">
            </i>
        </a>
    </div>

    <nav class="navbar-custom">

        <ul class="list-inline float-right mb-0">
            <li class="list-inline-item dropdown notification-list">
                <a class="nav-link dropdown-toggle waves-effect waves-light nav-user" data-toggle="dropdown" href="#" role="button"
                   aria-haspopup="false" aria-expanded="false">

                        <span style="color: white">Hello! <?php echo ucfirst(!empty($user_admin_details['name']) ? $user_admin_details['name'] : 'Guest'); ?></span>

                    <img src="<?php echo base_url(); ?>assets/admin/images/logo/user-512.png" alt="user" class="rounded-circle">
                </a>
                <div class="dropdown-menu dropdown-menu-right profile-dropdown " aria-labelledby="Preview">
                    <!-- item-->
                    <div class="dropdown-item noti-title">
                        <h5 class="text-overflow">
                            <small>Welcome! <?php echo ucfirst(!empty($user_admin_details['name']) ? strtok($user_admin_details['name'], " ") : 'Guest'); ?></small>
                        </h5>
                    </div>

                    <!-- item-->
                    <a href="<?php echo base_url('admin/basic-information'); ?>" class="dropdown-item notify-item">
                        <i class="zmdi zmdi-lock-open"></i> <span>Basic Information</span>
                    </a>

                    <a href="<?php echo base_url('admin/about-us'); ?>" class="dropdown-item notify-item">
                        <i class="zmdi zmdi-lock-open"></i> <span>About Us</span>
                    </a>

                    <a href="<?php echo base_url('admin/staff/add-staff'); ?>" class="dropdown-item notify-item">
                        <i class="zmdi zmdi-lock-open"></i> <span>Staff</span>
                    </a>

                    <a href="<?php echo base_url('admin/change-password'); ?>" class="dropdown-item notify-item">
                        <i class="zmdi zmdi-lock-open"></i> <span>Change Password</span>
                    </a>

                    <!-- item-->
                    <a href="<?php echo base_url('admin/user/logout'); ?>" class="dropdown-item notify-item">
                        <i class="zmdi zmdi-power"></i> <span>Logout</span>
                    </a>

                </div>
            </li>

        </ul>

        <ul class="list-inline menu-left mb-0">
            <li class="float-left">
                <button class="button-menu-mobile open-left waves-light waves-effect">
                    <i class="dripicons-menu"></i>
                </button>
            </li>
            <li class="hide-phone app-search">
                <span style="color: white; font-size: x-large">Right Angle</span>
            </li>
        </ul>

    </nav>

</div>
<!-- Top Bar End -->