<!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu">
    <div class="slimscroll-menu" id="remove-scroll">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu" id="side-menu">
                <li class="menu-title">Navigation</li>
                <li>
                    <a href="<?php echo base_url('admin/dashboard')?>">
                        <i class="fi-air-play"></i><span> Dashboard </span>
                    </a>
                    <a href="<?php echo base_url('admin/settings')?>">
                        <i class="fi-air-play"></i><span> Settings </span>
                    </a>
                    <a href="<?php echo base_url('admin/result-records')?>">
                        <i class="fi-air-play"></i><span> Result records </span>
                    </a>
                </li>


                <?php
                $admin_det = get_current_user_access();

                $admin_user_type = !empty($admin_det['user']['type']) ? $admin_det['user']['type'] : '';

                if($admin_user_type == 'super-admin'){
                    ?>

                    <li>
                        <a href="javascript: void(0);"><i class="fa fa-user"></i><span> User </span> <span class="menu-arrow"></span></a>
                        <ul class="nav-second-level" aria-expanded="false">
                            <li><a href="<?php echo base_url('admin/user/add-user')?>"><i class="fa fa-plus"></i>Add User</a></li>
                            <li><a href="<?php echo base_url('admin/user/list-user')?>"><i class="fa fa-list"></i>List User</a></li>
                        </ul>
                    </li>
                <?php
                }
                ?>

                <li>
                    <a href="javascript: void(0);"><i class="fa fa-qrcode"></i><span> Student </span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="<?php echo base_url('admin/student/add-student')?>"><i class="fa fa-plus"></i>Add Student</a></li>
                        <li><a href="<?php echo base_url('admin/student/list-student')?>"><i class="fa fa-list"></i>List Student</a></li>
                    </ul>
                </li>

                <li>
                    <a href="javascript: void(0);"><i class="fa fa-qrcode"></i><span> Course </span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="<?php echo base_url('admin/course/add-course')?>"><i class="fa fa-plus"></i>Add Course</a></li>
                        <li><a href="<?php echo base_url('admin/course/list-course')?>"><i class="fa fa-list"></i>List Course</a></li>
                    </ul>
                </li>

                <li>
                    <a href="javascript: void(0);"><i class="fa fa-qrcode"></i><span> Question </span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="<?php echo base_url('admin/question/add-question')?>"><i class="fa fa-plus"></i>Add Question</a></li>
                        <li><a href="<?php echo base_url('admin/question/list-question')?>"><i class="fa fa-list"></i>List Question</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- Sidebar -->
        <div class="clearfix"></div>
    </div>
    <!-- Sidebar -left -->

</div>
<!-- Left Sidebar End -->