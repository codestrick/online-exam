<?php if ( ! defined('BASEPATH'))
{
    exit('No direct script access allowed');
} ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Admin | Online Exam Portal</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description"/>
    <meta content="Coderthemes" name="author"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    <!-- App favicon -->
    <link rel="icon" href="<?php echo base_url('assets/images/header-icon.png'); ?>" type="image/png">

    <?php echo $template_css; ?>

    <script src="<?php echo base_url(); ?>assets/admin/js/modernizr.min.js"></script>

    <script type="text/javascript">
        var base_url = "<?php echo base_url(); ?>";
    </script>
</head>
<body>
<div id="wrapper">

    <?php echo $template_header; ?>
    <?php echo $sidebar; ?>

    <div class="content-page">

        <?php echo $template_content; ?>
        <?php echo $template_footer; ?>

    </div>
</div>

<?php echo $template_js; ?>

</body>
</html>