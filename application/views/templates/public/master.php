<!DOCTYPE html>
<html>
<head>
    <title><?php echo ! empty($title) ? $title . ' | Online Exam' : 'Online Exam'; ?></title>
    <link rel="icon" href="<?php echo base_url('assets/images/header-icon.png'); ?>" type="image/png">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="keywords" content="Online Exam Responsive web template"/>

    <script type="application/x-javascript">
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar()
        {
            window.scrollTo(0, 1);
        }
    </script>

    <!--css-->
    <?php echo $template_css; ?>
    <!--css-->

    <!--js-->
    <?php echo $template_js; ?>
    <!--js-->

    <!--web-fonts-->
    <link href='//fonts.googleapis.com/css?family=Cagliostro' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <!--web-fonts-->

    <script type="text/javascript">
        var base_url = "<?php echo base_url(); ?>";
    </script>

</head>
<body>

<!--header-->
<?php echo $template_header; ?>
<!--header-->

<!--content-->
<div class="content">
    <?php echo $template_content; ?>
</div>
<!--content-->

<!--footer-->
<?php echo $template_footer; ?>
<!--footer-->

</body>

<script>
    $(document).ready(function (e) {

    });
</script>
</html>