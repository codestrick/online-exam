<!--footer-->
<?php
$basic_info = get_basic_info();
$about_us = get_about_info();
?>
<div class="footer-w3">
    <div class="container">
        <div class="footer-grids">
            <div class="col-md-4 footer-grid">
                <h4>About Us</h4>
                <p><?php echo $about_us['content']; ?></p>
            </div>
            <div class="col-md-4 footer-grid">
                <h4>Important Links</h4>
                <ul>
                    <li><h2><a href="<?php echo base_url('about-us'); ?>">About Us</a></h2></li>
                    <li><h2><a href="<?php echo base_url('contact-us'); ?>">Contact Us</a></h2></li>

                </ul>
            </div>
            <div class="col-md-4 footer-grid">
                <h4>Information</h4>
                <ul>
                    <li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i><?php echo !empty($basic_info['address']) ? $basic_info['address'] : '' ?></li>
                    <li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i><?php echo !empty($basic_info['phone_number']) ? $basic_info['phone_number'] : '' ?></li>
                    <li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i><a href="<?php echo !empty($basic_info['email']) ? $basic_info['email'] : '' ?>"> <?php echo !empty($basic_info['email']) ? $basic_info['email'] : '' ?></a></li>
                    <li><i class="glyphicon glyphicon-time" aria-hidden="true"></i><?php echo !empty($basic_info['opening_time']) ? $basic_info['opening_time'] : '' ?></li>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!--footer-->

<!---copy--->
<div class="copy-section">
    <div class="container">
        <div class="social-icons">
            <a href="#"><i class="icon1"></i></a>
            <a href="#"><i class="icon2"></i></a>
            <a href="#"><i class="icon3"></i></a>
            <a href="#"><i class="icon4"></i></a>
        </div>
        <div class="copy">
            <p>&copy; <?php echo date('Y'); ?> Online Exam . All rights reserved | Developed & Maintained by Subham Ghorui (9046078057)</p>
        </div>
    </div>
</div>
<!---copy--->