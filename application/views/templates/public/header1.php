<?php $student_data = get_student_session(); ?>

<?php
$basic_info = get_basic_info();
?>
<!--header-->
<div class="header">
    <div class="header-top">
        <div class="container">
            <div class="detail">
                <ul>
                    <li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i> <?php echo !empty($basic_info['phone_number']) ? $basic_info['phone_number'] : '' ?></li>
                    <li><i class="glyphicon glyphicon-time" aria-hidden="true"></i>  <?php echo !empty($basic_info['opening_time']) ? $basic_info['opening_time'] : '' ?></li>
                </ul>
            </div>
            <div class="indicate">
                <p><i class="glyphicon glyphicon-map-marker" aria-hidden="true"> </i><?php echo !empty($basic_info['address']) ? $basic_info['address'] : '' ?></p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!---Brand and toggle get grouped for better mobile display--->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="navbar-brand">
                        <h1><a href="<?php echo base_url('home'); ?>"><?php echo !empty($basic_info['institution_name']) ? $basic_info['institution_name'] : '' ?></a></h1>
                    </div>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <nav class="link-effect-2" id="link-effect-2">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="<?php echo base_url('home'); ?>"><span data-hover="Home">Home</span></a></li>
                            <li><a href="<?php echo base_url('about-us'); ?>"><span data-hover="About">About</span></a></li>
<!--                            <li><a href="--><?php //echo base_url('under-construction'); ?><!--"><span data-hover="Services">Services</span></a></li>-->
<!--                            <li><a href="--><?php //echo base_url('under-construction'); ?><!--"><span data-hover="Projects">Projects</span></a></li>-->
<!--                            <li><a href="--><?php //echo base_url('under-construction'); ?><!--"><span data-hover="Courses">Courses</span></a></li>-->
                            <li><a href="<?php echo base_url('contact-us'); ?>"><span data-hover="Contact">Contact</span></a></li>

                            <?php
                            if ( ! empty($student_data) && ! empty($student_data['is_logged_in']))
                            {
                                ?>
                                <li><a href="<?php echo base_url('quiz'); ?>"><span data-hover="Online Exam">Online Exam</span></a></li>
                                <li><a href="<?php echo base_url('student/profile'); ?>"><span data-hover="My Profile">My Profile</span></a></li>
                                <?php
                            }
                            else
                            {
                                ?>
                                <li><a href="<?php echo base_url('student/login'); ?>"><span data-hover="Login">Login</span></a></li>
                                <?php
                            }
                            ?>
                        </ul>
                    </nav>
                </div>
            </div>
        </nav>
    </div>
</div>
<!--header-->

<!-- banner -->
<div class="banner">
    <div id="kb" class="carousel kb_elastic animate_text kb_wrapper" data-ride="carousel" data-interval="6000" data-pause="hover">

        <!-- Wrapper-for-Slides -->
        <div class="carousel-inner" role="listbox">

            <!-- First-Slide -->
            <div class="item active">
                <img src="<?php echo base_url(); ?>assets/public/images/banner.jpg" alt="" class="img-responsive"/>
                <div class="carousel-caption kb_caption">
                    <h3 data-animation="animated flipInX">Created By Right Angle</h3>
                    <h4 data-animation="animated flipInX">cupidatat non proident</h4>
                </div>
            </div>

            <!-- Second-Slide -->
            <div class="item">
                <img src="<?php echo base_url(); ?>assets/public/images/banner1.jpg" alt="" class="img-responsive"/>
                <div class="carousel-caption kb_caption kb_caption_right">
                    <h3 data-animation="animated flipInX">Get Everything Right</</h3>
                    <h4 data-animation="animated flipInX">cupidatat non proident</h4>
                </div>
            </div>

            <!-- Third-Slide -->
            <div class="item">
                <img src="<?php echo base_url(); ?>assets/public/images/banner2.jpg" alt="" class="img-responsive"/>
                <div class="carousel-caption kb_caption kb_caption_center">
                    <h3 data-animation="animated flipInX">Get Everything Right</</h3>
                    <h4 data-animation="animated flipInX">cupidatat non proident</h4>
                </div>
            </div>
        </div>

        <!-- Left-Button -->
        <a class="left carousel-control kb_control_left" href="#kb" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>

        <!-- Right-Button -->
        <a class="right carousel-control kb_control_right" href="#kb" role="button" data-slide="next">
            <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>

    </div>
    <script src="<?php echo base_url(); ?>assets/public/js/custom.js"></script>
</div>
<!--banner-->