<?php $student_data = get_student_session(); ?>

<?php
$basic_info = get_basic_info();
?>
<!--header-->
<div class="header">
    <div class="header-top">
        <div class="container">
            <div class="detail">
                <ul>
                    <li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i><?php echo !empty($basic_info['phone_number']) ? $basic_info['phone_number'] : '' ?></li>
                    <li><i class="glyphicon glyphicon-time" aria-hidden="true"></i>  <?php echo !empty($basic_info['opening_time']) ? $basic_info['opening_time'] : '' ?></li>
                </ul>
            </div>
            <div class="indicate">
                <p><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i><?php echo !empty($basic_info['address']) ? $basic_info['address'] : '' ?></p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!---Brand and toggle get grouped for better mobile display--->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="navbar-brand">
                        <h1><a href="<?php echo base_url('home'); ?>"><span><?php echo !empty($basic_info['institution_name']) ? $basic_info['institution_name'] : '' ?></span></a></h1>
                    </div>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <nav class="link-effect-2" id="link-effect-2">
                        <ul class="nav navbar-nav">
                            <li><a href="<?php echo base_url('home'); ?>"><span data-hover="Home">Home</span></a></li>
                            <li <?php echo ($this->uri->segment(1) == 'about-us') ? 'class="active"' : ''; ?>>
                                <a href="<?php echo base_url('about-us'); ?>">
                                    <span data-hover="About">About</span>
                                </a>
                            </li>
<!--                            <li --><?php //echo ($this->uri->segment(1) == '') ? 'class="active"' : ''; ?><!-->
<!--                                <a href="--><?php //echo base_url('under-construction'); ?><!--">-->
<!--                                    <span data-hover="Services">Services</span>-->
<!--                                </a>-->
<!--                            </li>-->
<!--                            <li --><?php //echo ($this->uri->segment(1) == '') ? 'class="active"' : ''; ?><!-->
<!--                                <a href="--><?php //echo base_url('under-construction'); ?><!--">-->
<!--                                    <span data-hover="Projects">Projects</span>-->
<!--                                </a>-->
<!--                            </li>-->
<!--                            <li --><?php //echo ($this->uri->segment(1) == '') ? 'class="active"' : ''; ?><!-->
<!--                                <a href="--><?php //echo base_url('under-construction'); ?><!--">-->
<!--                                    <span data-hover="Courses">Courses</span>-->
<!--                                </a>-->
<!--                            </li>-->
                            <li <?php echo ($this->uri->segment(1) == 'contact-us') ? 'class="active"' : ''; ?>>
                                <a href="<?php echo base_url('contact-us'); ?>">
                                    <span data-hover="Contact">Contact</span>
                                </a>
                            </li>
                            <?php
                            if ( ! empty($student_data) && !empty($student_data['is_logged_in']))
                            {
                                ?>
                                <li <?php echo ($this->uri->segment(1) == 'quiz') ? 'class="active"' : ''; ?>>
                                    <a href="<?php echo base_url('quiz'); ?>">
                                        <span data-hover="Online Exam">Online Exam</span>
                                    </a>
                                </li>
                                <li <?php echo ($this->uri->segment(2) == 'profile') ? 'class="active"' : ''; ?>>
                                    <a href="<?php echo base_url('student/profile'); ?>">
                                        <span data-hover="My Profile">My Profile</span>
                                    </a>
                                </li>
                                <?php
                            }
                            else
                            {
                                ?>
                                <li <?php echo ($this->uri->segment(2) == 'login') ? 'class="active"' : ''; ?>>
                                    <a href="<?php echo base_url('student/login'); ?>">
                                        <span data-hover="Login">Login</span>
                                    </a>
                                </li>
                                <?php
                            }
                            ?>
                        </ul>
                    </nav>
                </div>
            </div>
        </nav>
    </div>
</div>
<!--header-->

<!--breadcrumb-->
<div class="banner-w3agile">
    <div class="container">
        <h3><a href="<?php echo base_url('home'); ?>">Home</a> / <span><?php echo ! empty($breadcrumb) ? $breadcrumb : NULL; ?></span></h3>
    </div>
</div>
<!--breadcrumb-->