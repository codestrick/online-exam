<?php
if ( ! defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

class My_Controller extends CI_Controller
{
    /**
     * $ajaxRequest : this is the variable which contains the requested page is via ajax call or not. by default it is false and will be set as false and will be set as true in constructor after validating the request type.
     *
     */
    public $ajaxRequest       = FALSE;
    public $template          = NULL;
    public $allowed_user_role = array(
        'super-admin', 'admin'
    );

    public function __construct()
    {
        parent::__construct();

        /**
         * validating the request type is ajax or not and setting up the $ajaxRequest variable as true/false.
         *
         **/
        $requestType       = $this->input->server('HTTP_X_REQUESTED_WITH');
        $this->ajaxRequest = strtolower($requestType) == 'xmlhttprequest';
        /**
         * set the default template as blank when the request type is ajax
         */
        if ($this->ajaxRequest === TRUE)
        {
            $this->load->setTemplate('blank');
        }

        $module = $this->router->fetch_module();

        switch ($module)
        {
            case 'public':
                $this->load->setTemplate('public');
                break;
        }
    }

    public function _remap($method, $params = array())
    {
        if (method_exists($this, $method))
        {
            call_user_func_array(array($this, $method), $params);
        }
        else
        {
            show_404();
        }
    }

    protected function _check_user_role($role)
    {
        if (in_array($role, $this->allowed_user_role))
        {

        }
        else
        {
            $message = "Invalid operation performed.";
            if ( ! empty($message))
            {
                $this->session->set_flashdata('user_operation_message', $message);
            }
            redirect('admin/dashboard');
        }
    }

    protected function _restrict_for_super_admin_only()
    {
        $this->allowed_user_role = [
            'super-admin',
        ];
        $this->_check_user_role($this->session->userdata['online_exam_admin']['user']['type']);
    }
}
