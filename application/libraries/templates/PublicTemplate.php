<?php

    if (!defined('BASEPATH'))
        exit('No direct script access allowed');
    /**
     * Default template
     */
    require_once 'template.php';

    /**
     * Default template implementation.
     * 
     * It is the default renderer of all the pages if any other renderer is not used.
     */
    class PublicTemplate extends Template
    {

        public function __construct()
        {
            parent::__construct();

            $this->_CI = & get_instance();
            $this->viewPath = "templates/public/";
        }

        public function render($view, array $data = array())
        {

            $this->CI->load->library('session');
            $this->CI->load->library('form_validation');
            $this->CI->load->helper('url');
            $this->CI->load->library('Commonlib');

            $return_val = $this->CI->load->viewPartial($view, $data);

            $data['template_content'] = $return_val;

            $user_id = empty($session_data['user_id']) ? '' : $session_data['user_id'];
            $user_type = empty($session_data['user_type_id']) ? '' : $session_data['user_type_id'];

            $default_language = getCustomConfigItem('default_language');

            $data['current_language'] = empty($session_lang) ? $default_language : $session_lang;
            $data['user_id'] = $user_id;
            $data['user_type'] = $user_type;

            $css_tags = $this->collectCss("public", isset($data['local_css']) ? $data['local_css'] : array());
            $data['template_css'] = implode("", $css_tags); //implode("\n", $css_tags);
            $script_tags = $this->collectJs("public", isset($data['local_js']) ? $data['local_js'] : array());

            if (isset($data['template_title']))
            {
                $data['template_title'] = $data['template_title'];
            }
            $data['template_js'] = implode("", $script_tags); //implode("\n", $script_tags);

            if(!empty($data['header_theme']))
            {
                if($data['header_theme'] == 'header1')
                {
                    $data['template_header'] = $this->CI->load->viewPartial($this->viewPath . 'header1', $data);
                }
                else
                {
                    $data['template_header'] = $this->CI->load->viewPartial($this->viewPath . 'header2', $data);
                }
            }

            $data['template_footer'] = $this->CI->load->viewPartial($this->viewPath . 'footer', $data);

            $meta_data = $this->collect_meta_data(isset($data['local_meta_data']) ? $data['local_meta_data'] : array(), $arr_meta_data = array());
            $data['template_meta_data'] = implode("", $meta_data);

            $return_val = $this->CI->load->viewPartial($this->viewPath . $this->masterTemplate, $data);
            return $return_val;
        }

    }
    