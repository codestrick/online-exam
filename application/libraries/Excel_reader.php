<?php

require(APPPATH.'libraries/spreadsheet-reader-master/php-excel-reader/excel_reader2.php');

require(APPPATH.'libraries/spreadsheet-reader-master/SpreadsheetReader.php');

    class Excel_reader
    {
        public function read_excel_file($file)
        {
            $Reader = new SpreadsheetReader($file);
            $new_array = [];
            foreach ($Reader as $k => $Row)
            {
                $new_array[$k] = $Row;
            }
            return $new_array;

        }

    }

