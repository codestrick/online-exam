<?php

    if (!defined('BASEPATH'))
        exit('No direct script access allowed');

    class Commonlib
    {

        private $_CI;

        public function __construct()
        {

            $this->_CI = & get_instance();
        }

        /*         * ******************************** User Model **************************** */

        public function get_usertype_arr()
        {
            $this->_CI->load->model('User_model');
            return $this->_CI->User_model->get_usertype_arr();
        }

        public function get_total_users()
        {
            $this->_CI->load->model('User_model');
            return $this->_CI->User_model->get_total_users();
        }

        public function get_user_byuserid($userid)
        {
            $this->_CI->load->model('User_model');
            return $this->_CI->User_model->getuserbyid($userid);
        }

        public function common_login($username, $passwords)
        {
            $this->_CI->load->model('User_model');
            return $this->_CI->User_model->common_login($username, $passwords);
        }

        public function forcefully_user_login($username, $passwords)
        {
            $this->_CI->load->model('User_model');
            return $this->_CI->User_model->forcefully_user_login($username, $passwords);
        }
 
        public function common_emailexists($user_email, $user_id = null)
        {
            $this->_CI->load->model('User_model');
            return $this->_CI->User_model->emailexists($user_email, $user_id);
        }

        public function common_username_exist($username, $user_id = null, $user_type_id = null)
        {
            $this->_CI->load->model('User_model');
            return $this->_CI->User_model->common_username_exist($username, $user_id, $user_type_id);
        }

     

        /*         * ******************************** Owner Model **************************** */
    }
    