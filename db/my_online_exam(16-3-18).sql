-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 16, 2018 at 06:09 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `my_online_exam`
--

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id`, `name`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Computer Fundamental New', 'SUB001', '2018-01-18 06:09:46', '2018-01-18 06:11:25'),
(2, 'Computer Fundamental New', 'SUB002', '2018-01-21 18:34:03', '2018-01-21 19:30:14'),
(3, 'Computer Hardware', 'SUB001', '2018-02-27 06:02:21', '2018-02-27 06:02:21'),
(4, 'New Course', 'SUB001', '2018-03-10 05:16:51', '2018-03-10 05:16:51');

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE `question` (
  `id` bigint(20) NOT NULL,
  `course_id` bigint(20) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `question` text NOT NULL,
  `option_1` text NOT NULL,
  `option_2` text NOT NULL,
  `option_3` text NOT NULL,
  `option_4` text NOT NULL,
  `right_option` int(2) NOT NULL,
  `active_status` enum('active','inactive') DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`id`, `course_id`, `user_id`, `question`, `option_1`, `option_2`, `option_3`, `option_4`, `right_option`, `active_status`, `created_at`, `updated_at`) VALUES
(1, 1, 'SUB001', 'Voluptas iste velit cum cum fugit id qui accusantium sequi quae dignissimos quam sunt non1At blanditiis assumenda modi veritatis fugit ', 'Voluptas iste velit cum cum fugit id qui accusantium sequi quae dignissimos quam sunt non1', 'At blanditiis assumenda modi veritatis fugit ut dolore consequatur et qui obcaecati quo vitae voluptatum1', 'Consequuntur culpa consequatur sint voluptate est quis vero hic quas ad sed cumque praesentium rem rem culpa cillum ut deserunt1', 'Dolor culpa consequatur totam laborum consequatur Sunt facilis eligendi1', 2, 'active', '2018-01-21 13:50:25', '2018-03-15 18:39:47'),
(2, 1, 'SUB001', 'Debitis quibusdam natus perspiciatis ', 'Quis soluta est occaecat explicabo Eve', 'Voluptatibus laudantium odio e', 'Sit ea voluptas suscipit quas quas occaecat', 'Cum aute sed nihil pariatur ', 1, 'active', '2018-01-28 05:10:07', '2018-03-15 18:39:06'),
(3, 1, 'SUB001', 'Distinctio Et consequatur quasi', 'Doloremque ex aliquam lorem c', 'Repudiandae incidunt voluptas t ', 'Dolor vero nihil et qu', 'Labore voluptates nos', 1, 'active', '2018-01-28 05:10:29', '2018-03-15 18:39:23'),
(4, 4, 'SUB001', 'Natus eu aut est aute eiusmod voluptatum', 'Voluptatibus provident omnis blanditiis la', 'Cupiditate ut eiusmod occaecnt', 'Animi nostrud excepteur laboru', 'Eaque hic vel quasi illum error ipsa', 4, 'active', '2018-01-28 05:10:46', '2018-03-15 18:39:32');

-- --------------------------------------------------------

--
-- Table structure for table `result_records`
--

CREATE TABLE `result_records` (
  `id` bigint(20) NOT NULL,
  `student_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `course_id` bigint(20) NOT NULL,
  `total_marks` varchar(255) NOT NULL,
  `marks_obtained` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `settings` text NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `user_id`, `settings`, `updated_at`) VALUES
(2, 'SUB001', '{\"1\":{\"course_id\":\"1\",\"course_name\":\"Computer Fundamental New\",\"total_question\":\"3\",\"fm\":\"3\",\"time\":\"3\",\"exam-status\":\"off\"},\"3\":{\"course_id\":\"3\",\"course_name\":\"Computer Hardware\",\"total_question\":\"0\",\"fm\":\"0\",\"time\":\"0\",\"exam-status\":\"off\"},\"4\":{\"course_id\":\"4\",\"course_name\":\"New Course\",\"total_question\":\"1\",\"fm\":\"0\",\"time\":\"0\",\"exam-status\":\"off\"}}', '2018-03-16 05:51:40'),
(3, 'SUB002', '{\"2\":{\"course_id\":\"2\",\"course_name\":\"Computer Fundamental New\",\"total_question\":\"0\",\"fm\":\"0\",\"time\":\"0\",\"exam-status\":\"off\"}}', '2018-03-10 05:21:08');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` bigint(20) NOT NULL,
  `student_roll` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` text,
  `mobile` varchar(255) NOT NULL,
  `pincode` varchar(255) NOT NULL,
  `active_status` enum('active','inactive') NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `student_roll`, `password`, `user_id`, `name`, `email`, `address`, `mobile`, `pincode`, `active_status`, `created_at`, `updated_at`) VALUES
(2, '002', '5f4dcc3b5aa765d61d8327deb882cf99', 'SUB002', 'Heather Wong', 'depuquly@gmail.com', 'Mollitia non voluptatem susci', 'Distinctio Totam nulla', '784578', 'active', '2018-01-23 05:29:52', '2018-01-23 05:31:42'),
(3, '003', '5f4dcc3b5aa765d61d8327deb882cf99', 'SUB001', 'Alexandra Ortega', 'xoqyjiq@yahoo.com', 'Autem anim ex magni esse aut officiis praesentium error voluptatem non sunt reiciendis excepteur', 'Consequuntur porro soluta eiusmod ut quo porro eos non aut quia obcaecati voluptatem aspernatur libero dolor eligendi quos aut voluptates', 'Ex incidunt consequatur laborum Asperiores soluta', 'active', '2018-02-01 13:01:05', '2018-02-01 13:01:05');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `user_id` varchar(255) DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `user_type` enum('super-admin','admin','employee') NOT NULL,
  `exam_status` enum('active','inactive') NOT NULL DEFAULT 'inactive',
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `user_id`, `name`, `mobile`, `email`, `username`, `password`, `status`, `user_type`, `exam_status`, `created_at`, `updated_at`) VALUES
(1, 'SUB001', 'Koushik Samanta', '8457845784', 'koushiksamanta034@gmail.com', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'active', 'super-admin', 'inactive', '2017-10-08 07:21:17', '2018-03-04 13:00:43'),
(2, 'SUB002', 'subham', '9046078057', 'subham.ghorui@gmail.com', 'subham', '21232f297a57a5a743894a0e4a801fc3', 'active', 'admin', 'inactive', '2018-01-15 11:17:43', '2018-01-15 15:47:43'),
(3, 'SUB003', 'Hannah Rodgers', '7845789869', 'vevumelixo@yahoo.com', 'sontu', '366aaec08dd5be244455fdf9741f5931', 'active', 'admin', 'inactive', '2018-03-10 00:31:13', '2018-03-10 05:01:13');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `result_records`
--
ALTER TABLE `result_records`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `question`
--
ALTER TABLE `question`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `result_records`
--
ALTER TABLE `result_records`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
