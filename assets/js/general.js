$(document).ready(function ()
{
    initTables();

    $(".form-valid").validate({
        ignore: [],
        errorContainer: $('#errorContainer'),
        errorLabelContainer: $('#errorContainer ul'),
        wrapper: 'li',
        onfocusout: false,
        highlight: function (element, errorClass) {
            validation_highlight(element, errorClass);
        },
        unhighlight: function (element, errorClass) {
            validation_unhighlight(element, errorClass);
        },
        submitHandler: function (form) {
            form.submit();
        }
    });

    $(".bulk-form-valid").validate({
        ignore: [],
        errorContainer: $('#errorContainerModal'),
        errorLabelContainer: $('#errorContainerModal ul'),
        wrapper: 'li',
        onfocusout: false,
        highlight: function (element, errorClass) {
            validation_highlight(element, errorClass);
        },
        unhighlight: function (element, errorClass) {
            validation_unhighlight(element, errorClass);
        },
        submitHandler: function (form) {
            form.submit();
        }
    });

    // $('#form').parsley();

    if($('select').hasClass('select2'))
    {
        $(".select2").select2();
    }

    $(".select2").on("select2:select", function ()
    {
        $(this).valid();
    });
});
function initTables()
{
    $("table.dyntable:visible").each(function (i, ele)
    {
        var ele = $(ele);
        var source = ele.attr('source');
        var jsonStr = ele.attr('jsonInfo');
        var max_rows = ele.attr("max_rows");
        ele.dataTable({
            "searchable": true,
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "pageLength": 10,
            "sortable": true,
            "serverSide": true,
            "processing": true,
            "pagingType": "full_numbers",
            "ajax": source,
            "order": [[0, 'asc']],
            "columns": eval(jsonStr)
        });

        //  ele.dataTable().fnFilterOnReturn();
    });
}

function validation_highlight(element, errorClass)
{
    if ($(element).hasClass('select2'))
    {
        $(element).next('.select2-container').addClass(errorClass);
    }
    else
    {
        $(element).addClass(errorClass);
    }
}

function validation_unhighlight(element, errorClass)
{
    if ($(element).hasClass('select2'))
    {
        $(element).next('.select2-container').removeClass(errorClass);
    }
    else
    {
        $(element).removeClass(errorClass);
    }
}